/*jslint node: true, indent : 2*/
(function () {
  /**
   * Util to update connection settings of the High-Tool models
   **/
  'use strict';
  const fs = require('fs');
  const path = require('path');

  /**
   * Simple load/search&replace/save
   * @param  {string} filename     File to update
   * @param  {array} arrayOfRegex  Array of arrays of 2 elements [string to send to the RegExp factory, new text]
   * @param  {function} errorCb    On error callback
   * @param  {function} successCb  On success callback
   * @return {void}
   */
  function updateFile(filename, arrayOfRegex, errorCb, successCb) {
    fs.readFile(filename, 'utf8', function (err, data) {
      var currentRegex;
      if (err) {
        errorCb(err);
      } else {
        arrayOfRegex.forEach((replacement) => {
          currentRegex = new RegExp(replacement[0], "g");
          data = data.replace(currentRegex, replacement[1]);
        });
        fs.writeFile(filename, data, 'utf8', function (err) {
          if (err) {
            errorCb(err);
          } else {
            successCb();
          }
        });
      }
    });
  }

  /**
   * Modifies all the .cfg files of the Java HIGH-TOOL models
   * @param  {sailsRoot} sailsRoot   root folder of the sails, HIGH-TOOL server, instance.
   * @param  {Object} newSettings key/values of the setting. Defaults to parameters of
   * the docker instance.
   * @return {Promise} Resolves on success and rejects bubbling the error to the caller.
   */
  function updateModulesConfig(sailsRoot, newSettings) {
    console.log(newSettings);
    return new Promise(function (resolve, reject) {
      var updatePromises = [],
        arrayOfRegex = [
          ['dbURL\\s*=.*', `dbURL = jdbc:postgresql://${(newSettings.host ? newSettings.host : 'localhost')}${(newSettings.port ? ':' + newSettings.port : '')}/${(newSettings.database ? newSettings.database : 'high_tool')}`],
          ['user\\s*=.*', `user = ${(newSettings.user ? newSettings.user : 'high_tool')}`],
          ['password\\s*=.*', `password = ${(newSettings.password ? newSettings.password : 'mcrit')}`],
          ['baselineScheme\\s*=.*', `baselineScheme = ${(newSettings.baselineScheme ? newSettings.baselineScheme : 'high_tool')}`],
          ['PathExcel\\s*=.*', `PathExcel = ${(newSettings.PathExcel ? newSettings.PathExcel : '')}`],
          ['useLibreOffice\\s*=.*', `useLibreOffice = ${(newSettings.useLibreOffice ? 1 : 0)}`],
          ['LibreOfficeURL\\s*=.*', `LibreOfficeURL = ${(newSettings.LibreOfficeURL ? newSettings.LibreOfficeURL : 'uno:socket,host=localhost,port=2002;urp;StarOffice.ServiceManager')}`]
        ];
      fs.readdir(path.resolve(sailsRoot, 'java_modules', 'config'), function (err, files) {
        if (err) {
          reject(err);
        } else {
          files.filter((filename) => {
            return (filename.search(".cfg") !== -1);
          })
          .forEach((filename) => {
            updatePromises.push(new Promise(function (resolve, reject) {
              updateFile(path.resolve(sailsRoot, 'java_modules', 'config', filename), arrayOfRegex, reject, resolve);
            }));
          });
          Promise.all(updatePromises)
            .then(() => {
              resolve();
            })
            .catch((err) => {
              reject(err);
            });
        }
      });
    });
  }

  /**
   * Modifies the connections.js file to match the Data Stock configuration
   * @param  {string} sailsRoot   root folder of the sails, HIGH-TOOL server, instance.
   * @param  {Object} newSettings key/values of the setting. Defaults to parameters of
   * the docker instance.
   * @return {Promise} Resolves on success and rejects bubbling the error to the caller.
   */
  function updateConnectionSettings(sailsRoot, newSettings) {
    return new Promise(function (resolve, reject) {
      var newSettingStr = `HTDataStock: {
        migrate : 'safe',
        adapter: 'sails-postgresql',
        host: '${(newSettings['host'] ? newSettings['host'] : 'localhost')}',
        port: ${(newSettings['port'] ? newSettings['port'] : '5432')},
        user: '${(newSettings['user'] ? newSettings['user'] : 'high_tool')}',
        password: '${(newSettings['password'] ? newSettings['password'] : 'mcrit')}',
        database: '${(newSettings['database'] ? newSettings['database'] : 'high_tool')}'
      }`;

      updateFile(`${sailsRoot}/config/connections.js`, [
        ['HTDataStock\\s*:\\s*\\{(\\n|\\r|[^\\}])*\\}', newSettingStr]
      ], reject, resolve);
    });
  }

  function updateBaseSchema(sailsRoot, newBaseSchema = 'high_tool') {
    return new Promise((resolve, reject) => {
      updateFile(`${sailsRoot}/config/HTConfig.js`, [
        ['baseSchema\\s*:\\s*\\\'[a-zA-Z0-9_]+\\\',', `baseSchema : '${newBaseSchema}',`]
      ], reject, resolve);
    });
  }


  function getSAFOptionPromise(sailsRoot) {
    return new Promise((resolve, reject) => {
      getSAFOption(sailsRoot, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
  }

  function getSAFOption(sailsRoot, cb) {
    fs.readFile(`${sailsRoot}/java_modules/config/safety.cfg`, 'utf8', function (err, data) {
      if (err) {
        cb(err, null);
        return null;
      } else {
        var paramReg = new RegExp(/(PathExcel|useLibreOffice|LibreOfficeURL)\s*=\s*(.*)/g),
          retObj = {},
          paramRes;

        while ((paramRes = paramReg.exec(data)) !== null) {
          retObj[paramRes[1]] = paramRes[2];
        }
        cb(null, retObj);
      }
    });
  }

  function downloadTemplateCompiler(masterServer) {
    var downloadURL = masterServer || 'hightool.mcrit.com';

    return new Promise(function (resolve, reject) {
      const https = require('https'),
        fs = require('fs'),
        request = https.request({
          host : downloadURL,
          path : `/api/modules/templateCompiler`,
          method : 'GET'
        }, response => {
          var data = [];

          response
            .on('data', chunk => {
              data.push(chunk);
            })
            .on('response', msg => {
              if (msg.status !== 200) {
                reject({status : msg.statusCode, msg : msg.statusMessage});
                return response.end();
              }
            })
            .on('error', err => {
              return reject(err);
            })
            .on('end', () => {
              fs.writeFile(`${__dirname}/../java_modules/HTTemplateCompiler.jar`, Buffer.concat(data), err => {
                if (err) {
                  return reject(err);
                }
                resolve();
              })
            });
        });

        request.on('error', e => {
          console.error(`problem with request: ${e.message}`);
          reject(e);
        });

        request.end();

    });
  }


  function downloadAllModules(masterServer) {
    var downloadURL = masterServer || 'hightool.mcrit.com';

    return new Promise(function (resolve, reject) {
      getSAFOption(process.cwd(), (err, safSettings) => {
        const serverSettings = require('../config/connections.js').connections.HTDataStock;
        if (safSettings) {
          Object.keys(safSettings)
            .forEach(property => {
              serverSettings[property] = safSettings[property];
            });
        }

        const downloadQueue = ['DEM', 'ECR', 'PAD', 'FRD', 'VES', 'SAF', 'ENV'].reduce(function (queue, moduleId) {
          queue.push(new Promise(function (resolve, reject) {
            const https = require('https'),
              request = https.request({
                host : downloadURL,
                path : `/api/modules/${moduleId}`,
                method : 'GET'
              }, response => {
                var data = [];

                response
                  .on('response', msg => {
                    if (msj.status !== 200) {
                      reject({status : msg.statusCode, msg : msg.statusMessage});
                      return response.end();
                    }
                  })
                  .on('data', chunk => {
                    data.push(chunk);
                  })
                  .on('err', err => {
                    return reject(err);
                  })
                  .on('end', () => {
                    try {
                      const zip = new require('adm-zip')(Buffer.concat(data));
                      zip.extractAllTo(`${__dirname}/../`, true);
                      console.log(`unziped ${moduleId}`);


                          updateModulesConfig(process.cwd(), serverSettings)
                            .then(() => {
                              console.log(`Downloaded and updated ${moduleId}`);
                              resolve();
                            })

                    } catch (err) {
                      console.error(err);
                      reject();
                    }
                  });
              });

            request.on('error', e => {
              console.error(`problem with request: ${e.message}`);
              reject(e);
            });

            request.end();
          }));

          return queue;
        }, []);

        Promise.all(downloadQueue)
          .then(resolve)
          .catch(reject);
      });
    });
  }

  module.exports = {
    downloadTemplateCompiler : downloadTemplateCompiler,
    downloadAllModules : downloadAllModules,
    updateModulesConfig : updateModulesConfig,
    updateConnectionSettings : updateConnectionSettings,
    updateBaseSchema : updateBaseSchema,
    getSAFOption : getSAFOptionPromise
  };

}());