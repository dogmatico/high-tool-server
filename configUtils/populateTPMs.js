(function () {
  'use strict';
  var XLSX = require('xlsx'),
    pg = require('pg'),
    workbookLevers = XLSX.readFile('configUtils/TPMLists/Policy lever values_ordenat.xlsx'),
    workbookTPM = XLSX.readFile('configUtils/TPMLists/HIGH-TOOL_TPM_Assessment_20151001_removedTPMs.xlsx'),
    worksheet = workbookLevers.Sheets['ALL'],
    TPMSheetList = Object.keys(workbookTPM.Sheets),
    categoriesHash = {},
    categoriesArr = [],
    TPMArr = [];

    /** modes (mode_id)
     * "1";"road"
     * "2";"rail"
     * "3";"air"
     *"4";"iww"
     *"5";"sea"
     * "6";"short-sea"
     * "7";"coach"
     * "8";"road_car"
     *"9";"road_motorcycle"
     **/

    /**
     * road_pas_new_el_hyb : cngb (3), hev (8), lng (12)
     * road_pas_new_el_nhy : bev (1), fcev(5)
     * road_pas_new_ne : 0, lpg (9),6
     * road_pas_old : diesel (4), gasoline (7)
     * road_fre_new_el_hyb :	cngb (3), hev (8), lng (12)
     * road_fre_new_el_nhy :	bev (1), fcev(5)
     * road_fre_new_ne :	b30 (0), lpg (9), e85 (6)
     * road_fre_old	: diesel (4), gasoline (7)
     * rail_pas_new	electric (10),
     * rail_fre_new	electric (10),
     * rail_fre_old	diesel (4),
     * air_pas_new	jet fuel (11),
     * air_pas_old	jet fuel (11),
     * air_fre_new	jet fuel (11),
     * air_fre_old	jet fuel (11),
     * iww_fre_new	hfo (15), mdo/mgo (14),
     * iww_fre_old	lng (12),
     * sss_fre_new	hfo (15), mdo/mgo (14)
     * sss_fre_old	lng (12)
     **/

     /*"0";"car"
     "1";"van"[mode_vs_id=1,2]
     "10";"airfreight"
     "2";"hdv"
     "3";"railfreight"
     "4";"iww"
     "5";"marfreight"
     "6";"airpass"
     "7";"bus"
     "8";"motocp"
     "9";"railpass"*/



    function affectedDimensionsToTarget(literal, targetObj, TPM, HTModule) {
      function initAndAdd(propName, value) {
        if (!targetObj[propName]) {
          targetObj.__dimensions[propName] = [];
        }
        value.split(',')
          .map(item => {
            return parseInt(item, 10);
          })
          .forEach(id => {
          if (targetObj.__dimensions[propName].indexOf(id) === -1) {
            targetObj.__dimensions[propName].push(id);
          }
        });
      }

      if (HTModule === 'Safety') {
        targetObj.__parameters.push(`${TPM}_${literal}`);
      } else {
        switch (literal) {
        case 'road':
          initAndAdd('mode_id', '1,8,9');
          break;
        case 'truck':
          initAndAdd('mode_id', '1');
          break;
        case 'rail':
          initAndAdd('mode_id', '2');
          break;
        case 'air':
          initAndAdd('mode_id', '3');
          break;
        case 'iww':
          initAndAdd('mode_id', '4');
          break;
        case 'sss':
          initAndAdd('mode_id', '5,6');
          break;
        case 'coach':
          initAndAdd('mode_id', '7');
          break;
        case 'road_pas_new_el_hyb':
          initAndAdd('veh_fuel_id', '3,8,12');
          initAndAdd('mode_vs_id', '0,7,8');
          break;
        case 'road_pas_new_el_nhy':
          initAndAdd('veh_fuel_id', '1,5');
          initAndAdd('mode_vs_id', '0,7,8');
          break;
        case 'road_pas_new_ne':
          initAndAdd('veh_fuel_id', '0,9,6');
          initAndAdd('mode_vs_id', '0,7,8');
          break;
        case 'road_pas_old':
          initAndAdd('veh_fuel_id', '4,7');
          initAndAdd('mode_vs_id', '0,7,8');
          break;
        case 'road_fre_new_el_hyb':
          initAndAdd('veh_fuel_id', '3,8,12');
          initAndAdd('mode_vs_id', '1,2');
          break;
        case 'road_fre_new_el_nhy':
          initAndAdd('veh_fuel_id', '1,5');
          initAndAdd('mode_vs_id', '1,2');
          break;
        case 'road_fre_new_ne':
          initAndAdd('veh_fuel_id', '0,9,6');
          initAndAdd('mode_vs_id', '1,2');
          break;
        case 'road_fre_old':
          initAndAdd('veh_fuel_id', '4,7');
          initAndAdd('mode_vs_id', '1,2');
          break;
        case 'rail_pas_new':
          initAndAdd('veh_fuel_id', '10');
          initAndAdd('mode_vs_id', '9');
          break;
        case 'rail_fre_new':
          initAndAdd('veh_fuel_id', '10');
          initAndAdd('mode_vs_id', '3');
          break;
        case 'rail_fre_old':
          initAndAdd('veh_fuel_id', '4');
          initAndAdd('mode_vs_id', '3');
          break;
        case 'air_pas_new':
          initAndAdd('veh_fuel_id', '11');
          initAndAdd('mode_vs_id', '6');
          break;
        case 'air_pas_old':
          initAndAdd('veh_fuel_id', '11');
          initAndAdd('mode_vs_id', '6');
          break;
        case 'air_fre_new':
          initAndAdd('veh_fuel_id', '11');
          initAndAdd('mode_vs_id', '10');
          break;
        case 'air_fre_old':
          initAndAdd('veh_fuel_id', '11');
          initAndAdd('mode_vs_id', '10');
          break;
        case 'iww_fre_new':
          initAndAdd('veh_fuel_id', '15,14');
          initAndAdd('mode_vs_id', '4');
          break;
        case 'iww_fre_old':
          initAndAdd('veh_fuel_id', '12');
          initAndAdd('mode_vs_id', '4');
          break;
        case 'sss_fre_new':
          initAndAdd('veh_fuel_id', '15,14');
          initAndAdd('mode_vs_id', '5');
          break;
        case 'sss_fre_old':
          initAndAdd('veh_fuel_id', '12');
          initAndAdd('mode_vs_id', '5');
          break;
        case 'i_ev_emfactor (PM10)':
          initAndAdd('em_type_id', '4');
          break;
        case 'i_ev_emfactor (CO2)':
          initAndAdd('em_type_id', '2');
          break;
        case 'i_ev_emfactor (NOx)':
          initAndAdd('em_type_id', '1');
          break;
        case 'i_ev_emfactor (SO2)':
          initAndAdd('em_type_id', '3');
          break;
        case 'i_ev_emfactor (VOC)':
          initAndAdd('em_type_id', '0');
          break;
        }
      }
    }

    for (let i = 0, ln = TPMSheetList.length, found = false; i < ln && !found; i+= 1) {
      if (TPMSheetList[i] === 'TPMs -&gt;') {
        TPMSheetList.splice(0, i + 1);
        TPMSheetList.splice(TPMSheetList.length - 4, TPMSheetList.length - 1);
      }
    }

    var numCats = 0;
    TPMSheetList.forEach(function (TPM) {
      var currentSheet = workbookTPM.Sheets[TPM],
        currentCat = currentSheet.E3.v;
        if (!categoriesHash[currentCat] && categoriesHash[currentCat] !== 0) {
          categoriesHash[currentCat] = numCats;
          categoriesArr.push({
            shortLabel : currentCat,
            parent : 'TPMROOT',
            uuid: 'TPMCA' + numCats
          });
          numCats += 1;
        }
      TPMArr.push({
        shortLabel : currentSheet.E1.v,
        label : currentSheet.E5.v,
        description : currentSheet.E6.v,
        source : (currentSheet.E7 ? currentSheet.E7.v : ''),
        parent : 'TPMCA' + categoriesHash[currentCat],
        uuid : `TPMI${TPM.match(/(\d+\.{1}\d+|\d+).+/)[1].toString().replace('\.', '_')}`
      });
    });

    TPMArr.push({
        label: "Root TPMs",
        shortLabel: "Root TPMs",
        description: "description",
        unit: "unit",
        min: null,
        max: null,
        parent: null,
        uuid: "TPMROOT"
      });

    var headers = {},
      data = [];
    for (let z in worksheet) {
        if(z[0] === '!') continue;
        //parse out the column, row, and value
        var col = z.substring(0,1);
        var row = parseInt(z.substring(1));
        var value = worksheet[z].v;

        //store header names
        if(row == 1) {
            headers[col] = value;
            continue;
        }

        if(!data[row]) data[row]={};
        data[row][headers[col]] = value;
    }

    var red = data.reduce(function (prev, curr) {
      var ln = prev.length,
        itemNum = 0;

      if (ln === 0
        || prev[ln - 1].TPM !== curr['Transport policy measure']
        || prev[ln - 1].shortLabel !== curr['Policy lever']
        || curr['Differenciated dimensions'] === 'yes') {

        let TPMId = curr['Transport policy measure'].match(/(\d+\.{1}\d+|\d+).+/),
          min = Number.parseFloat(curr['Lower bound value']),
          max = Number.parseFloat(curr['Upper bound value']),
          baseline = Number.parseFloat(curr['Default value']),
          itemNum = 0,
          tmp;

          if (curr['Unit'] === '(100=Baseline)') {
            min = +(1 + min).toFixed(4);
            max = +(1 + max).toFixed(4);
            baseline = +(1 + baseline).toFixed(4);
          }


         if (min > max) {
           tmp = min;
           min = max;
           max = tmp;
         }

        prev.push({
          TPM : curr['Transport policy measure'],
          TPMId : TPMId[1].toString().replace('\.', '_') || '',
          shortLabel : curr['Policy lever'],
          TPMModule : curr['High-Tool Module'],
          description : curr['Policy lever description'],
          source : curr['Source of chosen values'],
          unit : curr['Unit'],
          min : min || null,
          max : max || null,
          baseline : baseline || null,
          target : {
            __parameters : ( curr['High-Tool Module'] === 'Safety' ? [] :
                            curr['High-Tool Module'] === 'Environment' ? ['i_ev_emfactor'] :
                            [curr['Policy lever']]),
            __dimensions : {}
          }
        })

        ln += 1;

        if (ln > 1 && prev[ln - 1].TPMId === prev[ln - 2].TPMId) {
          itemNum = parseInt(prev[ln - 2].uuid.match(/(\d+$)/)[1],10) + 1;
        }
        // TPM PL = Policy Lever ID I = Item Number
        prev[ln - 1].parent = `TPMI${prev[ln - 1].TPMId}`;
        prev[ln - 1].uuid = `TPMPL${prev[ln - 1].TPMId}I${itemNum}`;
      }

      if (curr['High-Tool Module'] === 'Environment') {
        affectedDimensionsToTarget(curr['Policy lever'], prev[ln -1].target, curr['Policy lever'], curr['High-Tool Module'])
      }
      affectedDimensionsToTarget(curr['Affected dimensions'], prev[ln -1].target, curr['Policy lever'], curr['High-Tool Module']);

      if (curr['Differenciated dimensions'] === 'yes') {
        prev[ln - 1].shortLabel += ` (${curr['Affected dimensions']})`;
      }

      return prev;
    }, [])
    // Remove Null items
    .filter(item => {
      return (item.min !== null &&
              item.max !== null &&
              item.baseline !== null);
    });
    /*.map(function (item) {
      item.target = `${item.target.__parameters.join(' ')}${Object.keys(item.target.modifiers).reduce((prev, curr) => {
        prev += `[${curr}=${item.target.modifiers[curr].join(',')}]`
        return prev;
      }, '')}`
      return item;
    })*/

    console.log(JSON.stringify(categoriesArr.concat(TPMArr.concat(red))));
}());
