(function () {
  'use strict';

  const XLSX = require('xlsx'),
    pg = require('pg'),
    workbookTPM = XLSX.readFile('configUtils/TPMLists/HIGH-TOOL_TPM_Assessment_20151001_removedTPMs.xlsx'),
    reg = new RegExp(/^(\d+\.{0,1}\d*)\s*(.*)/),
    TPMSheetList = Object.keys(workbookTPM.Sheets).filter(item => {
      return reg.exec(item);
    });

  var categoriesArr = [],
    tpmList = [];

  const tablesList = [
    // __tpm
    `CREATE TABLE __tpm
      (
        id character varying(255) NOT NULL,
        name text,
        short_name character varying(255),
        description text,
        target text[],
        white_paper text[],
        category_id integer,
        CONSTRAINT __tpm_pkey PRIMARY KEY (id),
        CONSTRAINT fk__tpm_categories_id FOREIGN KEY (category_id)
            REFERENCES __tpm_categories (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION
      )
      WITH (
        OIDS=FALSE
      );`,
      // __tpm_categories
    `CREATE TABLE __tpm_categories
      (
        id integer NOT NULL,
        name text,
        CONSTRAINT __tpm_categories_pkey PRIMARY KEY (id)
      )
    WITH (
      OIDS=FALSE
    );`,
      // __tpm_lever_value
    `CREATE TABLE __tpm_lever_value
      (
        __tpm_id character varying(255) NOT NULL,
        name character varying(255) NOT NULL,
        parameter_id character varying(255) NOT NULL,
        parameter_full_id character varying(1024) NOT NULL,
        default_value double precision NOT NULL,
        lower_bound double precision NOT NULL,
        upper_bound double precision NOT NULL,
        CONSTRAINT __tmp_levers_value_pk PRIMARY KEY (parameter_full_id),
        CONSTRAINT fk__tmp_id FOREIGN KEY (__tpm_id)
            REFERENCES __tpm (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_parameter_full_id FOREIGN KEY (parameter_full_id)
            REFERENCES __policy_lever (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_parameter_id FOREIGN KEY (parameter_id)
            REFERENCES __parameters (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION
      )
      WITH (
        OIDS=FALSE
      );`,
      `CREATE TABLE __policy_lever
      (
        id character varying(255) NOT NULL,
        name character varying(255) NOT NULL,
        description character varying(1024),
        remark character varying(512),
        parameter_id character varying(255) NOT NULL,
        module character varying(255) NOT NULL,
        dimension_1_id character varying(255),
        dimension_1_element_id character varying(255),
        dimension_2_id character varying(255),
        dimension_2_element_id character varying(255),
        dimension_3_id character varying(255),
        dimension_3_element_id character varying(255),
        dimension_4_id character varying(255),
        dimension_4_element_id character varying(255),
        dimension_5_id character varying(255),
        dimension_5_element_id character varying(255),
        dimension_6_id character varying(255),
        dimension_6_element_id character varying(255),
        dimension_7_id character varying(255),
        dimension_7_element_id character varying(255),
        dimension_8_id character varying(255),
        dimension_8_element_id character varying(255),
        is_relative bit(1) NOT NULL,
        unit_id character varying(255) NOT NULL,
        is_active_for_custom_tpm bit(1) NOT NULL,
        CONSTRAINT __policy_lever_pkey PRIMARY KEY (id),
        CONSTRAINT fk_dimension_1_id FOREIGN KEY (dimension_1_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_2_id FOREIGN KEY (dimension_2_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_3_id FOREIGN KEY (dimension_3_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_4_id FOREIGN KEY (dimension_4_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_5_id FOREIGN KEY (dimension_5_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_6_id FOREIGN KEY (dimension_6_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_7_id FOREIGN KEY (dimension_7_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_dimension_8_id FOREIGN KEY (dimension_8_id)
            REFERENCES __dimensions (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_parameter_id FOREIGN KEY (parameter_id)
            REFERENCES __parameters (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION,
        CONSTRAINT fk_unit_id FOREIGN KEY (unit_id)
            REFERENCES _unit (id) MATCH SIMPLE
            ON UPDATE NO ACTION ON DELETE NO ACTION
      )
      WITH (
        OIDS=FALSE
      );`
  ];


  function createTables(cursor, tablesList) {
    function execTableSQL(tableDefinition) {
      return new Promise(function(resolve, reject) {
        client.query(tableDefinition, err => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    }

    return Promise.all(tablesList.reduce(function (prev, curr) {
      prev.push(execTableSQL(curr));
      return prev
    }, []));
  }

  function deleteOldData(cursor) {
    return new Promise(function (resolve, reject) {
      const SQL = `DELETE FROM high_tool.__tpm;
                   DELETE FROM high_tool.__tpm_categories;`;
      cursor.query(SQL, err => {
        if (err) {
          reject({sql : SQL, err: err});
        } else {
          resolve();
        }
      });
    });
  }

  function updateCategories(cursor, SQLCategories) {
    return new Promise(function (resolve, reject) {
      const SQLCat = `INSERT INTO high_tool.__tpm_categories (id , name) VALUES ${SQLCategories.join(', ')};`;

      cursor.query(SQLCat, err => {
        if (err) {
          reject({sql: SQLCat, err: err});
        } else {
          resolve();
        }
      });
    });
  }

  function updateTPMTable(cursor, tpmList) {
    return new Promise(function (resolve, reject) {
      const keys = Object.keys(tpmList[0]);
      const values = tpmList.reduce(function (prev, curr) {
        let values = [];
        keys.forEach(column => {
          if (column === 'target' || column === 'white_paper') {
            values.push(`'{"${curr[column].join('", "')}"}'`);
          } else {
            values.push(`'${curr[column]}'`);
          }
        });
        prev.push(`(${values.join(', ')})`)
        return prev;
      }, []);
      const SQLTPMS = `INSERT INTO high_tool.__tpm (${keys.join(', ')}) VALUES ${values.join(', ')};`;

      cursor.query(SQLTPMS, err => {
        if (err) {
          reject({sql: SQLTPMS, err: err});
        } else {
          resolve();
        }
      });
    });
  }


  pg.connect(require(process.cwd() + '/config/connections.js').connections.HTDataStock, function (err, client, done) {
    if (err) {
      console.error(err);
      return new Error(err);
    }

    TPMSheetList.forEach(TPM => {
      const currentSheet = workbookTPM.Sheets[TPM],
        category = currentSheet.E3.v;

      let categoryId = categoriesArr.indexOf(category);

      if (categoryId === -1) {
        categoriesArr.push(category);
        categoryId = categoriesArr.length - 1;
      }

      const parsedName = reg.exec(TPM),
        id = parsedName[1],
        shortName = parsedName[2];

      tpmList.push({
        id : parsedName[1],
        short_name : parsedName[2],
        name : currentSheet.E1.v,
        description : currentSheet.E5.v,
        target : currentSheet.E6.v
          .replace(/'/g , '\'\'')
          .replace(/"/g, '\\"')
          .split('&#10;')
          .filter(item => {
            return item !== '';
          }),
        white_paper : currentSheet.E7.v
          .replace(/'/g, '\'\'')
          .replace(/"/g, '\\"')
          .split('&#10;')
          .filter(item => {
            return item !== '';
          }),
        category_id : categoryId
      });
    });

    const SQLCategories = categoriesArr.reduce(function (acum, category, index) {
      acum.push(`('${index}', '${category}')`);
      return acum;
    }, []);

    deleteOldData(client)
      .then(() => {
        return updateCategories(client, SQLCategories);
      })
      .then(() => {
        return updateTPMTable(client, tpmList);
      })
      .then(() => {
        console.log('Done');
        done();
        return process.exit(0);
      })
      .catch(err => {
        console.error(err);
        throw new Error(err);
        process.exit(1);
      });
  });

}());
