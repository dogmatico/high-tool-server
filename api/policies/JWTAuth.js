(function () {
  /**
  * sessionAuth
  *
  * @module      :: Policy
  * @description :: Policy to handle JWT authentication. Just exports the
  * module
  * @docs        :: http://sailsjs.org/#!documentation/policies
  *
  */
  var expressJwt = require('express-jwt'),
    secretKey = sails.config.JWT.secretKey;
  return module.exports = expressJwt({secret: secretKey});
}());
