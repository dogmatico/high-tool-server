/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Checks if the user is an admin or the owner o a 
 * pakage`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {
  if(!req.user || !req.user.id) {
    res.status(500);
    return res.json({error : "The user object or ID was not defined."});
  } else {
    req.filterByOwner = true;
    return next();
  }
};
