/**
* Package.js
*
* @description :: Model of HIGH-TOOL packages. It includes metadata, ownership and
* a json document with the values. Can also include an array of subpackages
* referenced by id.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  meta: {
   schemaName: 'high_tool_interface'
  },
  tableName : 'packages',
  attributes: {
    label : 'string',
    headline: 'string',
    abstract: 'string',
    owner : {
      model: 'user',
      via: 'id'
    },
    rootNode : 'string',
    subpackages : 'array',
    processed : 'boolean',
    data : 'json',
    nodeList : 'json',
    public : {
      type : 'boolean',
      defaultsTo : false
    }

  }
};
