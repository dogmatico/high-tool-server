/**
* StatisticalData.js
*
* @description :: Imports of EuroStat data.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  meta: {
   schemaName: 'high_tool_interface'
  },
  tableName: 'statistical_data',
  attributes: {
    id: {
      type: 'string',
      unique: true,
      required: true,
      primaryKey: true
    },
    description : 'string',
    value : 'json',
    unit: 'string',
    dataSource : 'string'
  },
  getHierarchicalTrieDictionary : function (schema) {
    schema = schema || sails.config.HTConfig.baseSchema;
    const {HierarchicalTrie} = require('high-tool-ds-updater');
    return new Promise((resolve, reject) => {
      this.find()
        .then(results => {
          const trieDictionary = results.reduce((acum, curr) => {
            const hierarchicalTrie = new HierarchicalTrie(
              'EU28', curr.data.map(item => [item.code, item.value])
            );
            acum[curr.id] = hierarchicalTrie;
            return acum;
          }, {});
          resolve(trieDictionary);
        })
        .catch(reject);
    });
  }
};
