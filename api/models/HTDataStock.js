/*jslint node: true, indent: 2, esnext : true  */
(function () {
  /**
  * HTDataStock.js
  *
  * @description :: This Model comprises methods to send native SQL queries to
  * the Data Stock in order to populate and query aggregates.
  * @docs        :: http://sailsjs.org/#!documentation/models
  */
  "use strict";

  const pg = require('pg');
  const Promise = require('bluebird');

  function sortAgeGroups(a, b) {
    if (a.year !== b.year) {
      return a.year - b.year;
    }
    if (a.agegroup.search('>=') !== -1) {
      return 1;
    }
    if (b.agegroup.search('>=') !== -1) {
      return -1;
    }
    return a.agegroup.split('-')[1] - b.agegroup.split('-')[1];
  }

  function singleTableExternality(schema, parameters) {

    const withSegment = `WITH countries AS (
      SELECT DISTINCT country_id as id, ht2006_cont_id as cont_id
      FROM ${schema}._ht2006_level_2
    ), externalityCosts AS (
      SELECT
      ${Object.keys(parameters.externalities).map(key => {
        return `MAX(CASE id WHEN '${key}' THEN
            value
        ELSE
            0
        END) AS ${parameters.externalities[key].name}`;
      }).join(', ')}
      FROM ${schema}.__externality
    ), filteredAggregated AS (
      SELECT year_id as year, (
        CASE
        ${parameters.modes.map(item => {
          return `WHEN ${item.modes.map(item => `${parameters.modeColumn} = '${item}'`).join(' OR ')} THEN
            '${item.name}'`;
        }).join('\n')}
        ELSE
          NULL
        END
      ) AS mode,
      ${Object.keys(parameters.externalities).map(key => {
        return `SUM(${parameters.externalities[key].column}) AS ${parameters.externalities[key].name}`
      }).join(', ')}
      FROM ${schema}.${parameters.tableId}
      INNER JOIN countries
      ON (countries.id = ${parameters.tableId}.country_id)
      WHERE countries.cont_id = '1'
      GROUP BY year, mode,cont_id
    ), computedData AS (
      SELECT mode, year,
      ${Object.keys(parameters.externalities).map(key => {
        const name = parameters.externalities[key].name;
        return `externalityCosts.${name} * filteredAggregated.${name} / 1000000 AS ${name}`
      }).join(', ')}
      FROM filteredAggregated
      LEFT OUTER JOIN externalityCosts ON (1 = 1)
      WHERE mode IS NOT NULL
      ORDER BY mode, year
    )`;

    const externalitiesName = Object.keys(parameters.externalities).map(key => parameters.externalities[key].name);

    const selectSegment = `SELECT json_build_object('name', '${parameters.name}', 'description', '${parameters.description}', 'unit', '${parameters.unit}', 'data', json_agg(data)) as json
      FROM (
        SELECT json_build_object(
        'mode', mode, 'values', json_agg(json_build_object(
          'year', year, ${externalitiesName.map(name => `'${name}', ${name}`).join(', ')}
          ))
        ) as data
        FROM computedData
        GROUP BY mode
      ) data;`

    return withSegment + '\n' + selectSegment;
  }

  function multipleTableExternality(schema, parameters) {
    const externalitiesId = parameters.queryParameters.reduce((acum, curr) => {
      curr.externalities.forEach(extern => {
        acum[extern.externalityId] = extern.name;
      });
      return acum;
    }, {});

    const costsWith = `externalityCosts AS (
      SELECT
      ${Object.keys(externalitiesId).map(key => {
        return `MAX(CASE id WHEN '${key}' THEN
            value
        ELSE
            NULL
        END) AS ${externalitiesId[key]}`;
      }).join(', ')}
      FROM ${schema}.__externality
    )`;

    const sqlMid = parameters.queryParameters.map(mode => {
      if (mode.externalities && mode.externalities[0] && mode.externalities[0].customSQL) {
        return mode.externalities[0].customSQL;
      }
      return `(
        SELECT '${mode.name}' AS mode, array_to_json(array_agg(${mode.abr})) AS values FROM (
          SELECT ${mode.externalities.reduce((acum, curr, index) => {
            if (index === 0) {
              acum.push(`${curr.name}.year`);
            }
            acum.push(`${curr.name}.${curr.name}`);
            return acum;
          }, []).join(', ')}
          FROM
            ${mode.externalities.reduce((acum, curr, index) => {
              if (curr.customSQL) {
                acum.push(curr.customSQL);
              } else {
                acum.push(`( SELECT year_id as year, SUM(${curr.table}.${curr.column} * ext.value / 1000000) AS ${curr.name}
                  FROM ${schema}.${curr.table} AS ${curr.table}
                  ${curr.dontJoinCountries === true ? '' : `INNER JOIN
                  countries ON (countries.id = ${curr.table}.${curr.countryColumn ? curr.countryColumn : 'country_id'})`},
                  (SELECT value FROM ${schema}.__externality WHERE id = '${curr.externalityId}') AS ext
                  WHERE ${mode.modes.map(id => `${curr.table}.${mode.modeColumn} = '${id}'`).join(' OR ')}${(mode.modes.length ? ' AND ' : '')}${(curr.customCont ? curr.customCont : 'countries.cont_id')} = '1'
                  GROUP BY year_id, ${(curr.customCont ? curr.customCont : 'cont_id')}
                  ORDER BY year_id
                ) ${curr.name}`);
              }
              return acum;
            }, []).join('\nNATURAL INNER JOIN\n')}
          ) ${mode.abr}
        )`;
      }).join(' UNION ALL ');

    const allSql = `WITH countries AS (
      SELECT DISTINCT country_id as id, ht2006_cont_id as cont_id
      FROM ${schema}._ht2006_level_2
    ),
    ${costsWith}
    SELECT row_to_json(tAgg) FROM (
        SELECT array_to_json(array_agg(row_to_json(tModes))) as data, '${parameters.unit}'::text as unit, '${parameters.name}'::text as name, '${parameters.description}'::text as description FROM (
          ${sqlMid}
        ) tModes
      ) tAgg;`;

    return allSql;
  }


  module.exports = {
    migrate : 'safe',
    autoCreatedAt : false,
    autoUpdatedAt : false,
    autoPK: false,
    connection : 'HTDataStock',
    // Custom Methods to Populate Agreggates in Output Database
    getPopulationLabourByCountries : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT countries.name AS country, upper(countries.id) AS code,
          SUM(${schema}.o_de_output.o_de_pop) AS population,
          SUM(${schema}.o_de_output.o_de_labour) AS labour,
          ${schema}.o_de_output.year_id AS year
          FROM ${schema}.o_de_output
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_de_output.region_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          WHERE countries.cont = '1' AND ${schema}.o_de_output.o_de_pop IS NOT NULL
          GROUP BY countries.name, countries.id,  ${schema}.o_de_output.year_id
          ORDER BY countries.name, ${schema}.o_de_output.year_id;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPopulationByAge : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}._agegroup.name AS agegroup, SUM(${schema}.o_de_output.o_de_pop) AS population,
          ${schema}.o_de_output.year_id AS year
          FROM ${schema}.o_de_output
          INNER JOIN ${schema}._agegroup ON (${schema}._agegroup.id = ${schema}.o_de_output.agegroup_id)
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_de_output.region_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          WHERE countries.cont = '1'
          GROUP BY ${schema}.o_de_output.year_id, ${schema}._agegroup.name
          ORDER BY ${schema}.o_de_output.year_id, ${schema}._agegroup.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            result.rows.sort(sortAgeGroups);
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPopulationByGender : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}._gender.description AS gender, SUM(${schema}.o_de_output.o_de_pop) AS population,
          ${schema}.o_de_output.year_id AS year
          FROM ${schema}.o_de_output
          INNER JOIN ${schema}._gender ON (${schema}._gender.id = ${schema}.o_de_output.gender_id)
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_de_output.region_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          WHERE countries.cont = '1'
          GROUP BY ${schema}.o_de_output.year_id, ${schema}._gender.description
          ORDER BY ${schema}._gender.description, ${schema}.o_de_output.year_id;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getLabourByCountries : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT countries.nam AS country, upper(countries.id) AS code,
          SUM(${schema}.o_de_output.o_de_labour) AS labour, ${schema}.o_de_output.year_id AS year
          FROM ${schema}.o_de_output
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_de_output.region_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          WHERE countries.cont = '1' AND ${schema}.o_de_output.o_de_pop IS NOT NULL
          GROUP BY countries.name, countries.id,  ${schema}.o_de_output.year_id
          ORDER BY countries.name, ${schema}.o_de_output.year_id;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getGVABySectorsAndContries : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT countries.name AS country, upper(countries.id) AS code,
          ${schema}.o_er_sectoral.time_id as year, ${schema}.o_er_sectoral.sector_id::int AS sector,
          SUM(${schema}.o_er_sectoral.o_er_gva)
          FROM ${schema}.o_er_sectoral
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_er_sectoral.region_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          WHERE countries.cont = '1'
          GROUP BY ${schema}.o_er_sectoral.time_id, ${schema}.o_er_sectoral.sector_id::int, countries.name, countries.id
          ORDER BY countries.name, ${schema}.o_er_sectoral.time_id, ${schema}.o_er_sectoral.sector_id::int;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getGVABySectors : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_er_sectoral.time_id as year, ${schema}.o_er_sectoral.sector_id::int AS sector,
          countries.name AS country,
          SUM(${schema}.o_er_sectoral.o_er_gva)
          FROM ${schema}.o_er_sectoral
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_er_sectoral.region_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          WHERE countries.cont = '1'
          GROUP BY ${schema}.o_er_sectoral.time_id, ${schema}.o_er_sectoral.sector_id::int, countries.name
          ORDER BY ${schema}.o_er_sectoral.time_id, ${schema}.o_er_sectoral.sector_id::int, countries.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getGDPPerCapita : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`SELECT tempTablegdp.country, tempTablegdp.time_id as year,
                    tempTablegdp.gdp * 1000000 / tempTablepop.population as gdp_capita, tempTablepop.population::float as pop,
                    tempTablegdp.gdp::float * 1000000 as gdp
                    FROM (SELECT ${schema}.o_er_gdp.time_id, ${schema}._country.remark AS country,
                      SUM(${schema}.o_er_gdp.o_er_gdp) as gdp FROM ${schema}.o_er_gdp
                      INNER JOIN (SELECT SUM(i_de_pop_eurostat) as population, region_id FROM ${schema}.i_de_eurostat GROUP BY region_id) tempTablePop ON (tempTablePop.region_id = ${schema}.o_er_gdp.region_id)
                      INNER JOIN ${schema}._ht2006_level_2 ON (${schema}.o_er_gdp.region_id = ${schema}._ht2006_level_2.id)
                      INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id) GROUP BY country, time_id) tempTablegdp
                      INNER JOIN (SELECT ${schema}._country.remark AS country, ${schema}.o_de_output.year_id, SUM(${schema}.o_de_output.o_de_pop) as population FROM ${schema}.o_de_output
                      INNER JOIN ${schema}._ht2006_level_2 ON (${schema}.o_de_output.region_id = ${schema}._ht2006_level_2.id)
                      INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id) GROUP BY country, ${schema}.o_de_output.year_id) tempTablepop ON (tempTablegdp.country = tempTablepop.country AND tempTablegdp.time_id = tempTablepop.year_id)
                      ORDER by country, year;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getVESStockByCountryAndFuel : function (schema) {
      return new Promise((resolve, reject) => {
        const fuelTypesQuery = `SELECT id, name, description FROM ${schema}._veh_fuel WHERE id::int BETWEEN 0 AND 9 ORDER BY id;`

        const promiseQuery = (sql) => {
          return new Promise((resolve, reject) => {
            this.query(sql, (err, res) => {
              if (err) {
                reject(err);
              } else {
                resolve(res.rows);
              }
            });
          });
        };

        promiseQuery(fuelTypesQuery)
          .then(fuelTypes => {
            const rowsToColumns = fuelTypes.reduce((acum, curr) => {
              acum.push(`MAX(CASE veh_fuel_id WHEN '${curr.id}' THEN o_vs_veh_stock ELSE 0 END) AS ${curr.name}`)
              return acum;
            }, []).join(', ');

            const toJSONObj = `json_build_object(${fuelTypes.reduce((acum, curr) => {
              acum.push(`'${curr.name}', ${curr.name} / total`);
              return acum;
            }, [`'year', year`]).join(', ')})`;

            const sqlQuery = `WITH countries AS (
                SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
                FROM ${schema}._ht2006_level_2
                INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
              ), fuelType AS (
                SELECT json_build_object('key', name, 'description', description) as type FROM ${schema}._veh_fuel WHERE id::int BETWEEN 0 AND 9 ORDER BY id
              ), normalized AS (
                SELECT year, country, SUM(o_vs_veh_stock) AS total, ${rowsToColumns} FROM (
                  SELECT year_id AS year, countries.name AS country, veh_fuel_id, SUM(o_vs_veh_stock) AS o_vs_veh_stock
                  FROM ${schema}.o_vs_veh_stock
                  INNER JOIN countries ON (${schema}.o_vs_veh_stock.country_id = countries.id)
                  WHERE countries.cont = '1' AND
                  ${schema}.o_vs_veh_stock.veh_fuel_id::int BETWEEN 0 AND 9 AND
                  ${schema}.o_vs_veh_stock.mode_vs_id IN ('0', '1', '2', '7', '8')
                  GROUP BY year, country, veh_fuel_id
                  UNION
                  SELECT year_id AS year, 'EU28+NO+CH' AS country, veh_fuel_id, SUM(o_vs_veh_stock) AS o_vs_veh_stock
                  FROM ${schema}.o_vs_veh_stock
                  INNER JOIN countries ON (${schema}.o_vs_veh_stock.country_id = countries.id)
                  WHERE countries.cont = '1' AND
                  ${schema}.o_vs_veh_stock.veh_fuel_id::int BETWEEN 0 AND 9 AND
                  ${schema}.o_vs_veh_stock.mode_vs_id IN ('0', '1', '2', '7', '8')
                  GROUP BY year, country, veh_fuel_id
                ) t_o_vs_stock
                GROUP BY year, country
              )
              SELECT json_build_object('description', 'Fuel split of road vehicle stock in each country', 'unit', 'share', 'fuelTypes', (SELECT json_agg(type) FROM fuelType), 'data', json_agg(t.data)) as json FROM (
                SELECT json_build_object('country', country, 'values', json_agg(${toJSONObj} ORDER BY year)) as data FROM normalized
                GROUP BY country
              ) t`;
              return promiseQuery(sqlQuery);
          })
          .then(res => resolve(res[0].json))
          .catch(reject);
      });
    },
    getVESStockByYearAndFuel : function (schema) {
      return new Promise((resolve, reject) => {
        const fuelTypesQuery = `SELECT id, name, description FROM ${schema}._veh_fuel WHERE id::int BETWEEN 0 AND 9 ORDER BY id;`

        const promiseQuery = (sql) => {
          return new Promise((resolve, reject) => {
            this.query(sql, (err, res) => {
              if (err) {
                reject(err);
              } else {
                resolve(res.rows);
              }
            });
          });
        };

        promiseQuery(fuelTypesQuery)
          .then(fuelTypes => {
            const rowsToColumns = fuelTypes.reduce((acum, curr) => {
              acum.push(`MAX(CASE veh_fuel_id WHEN '${curr.id}' THEN o_vs_veh_stock ELSE 0 END) AS ${curr.name}`)
              return acum;
            }, []).join(', ');

            const toJSONObj = `json_build_object(${fuelTypes.reduce((acum, curr) => {
              acum.push(`'${curr.name}', ${curr.name} / total`);
              return acum;
            }, [`'year', year`]).join(', ')})`;

            const sqlQuery = `WITH countries AS (
                SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
                FROM ${schema}._ht2006_level_2
                INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
              ), fuelType AS (
                SELECT json_build_object('key', name, 'description', description) as type FROM ${schema}._veh_fuel WHERE id::int BETWEEN 0 AND 9 ORDER BY id
              ), normalized AS (
                SELECT year, SUM(o_vs_veh_stock) AS total, ${rowsToColumns} FROM (
                  SELECT year_id AS year, veh_fuel_id, SUM(o_vs_veh_stock) AS o_vs_veh_stock
                  FROM ${schema}.o_vs_veh_stock
                  INNER JOIN countries ON (${schema}.o_vs_veh_stock.country_id = countries.id)
                  WHERE countries.cont = '1' AND
                  ${schema}.o_vs_veh_stock.veh_fuel_id::int BETWEEN 0 AND 9 AND
                  ${schema}.o_vs_veh_stock.mode_vs_id IN ('0', '1', '2', '7', '8')
                  GROUP BY year, veh_fuel_id
                ) t_o_vs_stock
                GROUP BY year
              )
              SELECT json_build_object('description', 'Fuel split of road vehicle stock by year', 'unit', 'share', 'fuelTypes', (SELECT json_agg(type) FROM fuelType), 'data', json_agg(${toJSONObj} ORDER BY year)) as json
              FROM normalized`;
              return promiseQuery(sqlQuery);
          })
          .then(res => resolve(res[0].json))
          .catch(reject);
      });
    },
    getVESStockByCountryAndMode : function (schema) {
      return new Promise((resolve, reject) => {
        const modesTypesQuery = `SELECT id, name, description FROM ${schema}._mode_vs WHERE id IN ('0', '1', '2', '7', '8') ORDER BY id;`

        const promiseQuery = (sql) => {
          return new Promise((resolve, reject) => {
            this.query(sql, (err, res) => {
              if (err) {
                reject(err);
              } else {
                resolve(res.rows);
              }
            });
          });
        };

        promiseQuery(modesTypesQuery)
          .then(modesTypes => {
            const rowsToColumns = modesTypes.reduce((acum, curr) => {
              acum.push(`MAX(CASE mode_vs_id WHEN '${curr.id}' THEN o_vs_veh_stock ELSE 0 END) AS ${curr.name}`)
              return acum;
            }, []).join(', ');

            const toJSONObj = `json_build_object(${modesTypes.reduce((acum, curr) => {
              acum.push(`'${curr.name}', ${curr.name} / total`);
              return acum;
            }, [`'year', year`]).join(', ')})`;

            const sqlQuery = `WITH countries AS (
                SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
                FROM ${schema}._ht2006_level_2
                INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
              ), modeType AS (
                SELECT json_build_object('key', name, 'description', description) as type FROM ${schema}._mode_vs WHERE id IN ('0', '1', '2', '7', '8') ORDER BY id
              ), normalized AS (
                SELECT year, country, SUM(o_vs_veh_stock) AS total, ${rowsToColumns} FROM (
                  SELECT year_id AS year, countries.name AS country, mode_vs_id, SUM(o_vs_veh_stock) AS o_vs_veh_stock
                  FROM ${schema}.o_vs_veh_stock
                  INNER JOIN countries ON (${schema}.o_vs_veh_stock.country_id = countries.id)
                  WHERE countries.cont = '1' AND
                  ${schema}.o_vs_veh_stock.veh_fuel_id::int BETWEEN 0 AND 9 AND
                  ${schema}.o_vs_veh_stock.mode_vs_id IN ('0', '1', '2', '7', '8')
                  GROUP BY year, country, mode_vs_id
                  UNION
                  SELECT year_id AS year, 'EU28+NO+CH' AS country, mode_vs_id, SUM(o_vs_veh_stock) AS o_vs_veh_stock
                  FROM ${schema}.o_vs_veh_stock
                  INNER JOIN countries ON (${schema}.o_vs_veh_stock.country_id = countries.id)
                  WHERE countries.cont = '1' AND
                  ${schema}.o_vs_veh_stock.veh_fuel_id::int BETWEEN 0 AND 9 AND
                  ${schema}.o_vs_veh_stock.mode_vs_id IN ('0', '1', '2', '7', '8')
                  GROUP BY year, country, mode_vs_id
                ) t_o_vs_stock
                GROUP BY year, country
              )
              SELECT json_build_object('description', 'Modal split of road vehicle stock in each country', 'unit', 'share', 'modeTypes', (SELECT json_agg(type) FROM modeType), 'data', json_agg(t.data)) as json FROM (
                SELECT json_build_object('country', country, 'values', json_agg(${toJSONObj} ORDER BY year)) as data FROM normalized
                GROUP BY country
              ) t`;
              return promiseQuery(sqlQuery);
          })
          .then(res => resolve(res[0].json))
          .catch(reject);
      });
    },
    getVESStockByYearAndMode : function (schema) {
      return new Promise((resolve, reject) => {
        const modesTypesQuery = `SELECT id, name, description FROM ${schema}._mode_vs WHERE id IN ('0', '1', '2', '7', '8') ORDER BY id;`

        const promiseQuery = (sql) => {
          return new Promise((resolve, reject) => {
            this.query(sql, (err, res) => {
              if (err) {
                reject(err);
              } else {
                resolve(res.rows);
              }
            });
          });
        };

        promiseQuery(modesTypesQuery)
          .then(modesTypes => {
            const rowsToColumns = modesTypes.reduce((acum, curr) => {
              acum.push(`MAX(CASE mode_vs_id WHEN '${curr.id}' THEN o_vs_veh_stock ELSE 0 END) AS ${curr.name}`)
              return acum;
            }, []).join(', ');

            const toJSONObj = `json_build_object(${modesTypes.reduce((acum, curr) => {
              acum.push(`'${curr.name}', ${curr.name} / total`);
              return acum;
            }, [`'year', year`]).join(', ')})`;

            const sqlQuery = `WITH countries AS (
                SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
                FROM ${schema}._ht2006_level_2
                INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
              ), modeType AS (
                SELECT json_build_object('key', name, 'description', description) as type FROM ${schema}._mode_vs WHERE id IN ('0', '1', '2', '7', '8') ORDER BY id
              ), normalized AS (
                SELECT year, SUM(o_vs_veh_stock) AS total, ${rowsToColumns} FROM (
                  SELECT year_id AS year, mode_vs_id, SUM(o_vs_veh_stock) AS o_vs_veh_stock
                  FROM ${schema}.o_vs_veh_stock
                  INNER JOIN countries ON (${schema}.o_vs_veh_stock.country_id = countries.id)
                  WHERE countries.cont = '1' AND
                  ${schema}.o_vs_veh_stock.veh_fuel_id::int BETWEEN 0 AND 9 AND
                  ${schema}.o_vs_veh_stock.mode_vs_id IN ('0', '1', '2', '7', '8')
                  GROUP BY year, mode_vs_id
                ) t_o_vs_stock
                GROUP BY year
              )
              SELECT json_build_object('description', 'Modal split of road vehicle stock by year', 'unit', 'share', 'modeTypes', (SELECT json_agg(type) FROM modeType), 'data', json_agg(${toJSONObj} ORDER BY year)) as json
              FROM normalized`;
              return promiseQuery(sqlQuery);
          })
          .then(res => resolve(res[0].json))
          .catch(reject);
      });
    },
    getPADpkmByContryAndMode : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          ) SELECT countries.name AS country, upper(countries.id) AS code,
            ${schema}.o_pd_core_pkmorig.year_id as year, ${schema}._mode.name AS mode,
            SUM(${schema}.o_pd_core_pkmorig.o_pd_pkm_orig) as sum
            FROM ${schema}.o_pd_core_pkmorig
            INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_pkmorig.region_o_id)
            INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
            INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_pkmorig.mode_id)
            WHERE countries.cont = '1' AND ${schema}.o_pd_core_pkmorig.mode_id <> '8'
            AND ${schema}.o_pd_core_pkmorig.mode_id <> '9'
            GROUP BY ${schema}.o_pd_core_pkmorig.year_id, ${schema}._mode.name, countries.id, countries.name
            ORDER BY countries.name, ${schema}.o_pd_core_pkmorig.year_id, ${schema}._mode.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADpkmByMode : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_pd_core_pkmorig.year_id as year, ${schema}._mode.name AS mode,
          SUM(${schema}.o_pd_core_pkmorig.o_pd_pkm_orig) as sum
          FROM ${schema}.o_pd_core_pkmorig
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_pkmorig.region_o_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_pkmorig.mode_id)
          WHERE countries.cont = '1'
          AND ${schema}.o_pd_core_pkmorig.mode_id <> '8'
          AND ${schema}.o_pd_core_pkmorig.mode_id <> '9'
          GROUP BY ${schema}.o_pd_core_pkmorig.year_id, ${schema}._mode.name
          ORDER BY ${schema}.o_pd_core_pkmorig.year_id, ${schema}._mode.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADvkmByContryAndMode : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT countries.name AS country, upper(countries.id) AS code,
          ${schema}.o_pd_core_vkmorig.year_id as year, ${schema}._mode.name AS mode,
          SUM(${schema}.o_pd_core_vkmorig.o_pd_vkm_orig) * 2 as sum
          FROM ${schema}.o_pd_core_vkmorig
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_vkmorig.region_o_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_vkmorig.mode_id)
          WHERE countries.cont = '1'
          AND ${schema}.o_pd_core_vkmorig.mode_id <> '8'
          AND ${schema}.o_pd_core_vkmorig.mode_id <> '9'
          GROUP BY ${schema}.o_pd_core_vkmorig.year_id, ${schema}._mode.name, countries.id, countries.name
          ORDER BY countries.name, ${schema}.o_pd_core_vkmorig.year_id, ${schema}._mode.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADvkmByMode : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_pd_core_vkmorig.year_id as year, ${schema}._mode.name AS mode,
          SUM(${schema}.o_pd_core_vkmorig.o_pd_vkm_orig) * 2 AS sum
          FROM ${schema}.o_pd_core_vkmorig
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_vkmorig.region_o_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_vkmorig.mode_id)
          WHERE countries.cont = '1'
          AND ${schema}.o_pd_core_vkmorig.mode_id <> '8'
          AND ${schema}.o_pd_core_vkmorig.mode_id <> '9'
          GROUP BY ${schema}.o_pd_core_vkmorig.year_id, ${schema}._mode.name
          ORDER BY ${schema}.o_pd_core_vkmorig.year_id, ${schema}._mode.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADVehOccupation : function (schema) {
      return new sails.promise(function (resolve, reject) {
        // return computed value as sum to reuse function
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT table_pkm.country, table_pkm.code, table_pkm.year, table_pkm.mode, (CASE WHEN (table_vkm.sum > 0) THEN table_pkm.sum / table_vkm.sum ELSE NULL END) AS sum
          FROM (SELECT countries.name AS country, upper(countries.id) AS code, ${schema}.o_pd_core_pkmorig.year_id as year,
            ${schema}._mode.name AS mode, SUM(${schema}.o_pd_core_pkmorig.o_pd_pkm_orig) sum FROM ${schema}.o_pd_core_pkmorig
            INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_pkmorig.region_o_id)
            INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
            INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_pkmorig.mode_id)
            WHERE countries.cont = '1' AND ${schema}.o_pd_core_pkmorig.mode_id <> '8'
            AND ${schema}.o_pd_core_pkmorig.mode_id <> '9'
            GROUP BY ${schema}.o_pd_core_pkmorig.year_id, ${schema}._mode.name, countries.name, countries.id) table_pkm
          INNER JOIN (SELECT countries.name AS country, ${schema}.o_pd_core_vkmorig.year_id as year,
            ${schema}._mode.name AS mode, SUM(${schema}.o_pd_core_vkmorig.o_pd_vkm_orig)
            FROM ${schema}.o_pd_core_vkmorig
            INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_vkmorig.region_o_id)
            INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
            INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_vkmorig.mode_id)
            WHERE countries.cont = '1' AND ${schema}.o_pd_core_vkmorig.mode_id <> '8'
            AND ${schema}.o_pd_core_vkmorig.mode_id <> '9'
            GROUP BY ${schema}.o_pd_core_vkmorig.year_id, ${schema}._mode.name, countries.name, countries.id
          ) table_vkm ON (table_vkm.country = table_pkm.country AND table_vkm.year = table_pkm.year AND table_vkm.mode = table_pkm.mode)
          ORDER BY country, year, mode;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADTripsByContryAndMode : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT countries.name AS country, upper(countries.id) AS code,
          ${schema}.o_pd_core_tripsorig.year_id as year, ${schema}._mode.name AS mode, SUM(${schema}.o_pd_core_tripsorig.o_pd_trips_orig)
          FROM ${schema}.o_pd_core_tripsorig
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_tripsorig.region_o_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_tripsorig.mode_id)
          WHERE countries.cont = '1' AND ${schema}.o_pd_core_tripsorig.mode_id <> '8'
          AND ${schema}.o_pd_core_tripsorig.mode_id <> '9'
          GROUP BY ${schema}.o_pd_core_tripsorig.year_id, ${schema}._mode.name, countries.id, countries.name
          ORDER BY countries.name, ${schema}.o_pd_core_tripsorig.year_id, ${schema}._mode.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADTripsByMode : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_pd_core_tripsorig.year_id as year, ${schema}._mode.name AS mode,
          SUM(${schema}.o_pd_core_tripsorig.o_pd_trips_orig)
          FROM ${schema}.o_pd_core_tripsorig
          INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_tripsorig.region_o_id)
          INNER JOIN countries ON (${schema}._ht2006_level_2.country_id = countries.id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_tripsorig.mode_id)
          WHERE countries.cont = '1' AND ${schema}.o_pd_core_tripsorig.mode_id <> '8'
          AND ${schema}.o_pd_core_tripsorig.mode_id <> '9'
          GROUP BY ${schema}.o_pd_core_tripsorig.year_id, ${schema}._mode.name
          ORDER BY ${schema}.o_pd_core_tripsorig.year_id, ${schema}._mode.name;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getPADBandByCountryAndMode : function (schema, unit) {
      return new sails.promise(function (resolve, reject) {
        if (unit !== 'vkm' && unit !== 'pkm') {
          return reject(new Error('Invalid unit type for distance band query.'));
        }

        const sqlQuery = `WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT countries.name AS country, upper(countries.id) AS code,
          ${schema}.o_pd_core_${unit}distancebandctry.year_id as year,
          ${schema}._db.name as distance, ${schema}._mode.name AS mode,
          ${schema}.o_pd_core_${unit}distancebandctry.o_pd_${unit}distancebandctry * 2 as sum
          FROM ${schema}.o_pd_core_${unit}distancebandctry
          INNER JOIN countries ON (${schema}.o_pd_core_${unit}distancebandctry.country_o_id = countries.id)
          INNER JOIN ${schema}._db ON (${schema}.o_pd_core_${unit}distancebandctry.db_id = ${schema}._db.id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_pd_core_${unit}distancebandctry.mode_id)
          WHERE countries.cont = '1' AND ${schema}.o_pd_core_${unit}distancebandctry.mode_id <> '8'
          AND ${schema}.o_pd_core_${unit}distancebandctry.mode_id <> '9'
          ORDER BY ${schema}._mode.name, countries.name, ${schema}.o_pd_core_${unit}distancebandctry.year_id, ${schema}._db.id;`

        this.query(sqlQuery, function (err, result) {
          if (err) {
            return reject(new Error(err));
          }
          return resolve(result);
        });
      }.bind(this));
    },
    getFRDTransitPerYearAndMode: function(schema) {
      const sqlQuery = `WITH countries AS (
        SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
        FROM ${schema}._ht2006_level_2
        INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
      ),
      traffic AS (
        SELECT ${schema}.o_fd_transit.year_id AS year,
        ${schema}._mode.name as mode, SUM(${schema}.o_fd_transit.o_fd_vkm_transit) AS vkm,
        SUM(${schema}.o_fd_transit.o_fd_tkm_transit) AS tkm
        FROM ${schema}.o_fd_transit
        INNER JOIN countries ON (countries.id = ${schema}.o_fd_transit.country_id)
        INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_fd_transit.mode_id)
        WHERE countries.cont = '1' AND ${schema}.o_fd_transit.mode_id <> '8' AND
        ${schema}.o_fd_transit.mode_id <> '9'
        GROUP BY year, mode
      ),
      totals AS (
        SELECT traffic.year, SUM(traffic.vkm) AS vkm,
        SUM(traffic.tkm) AS tkm
        FROM traffic
        GROUP BY year
      ),
      aggDataCTYear AS (
        SELECT 'EU28 + CH + NO' AS country, traffic.year AS year,
          json_agg(json_build_object('mode', traffic.mode,
            'tkm', traffic.tkm, 'tkm_share', traffic.tkm / totals.tkm,
            'vkm', traffic.vkm, 'vkm_share', traffic.vkm / totals.vkm,
            'loadFactor', traffic.tkm / traffic.vkm
          ))
        AS "values"
        FROM traffic
        INNER JOIN totals ON (traffic.year = totals.year)
        GROUP BY country, traffic.year
      ),
      aggData AS (
        SELECT aggDataCTYear.country AS country, json_agg(json_build_object(
      'year', aggDataCTYear.year,
      'data', aggDataCTYear.values
    ) ORDER BY aggDataCTYear.year) AS values
        FROM aggDataCTYear
        GROUP BY  aggDataCTYear.country
        ORDER BY aggDataCTYear.country
      )
      SELECT json_build_object(
        'name', 'EU28 + CH + NO freight transit by year and mode',
        'description', 'EU28 + CH + NO freight transit by year and mode',
        'unit', 'count and shares',
        'data', json_agg(json_build_object('country', aggData.country, 'values', aggData.values))
      ) AS json FROM aggData;`;

      return new Promise((resolve, reject) => {
        this.query(sqlQuery, (err, res) => {
          if (err) {
            reject(err);
          } else {
            resolve(res.rows[0].json);
          }
        })
      });
    },
    getFRDTransitByCountryAndMode : function (schema) {
      return new Promise((resolve, reject) => {
        this.query(`WITH countries AS (
          SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
          FROM ${schema}._ht2006_level_2
          INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
        ),
        traffic AS (
          SELECT ${schema}.o_fd_transit.year_id AS year, countries.name AS country,
          ${schema}._mode.name as mode, SUM(${schema}.o_fd_transit.o_fd_vkm_transit) AS vkm,
          SUM(${schema}.o_fd_transit.o_fd_tkm_transit) AS tkm
          FROM ${schema}.o_fd_transit
          INNER JOIN countries ON (countries.id = ${schema}.o_fd_transit.country_id)
          INNER JOIN ${schema}._mode ON (${schema}._mode.id = ${schema}.o_fd_transit.mode_id)
          WHERE countries.cont = '1' AND ${schema}.o_fd_transit.mode_id <> '8' AND
          ${schema}.o_fd_transit.mode_id <> '9'
          GROUP BY year, country, mode
        ),
        totals AS (
          SELECT traffic.year, traffic.country, SUM(traffic.vkm) AS vkm,
          SUM(traffic.tkm) AS tkm
          FROM traffic
          GROUP BY year, country
        ),
        aggDataCTYear AS (
          SELECT traffic.country AS country, traffic.year AS year,
            json_agg(json_build_object('mode', traffic.mode,
              'tkm', traffic.tkm, 'tkm_share', traffic.tkm / totals.tkm,
              'vkm', traffic.vkm, 'vkm_share', traffic.vkm / totals.vkm,
              'loadFactor', traffic.tkm / traffic.vkm
            ))
          AS "values"
          FROM traffic
          INNER JOIN totals ON (traffic.year = totals.year AND traffic.country = totals.country)
          GROUP BY traffic.country, traffic.year
        ),
        aggData AS (
          SELECT aggDataCTYear.country AS country, json_agg(json_build_object(
            'year', aggDataCTYear.year,
            'data', aggDataCTYear.values
          )
          ORDER BY aggDataCTYear.year) AS values,
          CASE
            WHEN aggDataCTYear.country='Switzerland' THEN 2
            WHEN aggDataCTYear.country='Norway' THEN 2
            ELSE 1
          END AS countryPriority
          FROM aggDataCTYear
          GROUP BY  aggDataCTYear.country
          ORDER BY countryPriority, aggDataCTYear.country
        )
        SELECT json_build_object(
          'name', 'EU28 + CH + NO freight transit by year and mode',
          'description', 'EU28 + CH + NO freight transit by year and mode',
          'unit', 'count and shares',
          'data', json_agg(json_build_object('country', aggData.country, 'values', aggData.values))
        ) AS json FROM aggData;;
          `, (err, result) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result.rows[0].json);
          }
        });
      });
    },
    getSAFInjuriesTable : function (schema, table, mode_urban_id) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
          SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
          FROM ${schema}._ht2006_level_2
          INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
        )
        SELECT countries.name AS country, ${schema}.o_sa_acc_${table}.year_id as year,
        SUM(${schema}.o_sa_acc_${table}.o_sa_fat_${table}_pred)::float as fatalities,
        SUM(${schema}.o_sa_acc_${table}.o_sa_serinj_${table}_pred)::float as serious,
        SUM(${schema}.o_sa_acc_${table}.o_sa_slinj_${table}_pred)::float as slight,
        SUM(${schema}.o_sa_costs_road.o_sa_costs_road)::float as costs
        FROM  ${schema}.o_sa_acc_${table}
        INNER JOIN countries ON (countries.id = ${schema}.o_sa_acc_${table}.country_id)
        INNER JOIN ${schema}.o_sa_costs_road ON (${schema}.o_sa_costs_road.mode_urban_id = '${mode_urban_id}' AND ${schema}.o_sa_costs_road.year_id = ${schema}.o_sa_acc_${table}.year_id AND ${schema}.o_sa_costs_road.country_id = ${schema}.o_sa_acc_${table}.country_id)
        GROUP BY countries.name, ${schema}.o_sa_acc_${table}.year_id
        ORDER BY country, year;`, function (err, res) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(res);
          }
        });
      }.bind(this));
    },
    getSAFInjuriesTableByYear : function (schema, table, mode_urban_id) {
      return new Promise((resolve, reject) => {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_sa_acc_${table}.year_id as year,
          SUM(${schema}.o_sa_acc_${table}.o_sa_fat_${table}_pred)::float as fatalities,
          SUM(${schema}.o_sa_acc_${table}.o_sa_serinj_${table}_pred)::float as serious,
          SUM(${schema}.o_sa_acc_${table}.o_sa_slinj_${table}_pred)::float as slight,
          SUM(${schema}.o_sa_costs_road.o_sa_costs_road)::float as costs
          FROM  ${schema}.o_sa_acc_${table}
          INNER JOIN countries ON (countries.id = ${schema}.o_sa_acc_${table}.country_id)
          INNER JOIN ${schema}.o_sa_costs_road ON (${schema}.o_sa_costs_road.mode_urban_id = '${mode_urban_id}' AND ${schema}.o_sa_costs_road.year_id = ${schema}.o_sa_acc_${table}.year_id AND ${schema}.o_sa_costs_road.country_id = ${schema}.o_sa_acc_${table}.country_id)
          WHERE countries.cont = '1'
          GROUP BY ${schema}.o_sa_acc_${table}.year_id
          ORDER BY year;`, (err, res) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(res);
          }
        });
      });
    },
    getSAFFatAndCostTableCountry : function (schema, table) {
      return new sails.promise(function (resolve, reject) {
        this.query(`SELECT ${schema}._country.remark AS country, ${schema}.o_sa_acc_${table}.year_id as year,
                    SUM(${schema}.o_sa_acc_${table}.o_sa_fat_${table}_pred)::float as fatalities, SUM(${schema}.o_sa_acc_${table}.o_sa_acc_cost_${table})::float as cost
                    FROM  ${schema}.o_sa_acc_${table}
                    INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}.o_sa_acc_${table}.country_id)
                    GROUP BY ${schema}._country.remark, ${schema}.o_sa_acc_${table}.year_id
                    ORDER BY country, year;`, function (err, res) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(res);
          }
        });
      }.bind(this));
    },
    getSAFFatAndCostTableWorldRegion : function (schema, table) {
      return new sails.promise(function (resolve, reject) {
        this.query(`SELECT ${schema}._ht2006_cont.description AS country, ${schema}.o_sa_acc_${table}.year_id as year,
                    SUM(${schema}.o_sa_acc_${table}.o_sa_fat_${table}_pred)::float as fatalities, SUM(${schema}.o_sa_acc_${table}.o_sa_acc_cost_${table})::float as cost
                    FROM  ${schema}.o_sa_acc_${table}
                    INNER JOIN ${schema}._ht2006_cont ON (${schema}._ht2006_cont.id = ${schema}.o_sa_acc_${table}.ht2006_cont_id)
                    GROUP BY ${schema}._ht2006_cont.description, ${schema}.o_sa_acc_${table}.year_id
                    ORDER BY country, year;`, function (err, res) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(res);
          }
        });
      }.bind(this));
    },
    getVESCount : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_vs_veh_stock.year_id AS year, countries.name AS country,
          ${schema}.o_vs_veh_stock.o_vs_veh_stock AS vehicles, ${schema}._mode_vs.description AS mode,
          ${schema}._veh_fuel.description AS fuel
          FROM ${schema}.o_vs_veh_stock
          INNER JOIN countries ON (countries.id = ${schema}.o_vs_veh_stock.country_id)
          INNER JOIN ${schema}._mode_vs ON (${schema}._mode_vs.id = ${schema}.o_vs_veh_stock.mode_vs_id)
          INNER JOIN ${schema}._veh_fuel ON (${schema}._veh_fuel.id = ${schema}.o_vs_veh_stock.veh_fuel_id)
          WHERE countries.cont = '1'
          ORDER BY ${schema}.o_vs_veh_stock.year_id, countries.name,
          ${schema}._mode_vs.description, ${schema}._veh_fuel.description;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getVESvkm : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_vs_vkm.year_id AS year, countries.name AS country,
          ${schema}.o_vs_vkm.o_vs_vkm AS vkm, ${schema}._mode_vs.description AS mode,
          ${schema}._veh_fuel.description AS fuel
          FROM ${schema}.o_vs_vkm
          INNER JOIN countries ON (countries.id = ${schema}.o_vs_vkm.country_id)
          INNER JOIN ${schema}._mode_vs ON (${schema}._mode_vs.id = ${schema}.o_vs_vkm.mode_vs_id)
          INNER JOIN ${schema}._veh_fuel ON (${schema}._veh_fuel.id = ${schema}.o_vs_vkm.veh_fuel_id)
          WHERE countries.cont = '1'
          ORDER BY ${schema}.o_vs_vkm.year_id, countries.name, ${schema}._mode_vs.description,
          ${schema}._veh_fuel.description;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getEnvironmentEmissionsByYear : function (schema) {
      const sqlQuery = `WITH countries AS (
          SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
          FROM ${schema}._ht2006_level_2
          INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
        ), pollution AS (
          SELECT ${schema}.o_ev_t.year_id AS year, SUM(${schema}.o_ev_t.o_ev_t_co2) AS co2,
          SUM(${schema}.o_ev_t.o_ev_t_fuel) AS fuel, SUM(${schema}.o_ev_t.o_ev_t_nox) AS nox,
          SUM(${schema}.o_ev_t.o_ev_t_pm) AS pm, SUM(${schema}.o_ev_t.o_ev_t_so2) AS so2
          FROM ${schema}.o_ev_t
          INNER JOIN countries ON (countries.id = ${schema}.o_ev_t.country_id)
          WHERE countries.cont = '1'
          GROUP BY year
          ORDER BY year
        )
        SELECT json_agg(t.json_build_object) AS json FROM
        (SELECT json_build_object('year', year, 'values', json_build_object('fuel', fuel, 'co2', co2, 'nox', nox, 'pm', pm, 'so2', so2))
        FROM pollution
        ORDER BY year) t`;

      return new Promise((resolve, reject) => {
        this.query(sqlQuery, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result.rows[0].json);
          }
        });
      });
    },
    getEnvironmentEmissions : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
          )
          SELECT ${schema}.o_ev_t.year_id AS year, countries.name AS country,
          upper(countries.id) AS code, SUM(${schema}.o_ev_t.o_ev_t_co2) AS co2,
          SUM(${schema}.o_ev_t.o_ev_t_fuel) AS fuel, SUM(${schema}.o_ev_t.o_ev_t_nox) AS nox,
          SUM(${schema}.o_ev_t.o_ev_t_pm) AS pm, SUM(${schema}.o_ev_t.o_ev_t_so2) AS so2
          FROM ${schema}.o_ev_t
          INNER JOIN countries ON (countries.id = ${schema}.o_ev_t.country_id)
          WHERE countries.cont = '1'
          GROUP BY year, country, code
          ORDER BY countries.name, ${schema}.o_ev_t.year_id;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    getEnvironmentCosts : function (schema) {
      return new sails.promise(function (resolve, reject) {
        this.query(`
          WITH countries AS (
              SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
              FROM ${schema}._ht2006_level_2
              INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}._ht2006_level_2.country_id)
            )
            SELECT ${schema}.o_ev_ext.year_id AS year, countries.name AS country,
            upper(countries.id) AS code, ${schema}._mode_vs.description AS mode,
            ${schema}.o_ev_ext.o_ev_ext_cost AS cost
            FROM ${schema}.o_ev_ext
            INNER JOIN countries ON (countries.id = ${schema}.o_ev_ext.country_id)
            INNER JOIN ${schema}._mode_vs ON (${schema}._mode_vs.id = ${schema}.o_ev_ext.mode_vs_id)
            WHERE countries.cont = '1'
            ORDER BY ${schema}.o_ev_ext.year_id, countries.name, ${schema}._mode_vs.description;`, function (err, result) {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(result);
          }
        });
      }.bind(this));
    },
    // Queries to get EuroPop2010 baseline scenario
    getDEMAssumption : function (schema) {
      return new Promise((resolve, reject) => {
        const sqlQuery = `
          WITH countries AS (
            SELECT DISTINCT _country.id AS id, _country.remark AS name, _ht2006_level_2.ht2006_cont_id AS cont
            FROM ${schema}._ht2006_level_2
            INNER JOIN ${schema}._country ON (high_tool._country.id = ${schema}._ht2006_level_2.country_id)
          ),
          parameters AS (
            SELECT json_build_object('id', id, 'description', description, 'unit', unit_id) AS paramDesc
            FROM ${schema}.__parameters
            WHERE id IN ('i_de_life_men', 'i_de_life_women', 'i_de_net_migration', 'i_de_tot_fert_rate')
          ),
          normalizedData AS (
            SELECT json_build_object('country', countries.name, 'values', json_agg(json_build_object('year', ${schema}.i_de_europop_ass.year_id,
            'i_de_life_men', ${schema}.i_de_europop_ass.i_de_life_men, 'i_de_life_women', ${schema}.i_de_europop_ass.i_de_life_women,
            'i_de_net_migration', ${schema}.i_de_europop_ass.i_de_net_migration, 'i_de_tot_fert_rate', ${schema}.i_de_europop_ass.i_de_tot_fert_rate) ORDER BY ${schema}.i_de_europop_ass.year_id))
            AS data
            FROM ${schema}.i_de_europop_ass
            INNER JOIN countries ON (countries.id = ${schema}.i_de_europop_ass.country_id)
            WHERE countries.cont = '1'
            GROUP BY countries.name
          )
          SELECT json_build_object('Description', 'Projected life expectancy, migrations and fertility', 'parameters', (SELECT json_agg(parameters.paramDesc) FROM parameters), 'data', json_agg(normalizedData.data)) AS json FROM normalizedData`;

        this.query(sqlQuery, function (err, res) {
          if (err) {
            reject(err);
          } else {
            if (res.rows && res.rows[0]) {
              resolve(res.rows[0].json);
            } else {
              reject(new Error('Empty Data stock'));
            }
          }
        });
      });
    },
    getECRAssumption : function (schema) {

      function reduceRows(rows, key) {
        return rows.filter(row => {
          return (HTCountryGroups.countryOrderPreference(row.country) === 1);
        })
        .reduce(function (prev, curr) {
          const rowYear = parseInt(curr.year, 10),
            aggIndex = (rowYear - 2010) / 5;

          if (!prev[aggIndex]) {
            prev[aggIndex] = [rowYear, 0, 0];
          }
          prev[aggIndex][1] += curr[key];
          return prev;
        }, [])
        .map(function (item, i, array) {
          if (i === 0) {
            item[2] = 'N/A';
          } else {
            const timeLapse = parseInt(item[0], 10) - parseInt(array[i - 1][0], 10),
              quotient = item[1] / array[i - 1][1],
              average = Math.exp(Math.log(quotient) / timeLapse) - 1;

            item[2] = Math.round(10000 * average) / 100;
          }
          return item;
        });
      }

      return new sails.promise(function (resolve, reject) {
        var EU28Agg = {};

        this.query(`SELECT ${schema}._country.remark as country, ${schema}.i_er_k_tot.time_id as year, SUM(${schema}.i_er_k_tot.i_er_k_tot) as capital
          FROM ${schema}.i_er_k_tot
          INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}.i_er_k_tot.country_id)
          GROUP BY ${schema}._country.remark, ${schema}.i_er_k_tot.time_id
          ORDER BY ${schema}._country.remark, ${schema}.i_er_k_tot.time_id;`, function (err, res) {
          if (err) {
            return reject(err);
          }
          EU28Agg.capital = reduceRows(res.rows, 'capital');

          this.query(`SELECT ${schema}._country.remark as country, ${schema}.i_er_gdp.time_id as year, ${schema}.i_er_gdp.i_er_gdp as gdp
            FROM ${schema}.i_er_gdp
            INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}.i_er_gdp.country_id)
            ORDER BY ${schema}._country.remark, ${schema}.i_er_gdp.time_id;`, function (err, res) {
            if (err) {
              return reject(err);
            }
            EU28Agg.gdp = reduceRows(res.rows, 'gdp');

            this.query(`SELECT ${schema}._country.remark as country, ${schema}.i_er_l_tot.time_id as year, SUM(${schema}.i_er_l_tot.i_er_l_tot) as labour
              FROM ${schema}.i_er_l_tot
              INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.i_er_l_tot.ht2006_level_2_id)
              INNER JOIN ${schema}._country ON (${schema}._ht2006_level_2.country_id = ${schema}._country.id)
              GROUP BY country, year
              ORDER BY ${schema}._country.remark, ${schema}.i_er_l_tot.time_id;`, function (err, res) {
              if (err) {
                return reject(err);
              }
              EU28Agg.labour = reduceRows(res.rows, 'labour');

              this.query(`SELECT ${schema}._country.remark as country, ${schema}.i_er_delta_inf_inv.time_id as year, SUM(${schema}.i_er_delta_inf_inv.i_er_delta_inf_inv) as inv
                FROM ${schema}.i_er_delta_inf_inv
                INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.i_er_delta_inf_inv.ht2006_level_2_id)
                INNER JOIN ${schema}._country ON (${schema}._ht2006_level_2.country_id = ${schema}._country.id)
                GROUP BY country, year
                ORDER BY ${schema}._country.remark, ${schema}.i_er_delta_inf_inv.time_id;`, function (err, res) {
                  if (err) {
                    return reject(err);
                  }
                  EU28Agg.inv_infrastructure = reduceRows(res.rows, 'inv');

                  this.query(`SELECT ${schema}._country.remark as country, ${schema}.i_er_delta_inv.time_id as year, ${schema}.i_er_delta_inv.i_er_delta_inv as inv
                    FROM ${schema}.i_er_delta_inv
                    INNER JOIN ${schema}._country ON (${schema}._country.id = ${schema}.i_er_delta_inv.country_id)
                    ORDER BY ${schema}._country.remark, ${schema}.i_er_delta_inv.time_id;`, function (err, res) {
                    if (err) {
                      return reject(err);
                    }
                    EU28Agg.inv_cap = reduceRows(res.rows, 'inv');

                    this.query(`SELECT ${schema}._country.remark as country, ${schema}.i_er_delta_rtd.time_id as year, SUM(${schema}.i_er_delta_rtd.i_er_delta_rtd) as inv
                      FROM ${schema}.i_er_delta_rtd
                      INNER JOIN ${schema}._ht2006_level_2 ON (${schema}._ht2006_level_2.id = ${schema}.i_er_delta_rtd.ht2006_level_2_id)
                      INNER JOIN ${schema}._country ON (${schema}._ht2006_level_2.country_id = ${schema}._country.id)
                      GROUP BY country, year
                      ORDER BY ${schema}._country.remark, ${schema}.i_er_delta_rtd.time_id;`, function (err, res) {
                        if (err) {
                          return reject(err);
                        }
                        EU28Agg.inv_rtd = reduceRows(res.rows, 'inv');
                    resolve(EU28Agg);
                  });
                }.bind(this));
              }.bind(this));
            }.bind(this));
          }.bind(this));
        }.bind(this));
      }.bind(this));
    },
    getVariableList : function (schema, variables) {
      return new Promise((resolve, reject) => {
        const queryConfig = (Array.isArray(variables) && variables.length > 0 ? {
          text : `SELECT id, description, table_id, unit_id, name FROM ${schema}.__parameters
                  WHERE ${variables.reduce((acum, param, index) => {
                    acum.push(`id = $${index + 1}`);
                    return acum;
                  }, []).join(' OR ')};`,
          values : variables
          } : `SELECT id, description, table_id FROM ${schema}.__parameters;`
        );

        this.query(queryConfig, (err, res) => {
          if (err) {
            return reject(new Error(err));
          }
          resolve(res.rows.reduce(function (prev, next) {
            var i;
            switch (next.id.match(/(^[i,o,p]{1})_\w{2}_/)[1]) {
              case 'i':
                i = 0;
                break;
              case 'o':
                i = 1;
                break;
              default:
                i = 2;
            }
            prev[i].variables.push(next);
            return prev;
          }, [{
            name : 'Inputs',
            variables : []
          },
          {
            name : 'Outputs',
            variables : []
          },
          {
            name : 'Parameters',
            variables : []
          }
          ]));
        });
      });
    },
    getTPMCombinations : function (schema) {
      return new Promise(function (resolve, reject) {
        const query = ` SELECT * FROM (
                          SELECT array_agg(tpm_id ORDER BY ordering) AS policies, policy_lever_id, formula
                          FROM ${schema}.__combination_tpm
                          GROUP BY id, policy_lever_id, formula
                        ) as t
                        ORDER BY t.policy_lever_id ASC, array_length(t.policies, 1) DESC;`
        this.query(query, (err, res) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve(res.rows);
          }
        });
      }.bind(this));
    },
    shiftHypernetworkImpedances : function (schema, firstYear, lastYear, stepSize) {
      return new Promise((resolve, reject) => {
        const columnsToUpdateFragment = [
          'i_pd_imp_ae_dist',
          'i_pd_imp_ae_time',
          'i_pd_imp_delta_los',
          'i_pd_imp_net_dist',
          'i_pd_imp_net_time'
        ]
          .map(field => `${field} = future.${field}`)
          .join(',\n');

        const query = `UPDATE ${schema}.i_pd_core_imp AS present
        SET ${columnsToUpdateFragment}
        FROM ${schema}.i_pd_core_imp AS future
        WHERE
          present.year_id::int >= ${firstYear} AND
          future.region_o_id = present.region_o_id AND
          future.region_d_id = present.region_d_id AND
          future.mode_id = present.mode_id AND
          future.year_id::int = LEAST(
            present.year_id::int + ${stepSize},
            ${lastYear}
          );`;
        this.query(query, err => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      });
    },
    // Externalities
    getAirPassengerPollutionCosts : function (schema) {
      const parameters = {
        name : 'Air pollutions generated by passenger transport in EU28+NO+CH',
        description : `Fossil fuels used in transportation generate a range of substances that cause health problems`,
        unit : 'mEUR',
        externalities : {
          'PM25_pollution' : {
            name : 'pm25Pollution',
            column : 'o_ev_t_pm'
          },
          'Nox_pollution' : {
            name : 'noxPollution',
            column : 'o_ev_t_nox'
          },
          'NMVOC_pollution' : {
            name : 'nmvocPollution',
            column : 'o_ev_t_voc'
          },
          'SO2_pollution' : {
            name : 'so2Pollution',
            column : 'o_ev_t_so2'
          }
        },
        tableId : 'o_ev_t',
        modeColumn : 'mode_vs_id',
        modes : [
          {
            name : 'road',
            modes : ['0', '7', '8']
          },{
            name : 'rail',
            modes : ['9']
          },{
            name : 'air',
            modes : ['6']
          }
        ]
      };

      return new Promise((resolve, reject) => {
        this.query(singleTableExternality(schema, parameters), (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].json ? res.rows[0].json : null));
          }
        });
      });
    },
    getAirFreightPollutionCosts : function (schema) {
      const parameters = {
        description : `Fossil fuels used in transportation generate a range of substances that cause health problems`,
        name : 'Air pollutions generated by freight transport in EU28+NO+CH',
        unit : 'mEUR',
        externalities : {
          'PM25_pollution' : {
            name : 'pm25Pollution',
            column : 'o_ev_t_pm'
          },
          'Nox_pollution' : {
            name : 'noxPollution',
            column : 'o_ev_t_nox'
          },
          'NMVOC_pollution' : {
            name : 'nmvocPollution',
            column : 'o_ev_t_voc'
          },
          'SO2_pollution' : {
            name : 'so2Pollution',
            column : 'o_ev_t_so2'
          }
        },
        tableId : 'o_ev_t',
        modeColumn : 'mode_vs_id',
        modes : [
          {
            name : 'road',
            modes : ['1', '2']
          },{
            name : 'rail',
            modes : ['3']
          },{
            name : 'air',
            modes : ['10']
          },{
            name : 'iww',
            modes : ['4']
          },{
            name : 'sss',
            modes : ['5']
          }
        ]
      };
      return new Promise((resolve, reject) => {
        this.query(singleTableExternality(schema, parameters), (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].json ? res.rows[0].json : null));
          }
        });
      });
    },
    getClimateChangePassengerCosts : function (schema) {
      const parameters = {
        name : 'Climate change: effect of greenhouse gasses generated by passenger transport in EU28+NO+CH',
        description : `Greenhouse gases (CO2, N2O and CH4) from burning fossil fuels used in transport have a long-term effect on global warming and climate change.`,
        unit : 'mEUR',
        externalities : {
          'climate_change' : {
            name : 'climateChange',
            column : 'o_ev_t_co2'
          }
        },
        tableId : 'o_ev_t',
        modeColumn : 'mode_vs_id',
        modes : [
          {
            name : 'road',
            modes : ['0', '7', '8']
          },{
            name : 'rail',
            modes : ['9']
          },{
            name : 'air',
            modes : ['6']
          }
        ]
      };
      return new Promise((resolve, reject) => {
        this.query(singleTableExternality(schema, parameters), (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].json ? res.rows[0].json : null));
          }
        });
      });
    },
    getClimateChangeFreightCosts : function (schema) {
      const parameters = {
        name : 'Climate change: effect of greenhouse gasses generated by freight transport in EU28+NO+CH',
        description : `Greenhouse gases (CO2, N2O and CH4) from burning fossil fuels used in transport have a long-term effect on global warming and climate change.`,
        unit : 'mEUR',
        externalities : {
          'climate_change' : {
            name : 'climateChange',
            column : 'o_ev_t_co2'
          }
        },
        tableId : 'o_ev_t',
        modeColumn : 'mode_vs_id',
        modes : [
          {
            name : 'road',
            modes : ['1', '2']
          },{
            name : 'rail',
            modes : ['3']
          },{
            name : 'air',
            modes : ['10']
          },{
            name : 'iww',
            modes : ['4']
          },{
            name : 'sss',
            modes : ['5']
          }
        ]
      };
      return new Promise((resolve, reject) => {
        this.query(singleTableExternality(schema, parameters), (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].json ? res.rows[0].json : null));
          }
        });
      });
    },
    getMarginalInfrastructureCosts : function (schema) {
      const parameters = {
        name : 'Marginal infrastructure costs: increased maintenance and repairs',
        description : `The marginal cost of infrastructure corresponds to increased maintenance and repairs caused by increased traffic.`,
        unit : 'mEUR',
        queryParameters : [
          {
            name : 'cars',
            externalities : [{
              table : 'o_pd_core_vkmtransitctry',
              column : 'o_pd_vkm_transit',
              externalityId : 'road_car_infr',
              name : 'marginalInfrastructureCosts',
              countryColumn : 'country_t_id'
            }],
            abr : 'ca',
            modes : ['1'],
            modeColumn : 'mode_id'
          },{
            name : 'coaches',
            externalities : [{
              table : 'o_pd_core_vkmtransitctry',
              column : 'o_pd_vkm_transit',
              externalityId : 'road_coach_infr',
              name : 'marginalInfrastructureCosts',
              countryColumn : 'country_t_id'
            }],
            abr : 'co',
            modes : ['7'],
            modeColumn : 'mode_id'
          },{
            name : 'hdv',
            externalities : [{
              customSQL : (function () {
                const requiredExterns = [
                  'road_fre_infr_HDTs_>_3.5t',
                  'road_fre_infr_HDTs_Articulated:_14-20t',
                  'road_fre_infr_HDTs_Articulated:_20-28t',
                  'road_fre_infr_HDTs_Articulated:_28-34t',
                  'road_fre_infr_HDTs_Articulated:_34-40t',
                  'road_fre_infr_HDTs_Articulated:_40-50t',
                  'road_fre_infr_HDTs_Articulated:_50-60t',
                  'road_fre_infr_HDTs_Rigid:_<=_7.5t',
                  'road_fre_infr_HDTs_Rigid:_>32t',
                  'road_fre_infr_HDTs_Rigid:_12-14t',
                  'road_fre_infr_HDTs_Rigid:_14-20t',
                  'road_fre_infr_HDTs_Rigid:_20-26t',
                  'road_fre_infr_HDTs_Rigid:_26-28t',
                  'road_fre_infr_HDTs_Rigid:_28-32t',
                  'road_fre_infr_HDTs_Rigid:_7.5-12t'
                ];

              const orderedSQL = `(SELECT orderTemp.index as mode_vs_id, __externality.value FROM ${schema}.__externality
              INNER JOIN (VALUES ${requiredExterns.map((item, index) => `(${index + 1}, '${item}')`)}) AS orderTemp(index, id)
              ON orderTemp.id = __externality.id
              ORDER BY orderTemp.index ASC) extVals`;

              const vehTypeNumber = `(SELECT year_id as year, mode_vs_id, SUM(o_vs_vkm) AS vehicles
                FROM ${schema}.o_vs_vkm
                WHERE mode_vs_id::int <= 15
                GROUP BY year, mode_vs_id
              ORDER BY year, mode_vs_id::int ASC) yearStock`

              const allVehiclesPerYear = `(SELECT year_id as year, SUM(o_vs_vkm) AS vehicles
                FROM ${schema}.o_vs_vkm
                WHERE mode_vs_id::int <= 15
                GROUP BY year) yearAllStockStock`

              const allTraficPerYear = `(
                SELECT o_fd_transit.year_id as year, SUM(o_fd_transit.o_fd_vkm_transit) AS trafic
                FROM ${schema}.o_fd_transit
                INNER JOIN countries
                ON (${schema}.o_fd_transit.country_id = countries.id )
                WHERE mode_id = '1' AND countries.cont_id = '1'
                GROUP BY year_id, countries.cont_id
              ) allTraficPerYearTab`

              const dotProductPerYear = `(SELECT prodTab.year, SUM(prodTab.prod) as dot FROM
                (
                  SELECT year, yearStock.vehicles * extVals.value as prod FROM
                  ${orderedSQL}
                  INNER JOIN
                  ${vehTypeNumber}
                  ON (yearStock.mode_vs_id::int =  extVals.mode_vs_id::int)
                ) prodTab
                GROUP BY prodTab.year) dotProductTab`;

              return `(
                SELECT 'hdv' AS mode, array_to_json(array_agg(hd)) AS values FROM (
                  SELECT allTraficPerYearTab.year AS year,
                  dotProductTab.dot * allTraficPerYearTab.trafic / (1000000 * yearAllStockStock.vehicles) AS marginalInfrastructureCosts
                  FROM
                    ${allTraficPerYear}
                    INNER JOIN
                      ${dotProductPerYear}
                    ON (dotProductTab.year = allTraficPerYearTab.year)
                    INNER JOIN
                      ${allVehiclesPerYear}
                    ON (yearAllStockStock.year = allTraficPerYearTab.year)
                  ORDER BY year) hd)`

              }())
            }],
            abr : 'hdv'
          },{
            name : 'iww',
            externalities : [{
              table : 'o_fd_transit',
              column : 'o_fd_tkm_transit',
              externalityId : 'iww_infr',
              name : 'marginalInfrastructureCosts'
            }],
            abr : 'iw',
            modes : ['4'],
            modeColumn : 'mode_id'
          }
        ]
      };
      return new Promise((resolve, reject) => {
        this.query(multipleTableExternality(schema, parameters), (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].row_to_json ? res.rows[0].row_to_json : null));
          }
        });
      });
    },
    getUpDownStreamCosts: function (schema) {
      const externalities = [
        'road_car_up_down',
        'road_coach_up_down',
        'road_truck_up_down',
        'rail_pax_up_down',
        'rail_fre_up_down',
        'air_pax_up_down',
        'iww_up_down',
        'sss_up_down'
      ];

      function from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, table, mode, mode_id, externality) {
        const column = (table === 'o_pd_core_vkmtransitctry' ?
        'o_pd_vkm_transit' : 'o_fd_vkm_transit');

        const countryColumn = (table === 'o_pd_core_vkmtransitctry' ?
        'country_t_id' : 'country_id');

        return `${mode} AS (
          SELECT '${mode}'::text AS mode, json_agg(row_to_json(t)) AS values
          FROM (
            SELECT year_id AS year,
            SUM(${column} * externalityCosts.${externality}) / 1000000 AS upDownstream
            FROM ${schema}.${table}
            LEFT OUTER JOIN externalityCosts ON (1 = 1)
            INNER JOIN countries ON (countries.id =  ${table}.${countryColumn})
            WHERE countries.cont_id = '1' AND mode_id = '${mode_id}'
            GROUP BY year
            ORDER BY year
          ) t
        )`;
      }

      function from_o_pd_core_pkmorig_table(schema, mode, mode_id, externality) {
        return `${mode} AS (
          SELECT '${mode}'::text AS mode, json_agg(row_to_json(t)) AS values
          FROM (
            SELECT year_id AS year,
            SUM(o_pd_pkm_orig * externalityCosts.${externality}) / 500000 AS upDownstream
            FROM ${schema}.o_pd_core_pkmorig
            LEFT OUTER JOIN externalityCosts ON (1 = 1)
            INNER JOIN ${schema}._ht2006_level_2 ON (_ht2006_level_2.id =  o_pd_core_pkmorig.region_o_id)
            WHERE _ht2006_level_2.ht2006_cont_id = '1' AND mode_id = '${mode_id}'
            GROUP BY year
            ORDER BY year
          ) t
        )`
      }


      function from_o_fd_od_table(schema, mode, mode_id, externality) {
        return `${mode} AS (
          SELECT '${mode}'::text AS mode, json_agg(row_to_json(t)) AS values
          FROM (
            SELECT year_id AS year,
            SUM(o_fd_vkm_od * externalityCosts.${externality}) / 1000000 AS upDownstream
            FROM ${schema}.o_fd_od
            LEFT OUTER JOIN externalityCosts ON (1 = 1)
            WHERE mode_id = '${mode_id}'
            GROUP BY year
            ORDER BY year
          ) t
        )`
      }


      const modes_tables = [
        from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, 'o_pd_core_vkmtransitctry', 'road', 1, 'road_car_up_down'),
        from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, 'o_pd_core_vkmtransitctry', 'coach', 7, 'road_coach_up_down'),
        from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, 'o_fd_transit', 'truck', 1, 'road_truck_up_down'),
        from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, 'o_pd_core_vkmtransitctry', 'railPassenger', 2, 'rail_pax_up_down'),
        from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, 'o_fd_transit', 'railFreight', 2, 'rail_fre_up_down'),
        from_o_pd_core_pkmorig_table(schema, 'air', 3, 'air_pax_up_down'),
        from_o_pd_core_vkmtransitctry_OR_o_fd_transit_table(schema, 'o_fd_transit', 'iww', 4, 'iww_up_down'),
        from_o_fd_od_table(schema, 'iss', 6, 'sss_up_down')
      ].join(', ');


      const topWithSegment = `WITH countries AS (
        SELECT DISTINCT country_id as id, ht2006_cont_id as cont_id
        FROM ${schema}._ht2006_level_2
      ), externalityCosts AS (
        SELECT
        ${externalities.map(column => `MAX(CASE id WHEN '${column}' THEN
            value
        ELSE
            0
        END) AS ${column}`).join(', ')}
        FROM ${schema}.__externality
      )`;

      const usedModesNames = [
        'road',
        'coach',
        'truck',
        'railPassenger',
        'railFreight',
        'air',
        'iww',
        'iss'
      ];

      const description = `Certain pre- and post transport processes have an environmental impact. These processes include sending the necessary energy to vehicles (fuel/electricity) known as “well-to-tank emissions” and the manufacturing, maintenance and disposal of the vehicles and infrastructures used in transport.`;

      const selectSegment = `SELECT json_build_object('description', '${description}', 'name', 'Up-downstream processes: well-to-tank emissions, manufacturing, maintenance and disposal of vehicles', 'unit', 'mEUR', 'data', json_agg(data)) as json
      FROM (
        ${usedModesNames.map(name => `SELECT * FROM ${name}`).join('\nUNION ALL\n')}
      ) data;`

      const sqlQuery = topWithSegment + ',' + modes_tables + selectSegment;

      return new Promise((resolve, reject) => {
        this.query(sqlQuery, (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].json ? res.rows[0].json : null));
          }
        });
      });
    },
    getAccidentsCosts : function (schema) {
      const parameters = {
        name : 'Accidents',
        description: `More traffic means more accidents, with or without casualties, but in any case with an economic cost.`,
        unit : 'mEUR',
        queryParameters : [
          {
            name : 'car',
            externalities : [{
              table : 'o_sa_acc_car',
              column : 'o_sa_serinj_car_pred',
              externalityId : 'severe_accident',
              name : 'severeInjuries'
            },
            {
              table : 'o_sa_acc_car',
              column : 'o_sa_slinj_car_pred',
              externalityId : 'slight_accident',
              name : 'slightInjuries'
            },
            {
              table : 'o_sa_acc_car',
              column : 'o_sa_fat_car_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'ca',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'bike',
            externalities : [{
              table : 'o_sa_acc_bike',
              column : 'o_sa_fat_bike_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'bi',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'p2w',
            externalities : [{
              table : 'o_sa_acc_p2w',
              column : 'o_sa_fat_p2w_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'p2',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'pedestrian',
            externalities : [{
              table : 'o_sa_acc_ped',
              column : 'o_sa_fat_ped_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'pe',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'publicTransport',
            externalities : [{
              table : 'o_sa_acc_pt',
              column : 'o_sa_fat_pt_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'pt',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'rail',
            externalities : [{
              table : 'o_sa_acc_rail',
              column : 'o_sa_fat_rail_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'ra',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'air',
            externalities : [{
              table : 'o_sa_acc_air',
              column : 'o_sa_fat_air_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities',
              dontJoinCountries : true,
              customCont : 'ht2006_cont_id'
            }],
            abr : 'ai',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'truck',
            externalities : [{
              table : 'o_sa_acc_truck',
              column : 'o_sa_fat_truck_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities'
            }],
            abr : 'tr',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'iww',
            externalities : [{
              table : 'o_sa_acc_iww',
              column : 'o_sa_fat_iww_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities',
              dontJoinCountries : true,
              customCont : 'ht2006_cont_id'
            }],
            abr : 'iw',
            modes : [],
            modeColumn : 'mode_vs_id'
          },{
            name : 'sss',
            externalities : [{
              table : 'o_sa_acc_sss',
              column : 'o_sa_fat_sss_pred',
              externalityId : 'deaths_accident',
              name : 'fatalities',
              dontJoinCountries : true,
              customCont : 'ht2006_cont_id'
            }],
            abr : 'ss',
            modes : [],
            modeColumn : 'mode_vs_id'
          }
        ]
      };
      return new Promise((resolve, reject) => {
        this.query(multipleTableExternality(schema, parameters), (err, res) => {
          if (err) {
            reject(new Error(err))
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].row_to_json ? res.rows[0].row_to_json : null));
          }
        });
      });
    },
    getGeneralizedCosts : function (schema, type) {
      let parameters;
      switch (type) {
        case 'passenger':
          parameters = {
            modes : [
              ['road', 1],
              ['rail', 2],
              ['air', 3],
              ['coach', 7]
            ],
            name : 'Generalised total cost for passenger transport',
            description : 'Generalised total cost for passenger transport',
            unit : 'mEUR'
          };
          break;
        case 'freight':
          parameters = {
            modes : [
              ['road', 1],
              ['rail', 2],
              ['iww', 4],
              ['sss', 6],
              ['maritime', 5]
            ],
            name : 'Generalised total cost for freight transport',
            description : 'Generalised total cost for freight transport',
            unit  : 'mEUR'
          };
          break;
        default:
          return Promise.reject(new Error('Invalid generalized costs type. The valid values are passenger or freight.'));
      }

      const withSegment = (type === 'passenger' ?
        `WITH aggTable AS (
          SELECT year_id as year, mode_id as mode,
          SUM(o_pd_orig_fix_cost + o_pd_orig_time_cost + o_pd_orig_toll_cost + o_pd_orig_var_cost) / 1000000 AS generalizedCosts
          FROM ${schema}.o_pd_core_orig_cost
          INNER JOIN ${schema}._ht2006_level_2
          ON (${schema}._ht2006_level_2.id = ${schema}.o_pd_core_orig_cost.region_o_id)
          WHERE _ht2006_level_2.ht2006_cont_id = '1'
          GROUP BY year, ht2006_cont_id, mode
        )` :
        `WITH aggTable AS (
          SELECT year_id as year, mode_id as mode, SUM(o_fd_t_od * o_fd_total_cost_od) / 1000000 AS generalizedCosts
          FROM ${schema}.o_fd_od
          INNER JOIN ${schema}._ht2006_level_2
          ON (${schema}._ht2006_level_2.id = ${schema}.o_fd_od.region_o_id)
          WHERE _ht2006_level_2.ht2006_cont_id = '1'
          GROUP BY year, ht2006_cont_id, mode
        )`);

      const midSQL = parameters.modes.reduce((acum, curr) => {
        acum.push(`
        (
          SELECT '${curr[0]}' AS mode, array_to_json(array_agg(row_to_json(${curr[0]}))) AS values FROM (
            SELECT year, generalizedCosts
            FROM aggTable
            WHERE aggTable.mode = '${curr[1]}'
            ORDER BY year
          ) ${curr[0]}
        )
        `);
        return acum;
      }, []).join('UNION ALL');

      const completeSQL = `SELECT row_to_json(t) FROM
      (
        SELECT '${parameters.name}' AS name,
        '${parameters.description}' AS description,
        '${parameters.unit}' AS unit,
        array_to_json(array_agg(row_to_json(allModes))) AS data
        FROM
        (
          ${withSegment}
          ${midSQL}
        ) allModes
      ) t;`;

      return new Promise((resolve, reject) => {
        this.query(completeSQL, (err, res) => {
          if (err) {
            reject(new Error(err));
          } else {
            resolve((res.rows && res.rows[0] && res.rows[0].row_to_json ? res.rows[0].row_to_json : null));
          }
        });
      });
    },
    getParameters : function (schema) {
      schema = schema || sails.config.HTConfig.baseSchema;
      return new Promise((resolve, reject) => {
        const pg = require('pg');
        const {HTDataStock} = sails.config.connections;

        pg.connect(HTDataStock, (err, client, done) => {
          if (err) {
            reject(err);
          } else {
            const sqlQuery = `SELECT * FROM ${schema}.__parameters;`;
            client.query(sqlQuery, (err, res) => {
              done();
              if (err) {
                reject(err);
              } else {
                const targetDimensionsDict = res.rows.reduce((acum, curr) => {
                  acum[curr.id] = {
                    dimensions : new Set(),
                    table : curr.table_id
                  };
                  for (let i = 1; i <= curr.nr_of_dimensions; i += 1) {
                    acum[curr.id].dimensions.add(curr[`dimension_${i}_id`]);
                  }
                  return acum;
                }, {});
                resolve(targetDimensionsDict);
              }
            });
          }
        });

      });
    },
    getGeoIdToNUTSMap : function (schema) {
      schema = schema || sails.config.HTConfig.baseSchema;
      return new Promise((resolve, reject) => {
        const pg = require('pg');
        const {HTDataStock} = sails.config.connections;

        pg.connect(HTDataStock, (err, client, done) => {
          if (err) {
            reject(err);
          } else {
            client.query(`SELECT id::int AS id, nuts2_id AS code FROM ${schema}._ht2006_level_2 WHERE nuts2_id IS NOT NULL
            UNION DISTINCT
            SELECT id::int AS id, name AS code FROM ${schema}._country;`, (err, res) => {
              if (err) {
                reject(err)
              } else {
                const dictionary = res.rows.reduce((acum, curr) => {
                  acum[curr.id] = curr.code;
                  return acum;
                }, {});
                resolve((code) => {
                  return (dictionary[code] ? dictionary[code] : '');
                });
              }
            });
          }
        });
      });
    }

  };
  return;
}());
