/**
* HTModelRuns.js
*
* @description :: Runs with existing results.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  meta: {
   schemaName: 'high_tool_interface'
  },
  tableName : 'modelRuns',
  attributes: {
    label : 'string',
    description : 'string',
    parameters : 'json',
    scenario : {
      model: 'Package',
      via: 'id'
    },
    owner : {
      model : 'User',
      via : 'id'
    },
    status: {
      type: 'integer',
      defaultsTo: 0
    },
    step : 'string',
    baseSchema : 'string',
    paused : {
      type : 'boolean',
      defaultsTo: false
    },
    markedDelete : {
      type : 'boolean',
      defaultsTo: false
    }
  },
  codeDictionary: function (code) {
    var result;
    switch (code) {
    case 0:
      result = 'Not created';
      break;
    case 1:
      result = 'Idle';
      break;
    case 10:
      result = 'Paused';
      break;
    case 11:
      result = 'Paused and waiting for Expert Mode';
      break;
    case 2:
      result = 'Active';
      break;
    case 3:
      result = 'Marked for deletion';
      break;
    case 4:
      result = 'Error: Cannot create schema';
      break;
    case 5:
      result = 'Error: Unexpected error while running a model';
      break;
    case 6:
      result = 'Deleted';
      break;
    default:
      result = 'Undefined code';
    }
    return result;
  },
  getRowModificationParameters : function (id) {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.findOne({id : id}),
        HTDataStock.getParameters(),
        StatisticalData.getHierarchicalTrieDictionary()
      ])
      .then(results => {
        const [modelRun, targetDimensionsDict, validTries] = results;
        const {transformModelRunToArguments} = require('high-tool-ds-updater');

        if (modelRun) {
          const transformedInputs = modelRun.parameters.map(parameters => transformModelRunToArguments(parameters, targetDimensionsDict, validTries));
          resolve(transformedInputs);
        } else {
          reject(new TypeError('ModelRun not found'));
        }
      })
      .catch(reject);
    });
  }
};
