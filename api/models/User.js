(function () {
  /**
  * User.js
  *
  * @description :: Uniqueness is by e-mail and passwords are saved using bcrypt.
  * If the hash algorithm is changed, then Passport.js' policies must be updated too.
  * @docs        :: http://sailsjs.org/#!documentation/models
  */

  var bcrypt = require('bcrypt-nodejs');

  function hashPassword(input, next) {
    if (input.password) {
      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(input.password, salt, function () {}, function(err, hash) {
          if (err) {
            sails.log(err);
            next(err);
          } else {
            input.password = hash;
            next(null, input);
          }
        });
      });
    } else {
      next(null, input);
    }
  }

  module.exports = {
    tableName : 'users',
    meta: {
     schemaName: 'high_tool_interface'
    },
    attributes: {
      email : {
        index : true,
        required : true,
        type : 'string',
        unique : true,
      },
      isAdmin : {
        type : 'boolean',
        defaultsTo: false
      },
      name : {
        type : 'string',
        required : true
      },
      schemaQuota : {
        type : 'integer',
        required : true,
        defaultsTo: 3
      },
      password : {
        type : 'string',
        required : true
      },
      toJSON : function () {
        var retObj = this.toObject();
        delete retObj.password;
        return retObj;
      }
    },
    beforeCreate: hashPassword,
    beforeUpdate: hashPassword
  };
}());
