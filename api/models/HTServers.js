/**
* HTServers.js
*
* @description :: Master servers of this instance to fetch updates
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  meta: {
   schemaName: 'high_tool_interface'
  },
  tableName: 'servers',
  attributes: {
    host : {
      type : 'string',
      required : true
    },
    port : 'string',
    description : 'string'
  }
};
