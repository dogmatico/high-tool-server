/*jslint node: true, es6 : true, esnext : true*/
/*globals StatisticalData*/
(function () {
	/**
	 * StatisticalDataController
	 *
	 * @description :: Server-side logic for managing statisticaldatas
	 * @help        :: See http://links.sailsjs.org/docs/controllers
	 */
	'use strict';
	module.exports = {
		create: function (req, res) {
	    StatisticalData
	      .create(req.body)
	      .then(result => {
					return res.status(202).json(result);
	      })
				.catch(err => {
					return res.status(500).send(err);
				});
	  },
	  list: function (req, res) {
	    StatisticalData
	      .findOne({id : req.params.id})
	      .then(result => {
	        return res.json(result);
	      })
				.catch(err => {
					return res.status(500).send(err);
				});
	  },
	  listAll: function (req, res) {
	    StatisticalData
	      .find()
				.then(results => {
	        return res.json(results);
	      })
				.catch(err => {
					return res.status(500).send(err);
				});
	  }
};
}());
