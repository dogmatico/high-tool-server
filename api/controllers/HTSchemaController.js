/*jslint node: true, es6 : true, esnext : true*/
(function () {
	/**
	 * HTSchemaController
	 *
	 * @description :: Server-side logic for managing Htschemas
	 * @help        :: See http://links.sailsjs.org/docs/controllers
	 */

	'use strict';

	function normalizedNode(node) {
		return {
			label: node.label || 'default',
			shortLabel: node.shortLabel || 'short',
			description: node.description || 'description',
			unit: node.unit || 'unit',
			source: node.source || 'source',
			min: node.min || 0,
			max: node.max || 0,
			baseline: node.baseline || 0,
			hasChildren : node.hasChildren || false,
			target : node.target || {},
			parent: node.parent || null,
			root: node.root || false,
			uuid: node.uuid
		};
	}

	module.exports = {

	  /**
	   * `HTSchemaController.print()`
	   */

	  printAll: function (req, res) {
	    return res.json(sails.schema);
	  },

	  /**
	   * `HTSchemaController.printSubGraph()`
	   */

	  printSubGraph: function(req, res) {
	    var nodeList = [];
	    var adjList = [];
	    var localIdTrie = new sails.Trie();
	    var lastLocalId = 0;

	    var DFS = function(nodeId) {
	      var nodeSubgraphId;

	      if(localIdTrie.get(sails.schema.nodeAttributes[nodeId].uuid) === false) {
	        localIdTrie.add(sails.schema.nodeAttributes[nodeId].uuid, lastLocalId);
	        nodeList[lastLocalId] = {};
	        Object.keys(sails.schema.nodeAttributes[nodeId]).forEach(function(key) {
	          nodeList[lastLocalId][key] = sails.schema.nodeAttributes[nodeId][key];
	        });
	        nodeList[lastLocalId].id = lastLocalId;
	        nodeSubgraphId = lastLocalId;
	        adjList[lastLocalId] = [];
	        lastLocalId++;
	      } else {
	        nodeSubgraphId = localIdTrie.get(sails.schema.nodeAttributes[nodeId].uuid);
	      }

	      for(var i = 0, ln = sails.schema.adjList[nodeId].length; i < ln; i++) {
	        var localIdChild = sails.schema.adjList[nodeId][i];
	        DFS(localIdChild);
	        adjList[nodeSubgraphId][i] = localIdTrie.get(sails.schema.nodeAttributes[localIdChild].uuid);
	      }

	      return;
	    };

	    var localIdRoot = sails.uuidTrie.get(req.params.rootNode);

	    if(req.params && req.params.rootNode && localIdRoot !== false) {
	      DFS(localIdRoot);
	      nodeList.sort(function (a,b) {
	        return a.id - b.id;
	      });
	    }


	    return res.json({
	        rootNodes : [{
	            localId: 0,
	            userId: req.params.rootNode
	        }],
	        adjList : adjList,
	        nodeAttributes : nodeList
	      });
	  },
		/**
		 * Returns all the TPM in an object. The properties match the 3 letter
		 * identifier of the HIGH-TOOL modules
		 * @param  {express request} req
		 * @param  {express response} res
		 * @return {json}     Object with the keys module > __parameter.
		 */
		getAllFamilies : function (req, res) {
			return res.json(sails.schema.allTPMByFamily);
		},
		getLeverList : function (req, res) {
			res.json(sails.schema.levers);
		},
		getSpatialAndTemporalDimensions : function (req, res) {
			res.json(sails.schema.spatialAndTemporalDimensions);
		},
		getTPMCombinations : function (req, res) {
			HTDataStock.getTPMCombinations(sails.config.HTConfig.baseSchema)
				.then(combinations => res.json(combinations))
				.catch(err => res.status(500).send(err));
		}
	};
}());
