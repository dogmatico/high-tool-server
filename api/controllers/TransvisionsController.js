/*jslint node: true, indent: 2 */
/*globals sails, Package*/
(function () {
  "use strict";
  /**
   * TransvisionsController
   *
   * @description :: Server-side computing of tranvisions module.
   * @help        :: See http://links.sailsjs.org/docs/controllers
   */
  function getPackageRoot(packageType) {
    var rootNode;
    switch (packageType) {
    case 'framework':
      rootNode = 'FCROOT';
      break;
    case 'policy':
      rootNode = 'TPROOT';
      break;
    case 'scenario':
      rootNode = 'SCROOT';
      break;
    case 'output':
      rootNode = 'OUROOT';
      break;
    default:
      rootNode = null;
    }
    return rootNode;
  }


  module.exports = {
    run : function (req, res) {
      if (!req.params.id) {
        res.status(400);
        return res.json({message: 'Include a package Id.'});
      }
      
      function checkOwner(req, packageData) {
        if (req.user.isAdmin === true || req.user.id === packageData.owner || packageData.public === true) {
            return sails.promise.resolve(packageData.nodeList);
          }
        
      }
      
      Package.findOne({id : req.params.id})
        .populate('owner')
        .then(function (result) {
          if (result === undefined) {
            return sails.promise.reject({code: 404, message : "Package not found."});
          }
          if (result.rootNode === 'SCROOT') {
            return Package.find({or: [{id : result.subpackages[0]}, {id : result.subpackages[1]}]})
              .then(function (subpack) {
                var i, j, ln, retObj = {};
                for (i = 0; i < 2; i++) {
                  if (req.user.isAdmin !== true && req.user.id !== subpack[i].owner && req.user.id === subpack[i].public !== true) {
                    return sails.promise.reject({code: 403, message : "You are not the owner of the package."});
                  }
                  for (j = 0, ln = subpack[i].nodeList.length; j < ln; j += 1) {
                    retObj[subpack[i].nodeList[j].uuid] = subpack[i].nodeList[j].values.terminalValue;
                  }
                }
                return sails.promise.resolve(retObj);
              });
          }
          return sails.promise.reject({code : 401, message : "Only Scenarios can be inputs of Tranvisions+."});
        })
        .then(function (nodes) {
          var transvisions = require('high-tool-transvisions');
          return res.json(transvisions.compute(45, nodes));
        })
        .catch(function (err) {
          res.status(err.code);
          return res.json(err);
        });
    }
  };
}());
