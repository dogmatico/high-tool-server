(function () {
  'use strict';

  /**
   * Handle subscriptions to notification service
  */

  const loggedAdmins = new Set();
  const loggedUsers = new Map();
  const loggedUsersReverse = new Map();

  module.exports = {
    loggedAdmins : loggedAdmins,
    loggedUsers : loggedUsers,
    loggedUsersReverse : loggedUsersReverse,
    sendMessageToOwner : (ownerId, message, data) => {
      if (loggedUsers.has(ownerId)) {
        const socketId = loggedUsers.get(ownerId);
        sails.sockets.broadcast(socketId, message, data);
      }
    },
    sendMessageToAdmins : (message, data) => {
      sails.sockets.broadcast('admin', message, data);
    },
    subscribeToNotification : (req, res) => {
      if (!req.isSocket) {
        return res.badRequest();
      }

      if (req.user.isAdmin) {
        sails.sockets.join(req, 'admin');
        loggedAdmins.add(sails.sockets.getId(req));
      } else {
        const socketId = sails.sockets.getId(req);
        loggedUsers.set(req.user.id, socketId);
        loggedUsersReverse.set(socketId, req.user.id);
      }

      res.send('Ok');
    },
    unSubscribeToNotification : (req, res) => {
      if (!req.isSocket) {
        return res.badRequest();
      }
      const socketId = sails.sockets.getId(req);
      if (loggedAdmins.has(socketId)) {
        sails.sockets.leave(req, 'admin');
        loggedAdmins.delete(req.user.id);
      }
      loggedUsers.delete(req.user.id);

      res.send('Ok');
    }
  };

}());
