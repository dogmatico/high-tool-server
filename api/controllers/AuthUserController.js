/*jslint node: true, es6 : true, esnext : true*/
/*globals sails*/
(function () {
  /**
  * AuthController
  *
  * @module :: Controller
  * @description :: A set of functions called `actions`.
  *
  * Actions contain code telling Sails how to respond to a certain type of request.
  * (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
  *
  * You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
  * and/or override them with custom routes (`config/routes.js`)
  *
  * NOTE: The code you write here supports both HTTP and Socket.io automatically.
  *
  * @docs :: http://sailsjs.org/#!documentation/controllers
  */
  "use strict";
  var passport = require('passport'),
    jwt = require('jsonwebtoken'),
    secret = sails.config.JWT.secretKey,
    token;

  module.exports = {
    login: function (req, res) {
      passport.authenticate('local', function (err, user) {
        if (!user) {
          res.send({
            success : false,
            message : 'Invalid Password'
          });
        } else if (err) {
          res.send({
            success : false,
            message : 'Error while login'
          });
        } else {
          token = jwt.sign(user, secret, {expiresIn: 60 * 60 * 24});
          return res.send({
            success: true,
            message: 'Ok',
            user : user,
            token : token
          });
        }
      })(req, res);
    },
    logout : function (req, res) {
      req.logout();
      res.send({
        success : true,
        message : 'Logout successful'
      });
    },
    _config : {}
  };
  return;
}());
