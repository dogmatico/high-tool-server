/*jslint node: true, es6 : true, esnext : true*/
/*globals sails, HTModelRuns, HTDataStock, User, Package*/
/**
 * RunController
 *
 * @description :: Server-side logic for managing runs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
'use strict';

 function checkOwnerShip(req, res, cb) {
 	HTModelRuns.findOne({id: req.params.runId})
 		.then(function (runMetadata) {
 			if (runMetadata) {
 				if (req.user.isAdmin || req.user.id === runMetadata.owner) {
 					runMetadata.name = sails.config.HTConfig.schemaPrefix + runMetadata.id;
 					cb(req, res, runMetadata);
 				} else {
 					res.status(403);
 					return res.json({message : 'Access denied.'});
 				}
 			} else {
 				res.status(404);
 				return res.json({message : 'Schema not found.'});
 			}
 		})
 		.catch(function (err) {
 			res.status(500);
 			return res.json({message : err});
 		});
 }

function checkUserQuota(userId) {
  return new sails.promise(function (resolve) {
    if (sails.config.HTConfig.userQuota === -1) {
      resolve(true);
    } else {
      sails.promise.all([
        User.findOne({id: userId}),
        HTModelRuns.find({owner: userId})
      ])
      .then(function (results) {
        if (results[1].length < results[0].schemaQuota) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(() => resolve(false));
    }
  });
}

module.exports = {
	listOwned : function (req, res) {
		var queryOptions = {};
		if (!req.user.isAdmin) {
			queryOptions.owner = req.user.id;
		}

		HTModelRuns.find(queryOptions)
			.populate("owner")
			.then(function (results) {
				res.json(results.map(function (item) {
					item.owner = {
						name : item.owner.name,
						id : item.owner.id
					};
					item.status = HTModelRuns.codeDictionary(item.status);
					['parameters', 'TPM'].forEach(key => delete item[key]);
					return item;
				}));
			})
			.catch(function (err) {
				res.status(500);
				res.json({message : err});
			});
	},
  changeStep : function (req, res)  {
    checkOwnerShip(req, res, (req, res, runMetadata) => {

      if (runMetadata.paused && runMetadata.status !== 2) {
        if (sails.config.HTConfig.getStepNumber(req.body.step) === null) {
          return res.status(400).send('Invalid step id.')
        }

        HTModelRuns.update({id : runMetadata.id}, {
          step : req.body.step
        })
          .then(updatedRuns => {
            if (updatedRuns && updatedRuns[0]) {
              return res.status(200).json(updatedRuns[0]);
            }
            return res.status(404).send('Did not found the run to update.')
          })
          .catch(err => {
            res.status(500).send(err)
          });
      } else {
        return res.status(503).send('To modify the step of a run, it must be paused and inactive. Try latter.');
      }
    });
  },
  deleteRun : function (req, res) {
    checkOwnerShip(req, res, function(req, res, runMetadata) {
			HTModelRuns.update({id: runMetadata.id}, {
          paused : true,
          status : 3
        })
        .then(runMetadata => {
          sails.runAndDeleteQueue.delete(runMetadata[0].id);
          res.json({message : 'Marked for deletion'});
        });
		});
  },
	getDefinition : function (req, res) {
		checkOwnerShip(req, res, function(req, res, runMetadata) {
			return res.json(runMetadata);
		});
	},
  getSteps : function (req, res) {
    return res.json(sails.config.HTConfig.steps.map(item => item.id));
  },
  pauseRun : function (req, res) {
    checkOwnerShip(req, res, function(req, res, runMetadata) {
      const newRunState = {
        paused : true
      };

      if (runMetadata.step !== 'EXPERTMODE') {
        newRunState.status = (sails.runAndDeleteQueue.markedDeletion.has(runMetadata.id)?
          3 : sails.runAndDeleteQueue.active === runMetadata.id ?
          2 : 10
        );
      } else {
        newRunState.status = 11;
      }

      HTModelRuns.update({id: runMetadata.id}, newRunState)
        .then(updatedRuns => {
          sails.runAndDeleteQueue.next();
          updatedRuns[0].status = HTModelRuns.codeDictionary(updatedRuns[0].status);
          res.json(updatedRuns[0]);
        })
        .catch((err) => res.json(err));
		});
  },
  resumeRun : function (req, res) {
    checkOwnerShip(req, res, function(req, res, runMetadata) {
      const newRunState = {
        paused : false
      };

      if (runMetadata.status !== 11) {
        newRunState.status = (sails.runAndDeleteQueue.markedDeletion.has(runMetadata.id) ? 1 : 3);
      }

      HTModelRuns.update({id: runMetadata.id}, newRunState)
        .then(updatedRuns => {
          sails.runAndDeleteQueue.next();
          updatedRuns[0].status = HTModelRuns.codeDictionary(updatedRuns[0].status);
          res.json(updatedRuns[0]);
        })
        .catch((err) => res.json(err));
		});
  },
  runPackage : function (req, res) {
    const firstStep = sails.config.HTConfig.steps[0];
    const checkTPMReg = new RegExp(/^(TPM|CUSTOMTPM|COMBINEDTPM).*/);
    const newRunParameters = {
      step : firstStep.id,
      description : firstStep.description,
      parameters : [],
      baseSchema : sails.config.HTConfig.baseSchema,
      expertMode : false
    };

    if (req.body.options && req.body.options.length) {
      req.body.options
        .filter(option => {
          return (option.id === 'expertMode' || option.id === 'policy86');
        })
        .forEach(option => {
          newRunParameters[option.id] = option.value;
        });
    }

    checkUserQuota(req.user.id)
      .then(hasQuota => {
        if (!hasQuota) {
          return Promise.reject({status : 401, msg : 'Exceded database quota'});
        } else {
          return Package.findOne({id : req.params.packageId});
        }
      })
      .then(packageData => {
        if (packageData) {
          newRunParameters.owner = req.user.id;
          newRunParameters.label = packageData.headline;
          newRunParameters.TPM = packageData.rootNode;
          newRunParameters.rootNode = packageData.rootNode;

          if (!checkTPMReg.test(packageData.rootNode)) {
            return Promise.reject({status : 400, msg : 'The package is of the wrong type.'});
          }


          if (packageData.rootNode === 'COMBINEDTPMROOT') {
            const usedLevers = Object.keys(packageData.data.values);
            newRunParameters.TPM = packageData.data.selectedTPM;
            newRunParameters.parameters = usedLevers.map(leverId => {
              const leverData = sails.schema.levers.leverList[sails.schema.levers.leverDictionary[leverId]];
              const dataToPush = {
                values : packageData.data.values[leverId],
                uuid : leverId
              };
              ['label', 'target', 'min', 'max', 'initialValue'].forEach(key => {
                dataToPush[key] = leverData[key];
              });

              return dataToPush;
            },[]);
          } else {
            newRunParameters.parameters = newRunParameters.parameters.concat(packageData.nodeList).map(lever => {
                const leverData = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
                lever.initialValue = (leverData.initialValue === 0 || leverData.initialValue ?
                  leverData.initialValue : (lever.target.__is_relative ? 1 : 0));
                return lever;
            });

          }

          return new Promise((resolve, reject) => {
            if (packageData.subpackages && packageData.subpackages.length) {
              Package.find({id : packageData.subpackages})
                .then(subpackages => {
                  subpackages.filter(subpack => {
                    return checkTPMReg.test(subpack.rootNode);
                  })
                  .forEach(subpack => {
                    newRunParameters.parameters = newRunParameters.parameters.concat(subpack.nodeList);
                  });
                  resolve(newRunParameters);
                })
                .catch(err => {
                  reject(err);
                });
            } else {
              resolve(newRunParameters);
            }
          });
        } else {
          return Promise.reject({status : 404, msg : 'Package not found'});
        }
      })
      .then(newRunParameters => {
        /**
         * Add leverless policies to options according to package type:
         * CUSTOM: CUSTOMTPM_NOLEVER_TPM_86 is in parameters list,
         * SINGLE : TPM === TPM_TPM_86
         * combined : "86" in TPM[]
         */
        for (let i = 0, ln = newRunParameters.parameters.length, found = false; i < ln && !found; i += 1) {
          if (newRunParameters.parameters[i].uuid === 'CUSTOMTPM_NOLEVER_TPM_86') {
            found = true;
            newRunParameters.policy86 = true;
          }
        }
        if ((Array.isArray(newRunParameters.TPM) && newRunParameters.TPM.indexOf('86') !== -1) ||
        newRunParameters.TPM === 'TPM_TPM_86') {
          newRunParameters.policy86 = true;
        }
        return HTModelRuns.create(newRunParameters);
      })
      .then(newRun => {
        sails.runAndDeleteQueue.run(newRun.id);
        res.status(201).send(newRun);
      })
      .catch(err => {
        if (err.status) {
          return res.status(err.status).send(err.msg);
        }
        return res.status(500).send(err);
      });
  },
  runScenario : function (req, res) {
    const firstStep = sails.config.HTConfig.steps[0];

    const newRunParameters = {
      step : firstStep.id,
      description : firstStep.description,
      scenario : req.params.scenarioId,
      parameters : [],
      baseSchema : null,
      expertMode : false
    };

    if (req.body.options && req.body.options.length) {
      req.body.options
        .filter(option => {
          return (option.id === 'expertMode');
        })
        .forEach(option => {
          newRunParameters[option.id] = option.value;
        });
    }

    checkUserQuota(req.user.id)
      .then(hasQuota => {
        if (!hasQuota) {
          return sails.promise.reject({status : 401, msg : 'Exceded database quota'});
        } else {
          return Package.findOne({id : req.params.scenarioId});
        }
      })
      .then(scenarioData => {
        if (scenarioData) {
          newRunParameters.owner = req.user.id;
          newRunParameters.label = scenarioData.headline;
          return Package.find({id : scenarioData.subpackages});
        } else {
          return sails.promise.reject({status : 404, msg : 'Scenario not found'});
        }
      })
      .then(subpackages => {
        if (subpackages.length) {
          const checkTPMReg = new RegExp(/^(TPM|CUSTOMTPM).*/);
          subpackages.forEach(pack => {
            if (checkTPMReg.test(pack.rootNode)) {
              newRunParameters.parameters = newRunParameters.parameters.concat(pack.nodeList);
            }

            if (pack.rootNode === 'FCROOT') {
              newRunParameters.baseSchema = pack.nodeList[0].schema;
            }
          });

          if (!newRunParameters.parameters.length || !newRunParameters.baseSchema) {
            return sails.promise.reject({status : 401, msg : 'Wrong scenario definition. Missing parameters or base schema.'});
          }
          return HTModelRuns.create(newRunParameters);
        } else {
          return sails.promise.reject({status : 401, msg : 'Wrong scenario definition. Add subpackages.'});
        }
      })
      .then(newRun => {
        sails.runAndDeleteQueue.run(newRun.id);
        res.status(201).send('New run added to run queue.');
      })
      .catch(err => {
        if (err.status) {
          return res.status(err.status).send(err.msg);
        }
        return res.status(500).send(err);
      });
  },
	runTPM : function (req, res) {
		if(req.user) {
			sails.promise.all([
	      User.findOne({id: req.user.id}),
	      HTModelRuns.find({owner: req.user.id})
	    ])
	    .then(function (results) {
	      if ((sails.config.HTConfig.userQuota === -1 || (results[1].length < results[0].schemaQuota) && req.body.TPM)) {
					sails.log('Creating run data.');
	        return HTModelRuns.create({
						owner : req.user.id,
						label: req.body.label,
						TPM : req.body.TPM,
						parameters : req.body.inputs,
            expertMode : req.body.expertMode,
						step : 'NEW',
            description : 'Create new modified schema'
					});
	      }
	      res.status(401);
	      return sails.promise.reject({message : 'Exceded database quota or wrong TPM definition'});
	    })
      .then(function (newRun) {
        sails.runAndDeleteQueue.run(newRun.id);
        res.status(201).send('New run added to run queue.');
      })
			.catch(function (err) {
				console.error(err);
				res.status(500);
				res.json({error: err});
			});
		} else {
			res.status(401);
			res.json({error: 'Only authenticated users can run TPMs and scenarios.'});
		}
	},
  printReadyToOutput : function (req, res) {
    let searchParams = {
      step : 'DONE'
    };

    if (!req.user.isAdmin) {
      searchParams.owner = req.user.id;
    }

    HTModelRuns
      .find(searchParams)
      .populate('owner')
      .populate('scenario')
      .then(doneRuns => {
        return res.json(doneRuns.map(item => {
          ['parameters',
            'scenario',
            'status',
            'step',
            'baseSchema',
            'paused',
            'type',
            'markedDelete'].forEach(key => {
              delete item[key];
            });

            delete item.owner.isAdmin;
            delete item.owner.createdAt;

            item.headline = item.label;
            return item;
        }));
      })
      .catch(err => {
        return res.status(500).send(err);
      });
  }
};
