(function () {
  'use strict';

  const topojson = require('topojson');
  /**
   * Function to get the Table and Where object to pass to
   * GISService.get(Geo/Topo)Json
   * @param  {string} regionId    Defines the World region to map (EU28, World, etc.)
   * @param  {string} level       Administrative division to show (E.g. NUTS0)
   * @return {[string : tableName, string : id_property, Object[] : where]} To be used in the function
   */
  function getTableAndConstrictions(regionId, admLevel) {
    let retArr = null;
    if (regionId === 'EU28') {
      switch (admLevel) {
        case 'NUTS2':
          retArr = ['_ht2006_level_2', 'nuts2_id', ]
          break;
      }
    }
    return retArr;
  }

  module.exports = {
    downloadMap : function (req, res) {
      const format = req.query.format || 'geojson';

      if (req.params.regionId === 'EU28') {
        if (req.params.admLevel === 'NUTS2') {
          GISService.getNUTS2(['nuts2_id', 'name'],
            [{type : 'AND', comparator: '=', column_name : 'ht2006_cont_id', value : 1}],
            format, (err, map) => {
              if (err) {
                return res.status(500).send(err);
              }
              return res.json(map);
            });
        }
        if (req.params.admLevel === 'NUTS0') {
          GISService.getNUTS0(['id'],
            [{type : 'AND', comparator: '<', column_name : 'id', value : 1000, cast : 'int'}],
            format, (err, map) => {
              if (err) {
                return res.status(500).send(err);
              }
              return res.json(map);
            });
        }
      } else {
        return res.status(400).send('Region not found.')
      }

    },
    getHyperNetwork : function (req, res) {
      const where = Object.keys(req.query).reduce((acum, queryKey) => {
        acum.push({
          type : 'AND',
          comparator : '=',
          column_name : queryKey,
          value : req.query[queryKey]
        });
        return acum;
      }, []);

      GISService.getHyperNetwork(sails.config.HTConfig.baseSchema, where, (err, HyperNetwork) => {
        if (err) {
          return res.status(500).send(err);
        }
        return res.json(HyperNetwork);
      });
    }
  };
}());
