/*jslint node: true, es6 : true, esnext : true*/
/*globals sails, HTModelRuns, HTDataStock*/
(function () {
  /**
  * HTDataStockController
  *
  * @description :: Server-side logic for managing HT Data Stock and
  * PostgreSQL schemata.
  * @help        :: See http://links.sailsjs.org/docs/controllers
  */
  'use strict';

  const pg = require('pg');
  const QueryStream = require('pg-query-stream');
  var Promise = require('bluebird');

  const sheetsName = {
    'DEM' : 'DEM_tables',
    'ECR' : 'ECR_tables',
    'PAD' : 'PAD_tables',
    'FRD' : 'FRD_tables',
    'VES' : 'VES_tables',
    'SAF' : 'SAF_tables',
    'ENV' : 'ENV_tables'
  };

  function countryOrderPreference(country) {
    var priority;

    switch (country) {
    case 'EU28':
    case 'EU28+NO+CH':
      priority = 0;
      break;
    case 'Austria':
    case 'Belgium':
    case 'Bulgaria':
    case 'Cyprus':
    case 'Czech Republic':
    case 'Czech Rep.':
    case 'Germany':
    case 'Denmark':
    case 'Estonia':
    case 'Spain':
    case 'Finland':
    case 'France':
    case 'Greece':
    case 'Croatia':
    case 'Hungary':
    case 'Ireland':
    case 'Italy':
    case 'Lithuania':
    case 'Luxembourg':
    case 'Luxembourg (Grand-Duche)':
    case 'Latvia':
    case 'Malta':
    case 'Netherlands':
    case 'Poland':
    case 'Portugal':
    case 'Romania':
    case 'Sweden':
    case 'Slovenia':
    case 'Slovakia':
    case 'United Kingdom':
      priority = 1;
      break;
    case 'Switzerland':
    case 'Norway':
      priority = 2;
      break;
    case 'Iceland':
    case 'Turkey':
    case 'Azerbaijan':
    case 'Bosnia Herzegovina':
    case 'Andorra':
    case 'Monaco':
    case 'Albania':
    case 'Armenia':
    case 'San Marino':
    case 'Serbia':
    case 'Liechtenstein':
    case 'Montenegro':
    case 'TFYR of Macedonia':
    case 'Belarus':
    case 'Ukraine':
    case 'Moldova':
    case 'Morocco':
    case 'Georgia':
    case 'Iran':
    case 'Uzbekistan':
    case 'Turkmenistan':
    case 'Russian Federation':
    case 'Kazakhstan':
      priority = 3;
      break;
    case 'CIS':
    case 'Antarctica':
    case 'Europe, Rest':
    case 'America Canada':
    case 'America USA':
    case 'America Mexico':
    case 'America Central':
    case 'America Caribbean':
    case 'America South':
    case 'Asia/Pacific Far East':
    case 'Asia/Pacific Southern Asia':
    case 'Asia/Pacific Indian Subcontinent':
    case 'Asia/Pacific Australia/Oceania':
    case 'Africa Nord':
    case 'Africa Central and South':
    case 'Africa East':
    case 'Middle East Mediterranean':
    case 'Middle East East':
    case 'Russia, east of Urals':
    case 'Russia, west of Urals':
      priority = 4;
      break;
    default:
      priority = 5;
    }
    return priority;
  }

  function checkIfEU28NOCH(row) {
    return (countryOrderPreference(row[0]) < 3);
  }

  function sectorToInt(sector) {
    var intSector;
    switch (sector) {
    case 0:
    case 2:
    case 4:
    case 6:
      intSector = 0;
      break;
    case 1:
    case 10:
    case 11:
    case 20:
    case 3:
    case 5:
    case 7:
    case 8:
    case 9:
      intSector = 1;
      break;
    default:
      intSector = 2;
    }
    return intSector;
  }

  function ReduceVESFactory(nonAgregationField) {
    var reduceFunction = function (prev, curr) {
      if (prev.length === 1 || prev[prev.length - 1].country !== curr.country) {
        prev.push({
          country: curr.country,
          code : curr.code,
          values: {}
        });
      }

      if (!prev[0].values[curr.year]) {
        prev[0].values[curr.year] = {
          total : 0
        };
      }
      if (!prev[0].values[curr.year][curr[nonAgregationField]]) {
        prev[0].values[curr.year][curr[nonAgregationField]] = 0;
      }

      if (!prev[prev.length - 1].values[curr.year]) {
        prev[prev.length - 1].values[curr.year] = {
          total : 0
        };
      }

      prev[0].values[curr.year][curr[nonAgregationField]] += curr.sum;
      prev[0].values[curr.year].total += curr.sum;
      prev[prev.length - 1].values[curr.year][curr[nonAgregationField]] = curr.sum;
      prev[prev.length - 1].values[curr.year].total += curr.sum;
      return prev;
    };

    return reduceFunction;
  }

  function reduceFREDByCountry(prev, curr) {
    if (prev.length === 0 || prev[prev.length - 1].country !== curr.country) {
      prev.push({
        country: curr.country,
        code : curr.code,
        values: {}
      });
    }

    if (!prev[prev.length - 1].values[curr.year]) {
      prev[prev.length - 1].values[curr.year] = {
        tkm : {
          total : 0
        },
        vkm : {
          total : 0
        },
        ratio : {}
      };
    }

    prev[prev.length - 1].values[curr.year].tkm[curr.mode] = curr.tkm;
    prev[prev.length - 1].values[curr.year].tkm.total += curr.tkm;

    prev[prev.length - 1].values[curr.year].vkm[curr.mode] = curr.vkm;
    prev[prev.length - 1].values[curr.year].vkm.total += curr.vkm;

    prev[prev.length - 1].values[curr.year].ratio[curr.mode] = curr.tkm / curr.vkm;
    return prev;
  }

  function checkOwnerShip(req, res, cb) {
    HTModelRuns.findOne({id: req.params.runId})
      .then(function (runMetadata) {
        if (runMetadata) {
          if (req.user.isAdmin || req.user.id === runMetadata.owner) {
            runMetadata.name = sails.config.HTConfig.schemaPrefix + runMetadata.id;
            cb(req, res, runMetadata);
          } else {
            res.status(403);
            return res.json({message : 'Access denied.'});
          }
        } else {
          res.status(404);
          return res.json({message : 'Schema not found.'});
        }
      })
      .catch(function (err) {
        res.status(500);
        return res.json({message : err});
      });
  }

  function sortByCountryProperty(a, b) {
    var aPriority = countryOrderPreference(a.country),
      bPriority = countryOrderPreference(b.country);

    return (aPriority !== bPriority ? aPriority - bPriority :
        (a.country > b.country ? 1 : -1)
      );
  }

  module.exports = {
    getAirPollution : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        let dataQuery;
        switch (req.params.type) {
          case 'passenger':
            dataQuery = HTDataStock.getAirPassengerPollutionCosts(runMetadata.name);
            break;
          case 'freight':
            dataQuery = HTDataStock.getAirFreightPollutionCosts(runMetadata.name);
            break;
          default:
            return res.status(400).send('Unknown air pollution type.');
        }
        dataQuery.then(data => res.json(data));
      });
    },
    getClimateChange : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        let dataQuery;
        switch (req.params.type) {
          case 'passenger':
            dataQuery = HTDataStock.getClimateChangePassengerCosts(runMetadata.name);
            break;
          case 'freight':
            dataQuery = HTDataStock.getClimateChangeFreightCosts(runMetadata.name);
            break;
          default:
            return res.status(400).send('Unknown climate change type.');
        }
        dataQuery.then(data => res.json(data))
          .catch(err => res.status(500).send(err));;
      });
    },
    getMarginalInfrastructureCosts : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        HTDataStock.getMarginalInfrastructureCosts(runMetadata.name)
          .then(data => res.json(data))
          .catch(err => res.status(500).send(err));
      });
    },
    getUpDownstreamCosts : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        HTDataStock.getUpDownStreamCosts(runMetadata.name)
          .then(data => res.json(data))
          .catch(err => res.status(500).send(err));
      });
    },
    getAccidentsCosts : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        HTDataStock.getAccidentsCosts(runMetadata.name)
          .then(data => res.json(data))
          .catch(err => res.status(500).send(err));
      });
    },
    getGeneralizedCosts : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        if (req.params.type === 'passenger' || req.params.type === 'freight') {
          HTDataStock.getGeneralizedCosts(runMetadata.name, req.params.type)
            .then(data => res.json(data))
            .catch(err => res.status(500).send(err));
        } else {
          res.status(400).send('Invalid generalized costs type.')
        }
      });
    },
    getDEMResults : function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {
        sails.promise.all([
          HTDataStock.getPopulationLabourByCountries(runMetadata.name),
          HTDataStock.getPopulationByAge(runMetadata.name),
          HTDataStock.getPopulationByGender(runMetadata.name)
        ])
          .then(function (results) {
            const retArr = results[0].rows.reduce(function (prev, curr, index) {
              if (index === 0 || prev[0][prev[0].length - 1].country !== curr.country) {
                prev[0].push({
                  country : curr.country,
                  code : curr.code,
                  population : [],
                  labour : []
                });
              }

              const prevCurrIndex = prev[0].length - 1;
              prev[0][prevCurrIndex].population.push(parseFloat(curr.population, 10));
              prev[0][prevCurrIndex].labour.push(parseFloat(curr.labour, 10));

              if (prevCurrIndex === 0) {
                prev[1].push(curr.year);
              }

              return prev;
            }, [[], []]);

            const retArrayAges = results[1].rows.reduce(function (prev, curr, index) {
              if (index === 0 || prev[0][prev[0].length - 1].year !== curr.year) {
                prev[0].push({
                  year : curr.year,
                  population : []
                });
              }

              const prevCurrIndex = prev[0].length - 1;
              prev[0][prevCurrIndex].population.push(parseFloat(curr.population, 10));

              if (prevCurrIndex === 0) {
                prev[1].push(curr.agegroup);
              }

              return prev;
            }, [[], []]);

            const retObjGenders = results[2].rows.reduce(function (prev, curr) {
              if (!prev[curr.year]) {
                prev[curr.year] = [0, 0, 0];
              }

              const castedPop = parseFloat(curr.population, 10);

              prev[curr.year][2] += castedPop;
              if (curr.gender === 'Male') {
                prev[curr.year][0] += castedPop;
              } else {
                prev[curr.year][1] += castedPop;
              }

              return prev;

            }, {});

            return res.json([
              {
                years: retArr[1],
                data : retArr[0]
              },
              {
                ageCohorts: retArrayAges[1],
                data : retArrayAges[0]
              },
              retObjGenders
            ]);
          })
          .catch(function (err) {
            return res.status(500).send(err);
          });
      });
    },
    getECRResults: function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {
        sails.promise.all([
          HTDataStock.getGVABySectorsAndContries(runMetadata.name),
          HTDataStock.getGVABySectors(runMetadata.name),
          HTDataStock.getGDPPerCapita(runMetadata.name)
        ])
          .then(function (result) {
            var retArrayGVACountry, retObjGVAYear, retArrayGDPCountry;

            function checkAndSetAcumulatorObj(obj, key, index, value) {
              if (!obj[key]) {
                obj[key] = [0, 0, 0, 0];
              }
              if (value) {
                obj[key][index] += value;
                obj[key][3] += value;
              }
            }

            retArrayGVACountry = result[0].rows.reduce(function (prev, curr) {
              if (prev.length === 1 || prev[prev.length - 1].country !== curr.country) {
                prev.push({
                  country : curr.country,
                  code : curr.code,
                  gva : {}
                });
              }
              //EU28 Aggregate
              if (countryOrderPreference(curr.country) === 1) {
                checkAndSetAcumulatorObj(prev[0].gva, curr.year, sectorToInt(curr.sector), curr.sum);
              }
              // Country Aggregate
              checkAndSetAcumulatorObj(prev[prev.length - 1].gva, curr.year, sectorToInt(curr.sector), curr.sum);

              return prev;
            }, [{
              country : 'EU28',
              code : 'EU28',
              gva : {}
            }]);


            retObjGVAYear = result[1].rows.reduce(function (prev, curr) {
              if (countryOrderPreference(curr.country) === 1) {
                checkAndSetAcumulatorObj(prev, curr.year, sectorToInt(curr.sector), curr.sum);
              }
              return prev;
            }, {});


            function getGDPCountryAverage(accumulated) {
              var firstGDP, lastGDP, firstGDPCapita, lastGDPCapita,
                lastYear = accumulated[accumulated.length - 1].values[accumulated[accumulated.length - 1].values.length - 1].year,
                firstYear = accumulated[accumulated.length - 1].values[0].year;

              if (lastYear === firstYear) {
                accumulated[accumulated.length - 1].avgYearAbs = 0;
                accumulated[accumulated.length - 1].avgYearCapita = 0;
              } else {
                lastGDPCapita = accumulated[accumulated.length - 1].values[accumulated[accumulated.length - 1].values.length - 1].gdp_capita;
                firstGDPCapita = accumulated[accumulated.length - 1].values[0].gdp_capita;
                lastGDP = accumulated[accumulated.length - 1].values[accumulated[accumulated.length - 1].values.length - 1].gdp;
                firstGDP = accumulated[accumulated.length - 1].values[0].gdp;

                accumulated[accumulated.length - 1].avgYearAbs = Math.log(lastGDP / firstGDP) / (lastYear - firstYear);
                accumulated[accumulated.length - 1].avgYearCapita = Math.log(lastGDPCapita / firstGDPCapita) / (lastYear - firstYear);
              }
              return;
            }

            retArrayGDPCountry = result[2].rows.reduce(function (prev, curr, index, iterable) {
              if (countryOrderPreference(curr.country) === 1) {
                if (prev.length === 1 || prev[prev.length - 1].country !== curr.country) {
                  if (prev.length !== 1) {
                    getGDPCountryAverage(prev);
                  }
                  prev.push({
                    country : curr.country,
                    values : [],
                    avgYearCapita : null,
                    avgYearAbs : null
                  });
                }
                prev[prev.length - 1].values.push({
                  year : parseInt(curr.year, 10),
                  gdp_capita : curr.gdp_capita,
                  gdp : curr.gdp
                });
                if (index === iterable.length - 1) {
                  getGDPCountryAverage(prev);
                }

                if (!prev[0].years[curr.year]) {
                  prev[0].years[curr.year] = [0, 0];
                }
                prev[0].years[curr.year][0] += curr.gdp;
                prev[0].years[curr.year][1] += curr.pop;
              }
              return prev;
            }, [{
              country : 'EU28',
              years : {},
              avgYearCapita : null,
              avgYearAbs : null
            }]);

            retArrayGDPCountry[0] = (function (item) {
              var retObj = [{
                country : item.country,
                values : [],
                avgYearCapita : null,
                avgYearAbs : null
              }];

              Object.keys(item.years).forEach(function (key) {
                retObj[0].values.push({
                  year : parseInt(key, 10),
                  gdp : item.years[key][0],
                  gdp_capita : item.years[key][0] / item.years[key][1]
                });
              });
              getGDPCountryAverage(retObj);

              return retObj[0];
            }(retArrayGDPCountry[0]));

            return res.json([
              retArrayGVACountry,
              retObjGVAYear,
              retArrayGDPCountry
            ]);
          });
      });
    },
    getVESResults: function (req, res) {
      let modelMethod;
      switch (req.params.outputType) {
        case 'modesByCountry':
          modelMethod = HTDataStock.getVESStockByCountryAndMode;
          break;
        case 'modesByYear':
          modelMethod = HTDataStock.getVESStockByYearAndMode;
          break;
        case 'fuelsByCountry':
          modelMethod = HTDataStock.getVESStockByCountryAndFuel;
          break;
        case 'fuelsByYear':
          modelMethod = HTDataStock.getVESStockByYearAndFuel;
          break;
        default:
          return res.notFound();
      }

      checkOwnerShip(req, res, function (req, res, runMetadata) {
          modelMethod(runMetadata.name)
            .then(data => res.json(data))
            .catch(err => res.status(500).send((err.stack ? err.stack : err)));
      });
    },
    getPADResults: function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {
        sails.promise.all([
          HTDataStock.getPADpkmByContryAndMode(runMetadata.name),
          HTDataStock.getPADpkmByMode(runMetadata.name),
          HTDataStock.getPADvkmByContryAndMode(runMetadata.name),
          HTDataStock.getPADvkmByMode(runMetadata.name),
          HTDataStock.getPADTripsByContryAndMode(runMetadata.name),
          HTDataStock.getPADTripsByMode(runMetadata.name),
          HTDataStock.getPADVehOccupation(runMetadata.name),
          HTDataStock.getPADBandByCountryAndMode(runMetadata.name, 'pkm')
        ])
          .then(function (result) {
            function reducePAD(prev, curr) {
              if (prev.length === 0 || prev[prev.length - 1].year !== curr.year) {
                prev.push({
                  year: curr.year,
                  modes: {},
                  total : 0
                });
              }
              prev[prev.length - 1].modes[curr.mode] = curr.sum;
              prev[prev.length - 1].total += curr.sum;
              return prev;
            }

            function reducePADByCountry(prev, curr) {
              if (prev.length === 0 || prev[prev.length - 1].country !== curr.country) {
                prev.push({
                  country: curr.country,
                  code : curr.code,
                  values: {}
                });
              }

              if (!prev[prev.length - 1].values[curr.year]) {
                prev[prev.length - 1].values[curr.year] = {
                  total : 0
                };
              }

              prev[prev.length - 1].values[curr.year][curr.mode] = curr.sum;
              prev[prev.length - 1].values[curr.year].total += curr.sum;
              return prev;
            }

            function reducePADModeCountryYearBand(prev, curr, index, array) {
              if (!prev[curr.mode]) {
                prev[curr.mode] = {};
              }

              if (!prev[curr.mode][curr.country]) {
                prev[curr.mode][curr.country] = [];
              }

              if (!prev[curr.mode][curr.country].length || prev[curr.mode][curr.country][prev[curr.mode][curr.country].length - 1][0] !== curr.year) {
                prev[curr.mode][curr.country].push([curr.year]);
              }

              prev[curr.mode][curr.country][prev[curr.mode][curr.country].length - 1].push(curr.sum);

              return prev;
            }

            function getPADDistanceBands(rows) {
              let distanceBands = [];
              const currYear = rows[0].year;
              for (let i = 0; currYear === rows[i].year; i += 1) {
                distanceBands.push(rows[i].distance);
              }
              return distanceBands;
            }

            var PADpkm = result[1].rows.reduce(reducePAD, []),
              PADpkmCountry = result[0].rows.reduce(reducePADByCountry, []),
              PADvkm = result[3].rows.reduce(reducePAD, []),
              PADvkmCountry = result[2].rows.reduce(reducePADByCountry, []),
              PADtrip = result[5].rows.reduce(reducePAD, []),
              PADtripByCountry = result[4].rows.reduce(reducePADByCountry, []),
              PADOccuCountry = result[6].rows.reduce(reducePADByCountry, []),
              PADpkmDistanceBands = {
                unit : 'pkm',
                bands : getPADDistanceBands(result[7].rows),
                data : result[7].rows.reduce(reducePADModeCountryYearBand, {})
              };

            return res.json([
              PADpkm,
              PADpkmCountry,
              PADvkm,
              PADvkmCountry,
              PADtrip,
              PADtripByCountry,
              PADOccuCountry,
              PADpkmDistanceBands
            ]);
          });
      });
    },
    getFRETransit: function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {
        Promise.all([
          HTDataStock.getFRDTransitByCountryAndMode(runMetadata.name),
          HTDataStock.getFRDTransitPerYearAndMode(runMetadata.name)
        ])
        .then(dataSet => res.json(dataSet))
        .catch(err => {
          console.error(err.stack);
          return res.status(500).send(err.stack);
        });
      });
    },
    getSAFInjuries : function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {
        var tables = [
          ['Car', 'car', 0, 'full'],
          ['Bicycle', 'bike', 4, 'full'],
          ['2-wheeled Passenger', 'p2w', 2, 'full'],
          ['Public Transport', 'pt', 3, 'full'],
          ['Rail', 'rail', null, 'partial'],
          ['Air', 'air', null, 'worldRegions'],
          ['IWW', 'iww', null, 'worldRegions'],
          ['Short-sea', 'sss', null, 'worldRegions']
        ],
          queries = [];

        function reduceSAFByCountry(prev, curr) {
          if (prev.length === 0 || prev[prev.length - 1].country !== curr.country) {
            prev.push({
              country: curr.country,
              values: {}
            });
          }

          if (!prev[prev.length - 1].values[curr.year]) {
            prev[prev.length - 1].values[curr.year] = {};
          }

          Object.keys(curr)
            .filter(function (key) {
              return key !== 'country' && key !== 'year';
            })
            .forEach(function (key) {
              prev[prev.length - 1].values[curr.year][key] = curr[key];
            });

          return prev;
        }

        tables.forEach(function (table) {
          switch (table[3]) {
          case 'full':
            queries.push(HTDataStock.getSAFInjuriesTable(runMetadata.name, table[1], table[2]));
            break;
          case 'partial':
            queries.push(HTDataStock.getSAFFatAndCostTableCountry(runMetadata.name, table[1]));
            break;
          case 'worldRegions':
            queries.push(HTDataStock.getSAFFatAndCostTableWorldRegion(runMetadata.name, table[1]));
            break;
          }
        });

        sails.promise.all(queries)
          .then(function (results) {
            return res.json(results
              .reduce(function (prev, curr, index) {
                prev.push({
                  tableName : tables[index][0],
                  type : tables[index][3],
                  tableContents : curr.rows.reduce(reduceSAFByCountry, [])
                    .sort(sortByCountryProperty)
                });
                return prev;
              }, [])
              .map(function (result) {
                if (result.type !== 'worldRegions' && result.tableContents) {
                  result.tableContents.unshift({
                    country : 'EU28',
                    values : result.tableContents.reduce(function (prev, curr) {
                      if (countryOrderPreference(curr.country) === 1) {
                        Object.keys(curr.values).forEach(function (year) {
                          if (!prev[year]) {
                            prev[year] = {};
                          }
                          Object.keys(curr.values[year]).forEach(function (accidentType) {
                            if (!prev[year][accidentType]) {
                              prev[year][accidentType] = 0;
                            }
                            prev[year][accidentType] += curr.values[year][accidentType];
                          });
                        });
                      }
                      return prev;
                    }, {})
                  });
                }
                return result;
              }));
          });
      });
    },
    getEnvironmentEmissions : function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {
        HTDataStock.getEnvironmentEmissions(runMetadata.name)
          .then(function (result) {
            function reduceENVByCountry(prev, curr) {
              if (prev.length === 0 || prev[prev.length - 1].country !== curr.country) {
                prev.push({
                  country: curr.country,
                  code: curr.code,
                  values: {}
                });
              }

              if (!prev[prev.length - 1].values[curr.year]) {
                prev[prev.length - 1].values[curr.year] = {};
              }

              Object.keys(curr)
                .filter(function (key) {
                  return key !== 'country' && key !== 'year' && key !== 'code';
                })
                .forEach(function (key) {
                  prev[prev.length - 1].values[curr.year][key] = curr[key];
                });

              return prev;
            }
            return res.json(result.rows.reduce(reduceENVByCountry, []));
          });
      });
    },
    getAssumptions : function (req, res) {
      const schema = (req.params.schemaId === 'eureference' ? sails.config.HTConfig.baseSchema : req.params.schemaId);
      var modelSearch;

      switch (req.params.moduleId) {
        case 'DEM':
          modelSearch = HTDataStock.getDEMAssumption;
          break;
        case 'ECR':
          modelSearch = HTDataStock.getECRAssumption;
          break;
        default:
          return res.status(401).send('The requested model does not exists.');
      }

      modelSearch(schema)
        .then(function (result) {
          res.status(200);
          return res.json(result);
        })
        .catch(function (err) {
          return res.status(500).send(err);
        });
    },
    getVariableList: function (req, res) {
      HTDataStock.getVariableList(sails.config.HTConfig.baseSchema, req.query.variables)
        .then(function (result) {
          return res.json(result);
        })
        .catch(function (err) {
          return res.status(500).send(err);
        });
    },
    exportToXLS: function (req, res) {
      checkOwnerShip(req, res, function (req, res, runMetadata) {

        function sortByCountryFirstColumn(a, b) {
          var aVal = countryOrderPreference(a[0]),
            bVal = countryOrderPreference(b[0]);
          return (aVal !== bVal ? aVal - bVal :
              (a > b ? 1 : -1)
            );
        }

        function sortByCountryObjectCountry(a, b) {
          var aVal = countryOrderPreference(a.country),
            bVal = countryOrderPreference(b.country);
          return (aVal !== bVal ? aVal - bVal :
              (a.country > b.country ? 1 : -1)
            );
        }

        function createDEMSheet(schema) {
          return new sails.promise(function (resolve, reject) {
            sails.promise.all([
              HTDataStock.getPopulationByGender(schema),
              HTDataStock.getPopulationByAge(schema),
              HTDataStock.getPopulationLabourByCountries(schema)
            ])
              .then(function (results) {
                var JSONString;

                function transformPopulationByGender(rawData) {
                  var tempObj = {
                    target : [
                      sheetsName.DEM,
                      [1, 6]
                    ]
                  };

                  tempObj.data = rawData.rows.reduce(function (prev, curr) {
                    const yearToRow = Math.floor((parseInt(curr.year, 10) - 2010) / 5),
                      castedPop = Math.round(parseFloat(curr.population, 10) / 1000000);

                    if (!prev[yearToRow]) {
                      prev[yearToRow] = [parseInt(curr.year, 10), 0, 0, 0];
                    }

                    prev[yearToRow][3] += castedPop;
                    if (curr.gender === 'Male') {
                      prev[yearToRow][1] += castedPop;
                    } else {
                      prev[yearToRow][2] += castedPop;
                    }
                    return prev;
                  }, []);

                  return tempObj;
                }

                function tranformPopulationByAge(rawData) {

                  var tempObj = {
                    target : [
                      sheetsName.DEM,
                      [1, 22]
                    ],
                    data : []
                  },
                    ageCohorts = 0;

                  const joinFactor = 4;
                  /* jslint for: true */
                  for (let i = 0, year = rawData.rows[0].year, ln = rawData.rows.length; i < ln && year === rawData.rows[i].year; i += 1) {
                    ageCohorts += 1;
                  }
                  const reducedAgeCohorts = Math.floor(ageCohorts / joinFactor);
                  /* jslint for : false */

                  tempObj.data = rawData.rows
                    .reduce(function (prev, curr, index, array) {
                      if (prev.length === 0 || prev[prev.length - 1][0] !== parseInt(curr.year,10)) {
                        prev.push([parseInt(curr.year,10)].concat(new Array(reducedAgeCohorts).fill(0)));
                      }

                      prev[prev.length - 1][1 + Math.min(Math.floor((index % ageCohorts) / reducedAgeCohorts), reducedAgeCohorts)] += parseFloat(curr.population);
                      return prev;
                    }, [])
                    .map(item => {
                      for (let i = 1, ln = item.length; i < ln; i += 1) {
                        item[i] = item[i] / 1000000;
                      }
                      return item;
                    });

                  return tempObj;
                }

                function transformPopulationByCountry(rawData, magnitude, coordinates) {
                  var tempObj = {
                    target : [
                      sheetsName.DEM,
                      coordinates
                    ],
                    data : []
                  };

                  rawData = rawData.rows
                    .reduce((prev, curr) => {
                      var currentYear = parseInt(curr.year, 10);
                      if (currentYear < prev.min) {
                        prev.min = currentYear;
                      }
                      if (currentYear > prev.min) {
                        prev.max = currentYear;
                      }
                      if (prev.data.length === 0 || prev.data[prev.data.length - 1][0] !== curr.country) {
                        prev.data.push([
                          curr.country, 0, 0, 0
                        ]);
                      }
                      if (currentYear === prev.min) {
                        prev.data[prev.data.length - 1][1] = parseFloat(curr[magnitude]);
                      } else if (currentYear === prev.max) {
                        prev.data[prev.data.length - 1][2] = parseFloat(curr[magnitude]);
                      }
                      return prev;
                    }, {
                      min : 3000,
                      max : 0,
                      data : []}
                    );

                    tempObj.data = rawData.data
                      .reduce((prev, curr) => {
                        for (let i = 1; i < 3; i += 1) {
                          prev[0][i] += curr[i];
                        }
                        prev.push(curr);
                        return prev;
                      }, [[ 'EU28', 0, 0, 0 ]])
                      .map(item => {
                        for (let i = 1; i < 3; i += 1) {
                          item[i] = (item[i] / 1000000);
                        }
                        item[3] = (item[2] / item[1] - 1);
                        return item;
                      })
                      .filter(checkIfEU28NOCH)
                      .sort(sortByCountryFirstColumn);
                  return tempObj;
                }

                resolve([
                  transformPopulationByGender(results[0]),
                  tranformPopulationByAge(results[1]),
                  transformPopulationByCountry(results[2], 'population', [1, 38]),
                  transformPopulationByCountry(results[2], 'labour', [1, 76])
                ]);

              })
              .catch(function (err) {
                reject(new Error(err));
              });
          });
        }

        function createECRSheet(schema) {
          return new sails.promise(function (resolve, reject) {
            sails.promise.all([
              HTDataStock.getGVABySectorsAndContries(schema),
              HTDataStock.getGVABySectors(schema),
              HTDataStock.getGDPPerCapita(schema)
            ])
              .then(function (results) {

                function tranformGDPPerCapita(rawData) {
                  var tempObj = {
                    target : [
                      sheetsName.ECR,
                      [1, 7]
                    ],
                    data : []
                  },
                  firstYear = 3000,
                  lastYear = 0;

                  tempObj.data = rawData
                    .reduce((prev, curr) => {
                      // Set to popFirst, GPDFirst, popLast, GDPLast
                      if (prev.length === 1 || prev[prev.length - 1][0] !== curr.country) {
                        if (prev.length !== 1) {
                          for (let i = 1; i <= 4; i += 1) {
                            prev[0][i] += prev[prev.length - 1][i];
                          }
                        }
                        prev.push([curr.country, 0, 0, 0, 0]);
                      }

                      const currentYear = parseInt(curr.year, 10);
                      if (currentYear >= lastYear) {
                        lastYear = currentYear;
                        prev[prev.length - 1][3] = curr.pop;
                        prev[prev.length - 1][4] = curr.gdp;
                      }
                      if (currentYear <= firstYear) {
                        firstYear = currentYear;
                        prev[prev.length - 1][1] = curr.pop;
                        prev[prev.length - 1][2] = curr.gdp;
                      }

                      return prev;
                    }, [['EU28', 0, 0, 0, 0]])
                    .map(item => {
                      // Transform to D gdp D gppCap, gdpCapFirst gdpCapLast
                      var deltaGDP;
                      if (firstYear === lastYear) {
                        deltaGDP = 0;
                      } else {
                        deltaGDP = Math.log(item[4] / item[2]) / (lastYear - firstYear);
                      }
                      item[4] = item[4] / item[3];
                      item[3] = item[2] / item[1];

                      item[1] = deltaGDP;
                      item[2] = (firstYear === lastYear ? 0 : Math.log(item[4] / item[3]) / (lastYear - firstYear));
                      return item;
                    })
                    .filter(checkIfEU28NOCH)
                    .sort(sortByCountryFirstColumn);

                    return tempObj;
                  }

                var retArrayGVACountry, retObjGVAYear;

                function checkAndSetAcumulatorObj(obj, key, index, value) {
                  if (!obj[key]) {
                    obj[key] = [0, 0, 0, 0];
                  }
                  if (value) {
                    obj[key][index] += value;
                    obj[key][3] += value;
                  }
                }

                function transformGVABySectorsAndContries(rawData) {
                  var tempObj = {
                    target : [
                      sheetsName.ECR,
                      [1, 60]
                    ],
                    data : []
                  },
                  firstYear, lastYear;

                  tempObj.data = rawData.reduce(function (prev, curr) {
                    if (prev.length === 1 || prev[prev.length - 1].country !== curr.country) {
                      prev.push({
                        country : curr.country,
                        code : curr.code,
                        gva : {}
                      });
                    }
                    //EU28 Aggregate
                    if (countryOrderPreference(curr.country) === 1) {
                      checkAndSetAcumulatorObj(prev[0].gva, curr.year, sectorToInt(curr.sector), curr.sum);
                    }
                    // Country Aggregate
                    checkAndSetAcumulatorObj(prev[prev.length - 1].gva, curr.year, sectorToInt(curr.sector), curr.sum);

                    return prev;
                  }, [{
                    country : 'EU28',
                    code : 'EU28',
                    gva : {}
                  }])
                  .reduce(function (prev, curr, index) {
                    if (index === 0) {
                      var years = Object.keys(curr.gva).sort();
                      firstYear = years[0];
                      lastYear = years[years.length - 1];
                    }
                    prev.push([curr.country].concat(curr.gva[lastYear]));
                    return prev;
                  }, [])
                  .filter(checkIfEU28NOCH)
                  .sort(sortByCountryFirstColumn);

                  return tempObj;
                }

                function transformGVABySectors(rawData) {
                  var tempObj = {
                    target : [
                      sheetsName.ECR,
                      [1, 43]
                    ],
                    data : []
                  }, tempData;

                  tempData = rawData.reduce(function (prev, curr) {
                    if (countryOrderPreference(curr.country) === 1) {
                      checkAndSetAcumulatorObj(prev, curr.year, sectorToInt(curr.sector), curr.sum);
                    }
                    return prev;
                  }, {});

                  Object.keys(tempData).forEach(year => {
                    var data = [parseInt(year, 10)];
                    for (let i = 0; i < 3; i+= 1) {
                      data.push(tempData[year][i]);
                      data.push(tempData[year][i] / tempData[year][3]);
                    }
                    tempObj.data.push(data);
                  });
                  return tempObj;
                }

                resolve([
                  tranformGDPPerCapita(results[2].rows),
                  transformGVABySectorsAndContries(results[0].rows),
                  transformGVABySectors(results[1].rows)
                ]);
            })
            .catch(err => {
              reject(new Error(err));
            });
          });
        }

        function createVESSheet(schema) {
          return new Promise((resolve, reject) => {
            Promise.all([
              HTDataStock.getVESStockByCountryAndFuel(schema),
              HTDataStock.getVESStockByCountryAndMode(schema),
              HTDataStock.getVESStockByYearAndFuel(schema),
              HTDataStock.getVESStockByYearAndMode(schema)
            ])
            .then(results => {
              const {firstYear, lastYear, stepSize} = sails.config.HTConfig;
              const {fuelTypes} = results[0];
              const {modeTypes} = results[1];

              const countryFuelTabularData = new Array(results[0].data.length).fill([])
                .map(item => new Array(1 + fuelTypes.length).fill(0));

              results[0].data.sort(sortByCountryProperty);

              results[0].data.forEach((country, row) => {
                countryFuelTabularData[row][0] = country.country;
                country.values.filter(item => (parseInt(item.year, 10) === lastYear))
                  .forEach(lastYearData => {
                    fuelTypes.forEach((fuel, column) => {
                      if (lastYearData[fuel.key]) {
                        countryFuelTabularData[row][1 + column] = lastYearData[fuel.key];
                      }
                    })
                  });
              });

              const countryModeTabularData = new Array(results[1].data.length).fill([])
                .map(item => new Array(1 + modeTypes.length).fill(0));

              results[1].data.sort(sortByCountryProperty);

              results[1].data.forEach((country, row) => {
                countryModeTabularData[row][0] = country.country;
                country.values.filter(item => (parseInt(item.year, 10) === lastYear))
                  .forEach(lastYearData => {
                    modeTypes.forEach((mode, column) => {
                      if (lastYearData[mode.key]) {
                        countryModeTabularData[row][1 + column] = lastYearData[mode.key];
                      }
                    })
                  });
              });

              const yearFuelTabularData = new Array(1 + (lastYear - firstYear) / stepSize).fill([])
                .map((item, index) => [(firstYear + index * stepSize), ...new Array(fuelTypes.length).fill(0)]);
              const yearModeTabularData = new Array(1 + (lastYear - firstYear) / stepSize).fill([])
                .map((item, index) => [(firstYear + index * stepSize), ...new Array(modeTypes.length).fill(0)]);

              results[2].data.forEach(year => {
                const row = (parseInt(year.year, 10) - firstYear) / stepSize;
                fuelTypes.forEach((fuel, column) => {
                  if (year[fuel.key]) {
                    yearFuelTabularData[row][1 + column] = year[fuel.key];
                  }
                });
              });
              results[3].data.forEach(year => {
                const row = (parseInt(year.year, 10) - firstYear) / stepSize;
                modeTypes.forEach((mode, column) => {
                  if (year[mode.key]) {
                    yearModeTabularData[row][1 + column] = year[mode.key];
                  }
                });
              });

              resolve([
                {
                  target : [
                    sheetsName.VES,
                    [1, 20]
                  ],
                  data : countryFuelTabularData
                },
                {
                  target : [
                    sheetsName.VES,
                    [1, 57]
                  ],
                  data : countryModeTabularData
                },
                {
                  target : [
                    sheetsName.VES,
                    [1, 7]
                  ],
                  data : yearFuelTabularData
                },
                {
                  target : [
                    sheetsName.VES,
                    [1, 93]
                  ],
                  data : yearModeTabularData
                }
              ]);

            })
            .catch(reject);
          });
        }

        function createPADSheet(schema) {
          return new sails.promise(function (resolve, reject) {
            sails.promise.all([
              HTDataStock.getPADpkmByContryAndMode(runMetadata.name),
              HTDataStock.getPADpkmByMode(runMetadata.name),
              HTDataStock.getPADvkmByContryAndMode(runMetadata.name),
              HTDataStock.getPADvkmByMode(runMetadata.name),
              HTDataStock.getPADTripsByContryAndMode(runMetadata.name),
              HTDataStock.getPADTripsByMode(runMetadata.name),
              HTDataStock.getPADVehOccupation(runMetadata.name)
            ])
              .then(function (results) {
                function reducePAD(prev, curr) {
                  if (prev.length === 0 || prev[prev.length - 1].year !== curr.year) {
                    prev.push({
                      year: curr.year,
                      modes: {},
                      total : 0
                    });
                  }
                  prev[prev.length - 1].modes[curr.mode] = curr.sum;
                  prev[prev.length - 1].total += curr.sum;
                  return prev;
                }

                function reducePADByCountry(prev, curr) {
                  if (prev.length === 0 || prev[prev.length - 1].country !== curr.country) {
                    prev.push({
                      country: curr.country,
                      code : curr.code,
                      values: {}
                    });
                  }

                  if (!prev[prev.length - 1].values[curr.year]) {
                    prev[prev.length - 1].values[curr.year] = {
                      total : 0
                    };
                  }

                  prev[prev.length - 1].values[curr.year][curr.mode] = curr.sum;
                  prev[prev.length - 1].values[curr.year].total += curr.sum;
                  return prev;
                }


                function transformPADDataYear(rawData, coordinates) {
                  var tempObj = {
                    target : [
                      sheetsName.PAD,
                      coordinates
                    ],
                    data : []
                  }, modes;

                  tempObj.data = rawData
                    .reduce(reducePAD, [])
                    .reduce((prev, curr, index) => {
                      if (index === 0) {
                        modes = Object.keys(curr.modes);
                      }
                      var row = [parseInt(curr.year, 10)];
                      for (let i = 0; i < modes.length; i += 1) {
                        row[2*i + 1] = curr.modes[modes[i]] / 1000000;
                        row[2*i + 2] = curr.modes[modes[i]] / curr.total;
                      }
                      prev.push(row);
                      return prev;
                    }, []);

                  return tempObj;
                }

                function transformPADCountries(rawData, coordinates) {
                  var tempObj = {
                    target : [
                      sheetsName.PAD,
                      coordinates
                    ],
                    data : []
                  }, modes, years, lastYear;

                  tempObj.data = rawData
                    .reduce(reducePADByCountry, [])
                    .reduce((prev, curr, index) => {
                      if (index === 0) {
                        years = Object.keys(curr.values).sort();
                        lastYear = years[years.length - 1];
                        modes = Object.keys(curr.values[lastYear])
                          .filter(item => {
                            return item !== 'total';
                          });
                      }
                      var row = [curr.country];
                      for (let i = 0; i < modes.length; i += 1) {
                        row[2*i + 1] = curr.values[lastYear][modes[i]] / 1000000;
                        row[2*i + 2] = curr.values[lastYear][modes[i]] / curr.values[lastYear].total;
                      }
                      prev.push(row);
                      return prev;
                    }, [])
                    .filter(checkIfEU28NOCH)
                    .sort(sortByCountryFirstColumn);
                  return tempObj;
                }

                function transformPADOccupation(rawData, coordinates) {
                  var tempObj = {
                    target : [
                      sheetsName.PAD,
                      coordinates
                    ],
                    data : []
                  }, modes, years, lastYear;

                  tempObj.data = rawData
                    .reduce(reducePADByCountry, [])
                    .reduce((prev, curr, index) => {
                      if (index === 0) {
                        years = Object.keys(curr.values).sort();
                        lastYear = years[years.length - 1];
                        modes = Object.keys(curr.values[lastYear])
                          .filter(item => {
                            return item !== 'total';
                          });
                      }
                      var row = [curr.country];
                      for (let i = 0; i < modes.length; i += 1) {
                        row[i + 1] = curr.values[lastYear][modes[i]] || '';
                      }
                      prev.push(row);
                      return prev;
                    }, [])
                    .filter(checkIfEU28NOCH)
                    .sort(sortByCountryFirstColumn);
                  return tempObj;
                }

                resolve([
                  transformPADDataYear(results[1].rows, [1, 7]),
                  transformPADCountries(results[0].rows, [1, 23]),
                  transformPADDataYear(results[3].rows, [1, 61]),
                  transformPADCountries(results[2].rows, [1, 77]),
                  transformPADDataYear(results[5].rows, [1, 114]),
                  transformPADCountries(results[4].rows, [1, 130]),
                  transformPADOccupation(results[6].rows, [1, 167])
                ]);

              });
            });
          }

        function createFRDSheet(schema) {
          class FRDAggrReduce {
            static get modesKeys() {
              return ['road', 'rail', 'iww', 'short-sea', 'sea'];
            }

            constructor(magnitude) {
              this.magnitude = magnitude;
            }
          }

          class EU28ReduceFact extends FRDAggrReduce {
            constructor(magnitude) {
              super(magnitude);
            }

            reduce(acum, curr, index, array) {
              const nextLine = [curr.year].concat(new Array(FRDAggrReduce.modesKeys.length * 2).fill(0));

              curr.data.forEach(mode => {
                const insertionIndex = FRDAggrReduce.modesKeys.indexOf(mode.mode);
                if (insertionIndex !== -1) {
                  nextLine[2 * insertionIndex + 1] = mode[this.magnitude] / 1000000000;
                  nextLine[2 * insertionIndex + 2] = mode[this.magnitude + '_share'];
                }
              });
              acum.push(nextLine);
              return acum;
            }
          }

          class CountryReduceFact extends FRDAggrReduce {
            constructor(magnitude) {
              super(magnitude);
            }

            reduce (acum, curr, index, array) {
              const nextLine = [curr.country].concat(new Array(FRDAggrReduce.modesKeys.length * 2).fill(0));

              let year2050 = curr.values.filter(yearEntry => {
                return (Number.parseInt(yearEntry.year, 10) === 2050);
              });
              year2050 = (year2050.length ? year2050[0].data : []);

              year2050.forEach(mode => {
                const insertionIndex = FRDAggrReduce.modesKeys.indexOf(mode.mode);
                if (insertionIndex !== -1) {
                  nextLine[2 * insertionIndex + 1] = mode[this.magnitude] / 1000000000;
                  nextLine[2 * insertionIndex + 2] = mode[this.magnitude + '_share'];
                }
              });

              acum.push(nextLine);
              return acum;
            }
          }

          return new Promise((resolve, reject) => {
            Promise.all([
              HTDataStock.getFRDTransitByCountryAndMode(runMetadata.name),
              HTDataStock.getFRDTransitPerYearAndMode(runMetadata.name)
            ])
            .then(dataSets => {
              const countryData = dataSets[0].data;
              const EU28Agg = dataSets[1].data[0];

              const redEU28tkm = new EU28ReduceFact('tkm');
              const FRDEU28tkm = EU28Agg.values.reduce(redEU28tkm.reduce.bind(redEU28tkm), []);

              const FRDCountry = {};

              ['tkm', 'vkm'].forEach(magnitude => {
                const countryReducetkm = new CountryReduceFact(magnitude);
                FRDCountry[magnitude] = countryData.reduce(countryReducetkm.reduce.bind(countryReducetkm), []);
              });

              const FRDCountryLoadFactor = countryData.reduce((acum, curr) => {
                const nextLine = [curr.country].concat(new Array(FRDAggrReduce.modesKeys.length).fill(0));

                let year2050 = curr.values.filter(yearEntry => {
                  return (Number.parseInt(yearEntry.year, 10) === 2050);
                });
                year2050 = (year2050.length ? year2050[0].data : []);

                year2050.forEach(mode => {
                  const insertionIndex = FRDAggrReduce.modesKeys.indexOf(mode.mode);
                  if (insertionIndex !== -1) {
                    nextLine[insertionIndex + 1] = mode.loadFactor;

                  }
                });

                acum.push(nextLine);
                return acum;
              }, []);

              const newSheets = [
                [FRDEU28tkm, [1,7]],
                [FRDCountry.tkm, [1, 21]],
                [FRDCountry.vkm, [1, 57]],
                [FRDCountryLoadFactor, [1, 92]]
              ].map(item => {
                return {
                  target : [
                    sheetsName.FRD,
                    item[1]
                  ],
                  data : item[0]
                };
              });

              resolve(newSheets);
            })
            .catch(err => {
              console.log(err);
              reject(err);
            });
          });
        }

        function createENVSheet(schema) {
          return new sails.promise(function (resolve, reject) {
            Promise.all([
              HTDataStock.getEnvironmentEmissions(runMetadata.name),
              HTDataStock.getEnvironmentEmissionsByYear(runMetadata.name)
            ])
              .then(results => {

                function transformENVEmissionsByYear(rawData) {
                  const emissions = ['fuel', 'co2', 'nox', 'pm', 'so2'];

                  const emissionsColumn = emissions.reduce((acum, curr, index) => {
                    acum[curr] = index;
                    return acum;
                  }, {});

                  const retObj = {
                    target : [
                      sheetsName.ENV,
                      [1, 6]
                    ],
                  };

                  const {firstYear, lastYear} = sails.config.HTConfig;

                  retObj.data = new Array(1 + (lastYear - firstYear) / 5).fill([])
                    .map((item, index) => [firstYear + index * 5 , ...new Array(emissions.length).fill(0)]);

                  rawData.forEach(year => {
                    const row = (parseInt(year.year, 10) - firstYear) / 5;
                    emissions.forEach((emission, index) => {
                      if (year.values[emission]) {
                        retObj.data[row][1 + index] = year.values[emission];
                      }
                    });
                  });
                  return retObj;
                }

                function transformENVEmissions(rawData) {
                  var lastYear = 0, magnitudes = [],
                    tempObj = {
                      target : [
                        sheetsName.ENV,
                        [1, 19]
                      ],
                      data : []
                    };

                  function reduceENVByCountry(prev, curr, index) {
                    var currentYear = parseInt(curr.year, 10);
                    if (index === 0) {
                      magnitudes = Object.keys(curr)
                        .filter(function (key) {
                          return key !== 'country' && key !== 'year' && key !== 'code';
                        });
                    }
                    if (index === 0 || prev[prev.length - 1][0] !== curr.country) {
                      prev.push([curr.country].concat(new Array(magnitudes.length).fill(0)));
                    }

                    if (lastYear <= currentYear) {
                      lastYear = currentYear;
                      for (let i = 1, ln = magnitudes.length; i <= ln; i+= 1) {
                        prev[prev.length - 1][i] = curr[magnitudes[i - 1]];
                      }
                    }

                    return prev;
                  }
                  tempObj.data = rawData.reduce(reduceENVByCountry, []);

                  tempObj.data.push(tempObj.data.reduce((prev, curr) => {
                    if (countryOrderPreference(curr[0]) === 1) {
                      for (let i = 1, ln = curr.length; i <ln; i+= 1) {
                        prev[i] += curr[i];
                      }
                    }
                    return prev;
                  }, ['EU28'].concat(new Array(magnitudes.length).fill(0))));

                  tempObj.data.sort(sortByCountryFirstColumn);
                  tempObj.data = tempObj.data.filter(checkIfEU28NOCH);
                  return tempObj;
                }

                resolve([
                  transformENVEmissionsByYear(results[1]),
                  transformENVEmissions(results[0].rows)
                ]);
              });
          });
        }

        function createSAFSheet(schema) {
          return new sails.promise(function (resolve, reject) {
            schema = 'high_tool';
            var tables = [
              ['Car', 'car', 0, 'full'],
              ['Bicycle', 'bike', 4, 'full'],
              ['2-wheeled Passenger', 'p2w', 2, 'full'],
              ['Public Transport', 'pt', 3, 'full'],
              ['Rail', 'rail', null, 'partial'],
              ['Air', 'air', null, 'worldRegions'],
              ['IWW', 'iww', null, 'worldRegions'],
              ['Short-sea', 'sss', null, 'worldRegions']
            ];

            const queries = [];
            queries.push(new Promise((resolve, reject) => {
              HTDataStock.getSAFInjuriesTableByYear(runMetadata.name, 'car', 0)
                .then(res => {
                  const {firstYear, lastYear, stepSize} = sails.config.HTConfig;

                  const header = ['fatalities','serious', 'slight', 'costs'];

                  const retArray = new Array(1 + (lastYear - firstYear) / stepSize).fill([]).map(item => new Array(1 + header.length).fill(0));

                  res.rows.forEach(row => {
                    const rowIndex = parseInt((row.year - firstYear) / stepSize, 10);
                    retArray[rowIndex][0] = row.year;
                    header.forEach((key, index) => {
                      retArray[rowIndex][index + 1] = row[key];
                    });
                  });

                  resolve({
                    target : [
                      sheetsName.SAF,
                      [1, 7]
                    ],
                    data : retArray
                  });
                })
                .catch(reject);
            }));

            var lastYear, modes;

            function reduceSAFByCountry(prev, curr, index) {
              if (index === 0) {
                lastYear = curr.year;
                modes = Object.keys(curr)
                  .filter(function (key) {
                    return key !== 'country' && key !== 'year';
                  });
              }

              if (index === 0 || prev[prev.length - 1][0] !== curr.country) {
                prev.push([curr.country].concat(new Array(modes.length).fill(0)));
              }

              if (lastYear <= curr.year) {
                lastYear = curr.year;
                for (let i = 1, ln = modes.length; i <= ln; i += 1) {
                  prev[prev.length - 1][i] = curr[modes[i-1]];
                }
              }
              return prev;
            }

            tables.forEach(function (table) {
              switch (table[3]) {
              case 'full':
                queries.push(HTDataStock.getSAFInjuriesTable(runMetadata.name, table[1], table[2]));
                break;
              case 'partial':
                queries.push(HTDataStock.getSAFFatAndCostTableCountry(runMetadata.name, table[1]));
                break;
              case 'worldRegions':
                queries.push(HTDataStock.getSAFFatAndCostTableWorldRegion(runMetadata.name, table[1]));
                break;
              }
            });

            function indexToCoordinates(index) {
              var coordinates;
              switch (index) {
              case 1:
                coordinates = [1, 20];
                break;
              case 2:
                coordinates = [1, 55];
                break;
              case 3:
                coordinates = [1, 90];
                break;
              case 4:
                coordinates = [1, 125];
                break;
              case 5:
                coordinates = [1, 160];
                break;
              case 6:
                coordinates = [1, 197];
                break;
              case 7:
                coordinates = [1, 234];
                break;
              case 8:
                coordinates = [1, 271];
                break;
              default :
                console.log('more tables for ' + index);
              }

              return coordinates;
            }

            sails.promise.all(queries)
              .then(function (results) {
                resolve(results
                  .reduce(function (prev, curr, index) {
                    if (index === 0) {
                      prev.push(curr);
                      return prev;
                    }
                    prev.push({
                      target : [
                        sheetsName.SAF,
                        indexToCoordinates(index)
                      ], // decrease index for the table with year values TODO: SAF output more clear
                      data : (tables[index - 1][3] === 'worldRegions' ?
                        curr.rows.reduce(reduceSAFByCountry, []) :
                        curr.rows.reduce(reduceSAFByCountry, [])
                        .reduce((prev, curr, index) => {
                          if (index === 0) {
                            prev.push(['EU28 + NO + CH'].concat(new Array(curr.length - 1).fill(0)));
                          }
                          if (countryOrderPreference(curr[0]) === 1) {
                            for (let i = 1, ln = curr.length; i < ln; i+=1) {
                              prev[0][i] += curr[i];
                            }
                          }
                          prev.push(curr);
                          return prev;
                        }, [])
                        .sort(sortByCountryFirstColumn)
                        .filter(checkIfEU28NOCH)
                      )
                    });
                    return prev;
                  }, []));
            });
          });
        }

        function metadataSheet(runMetadata) {
          return new Promise(resolve => {
            const TPMDefinition = (runMetadata.TPM &&
              sails.uuidTrie.get(runMetadata.TPM) ?
              sails.schema.nodeAttributes[sails.uuidTrie.get(runMetadata.TPM)] :
              null
            );

            const runLabel = (
              TPMDefinition && TPMDefinition.uuid !== 'TPMROOT' &&
              TPMDefinition.uuid !== 'CUSTOMTPMROOT' ?
              TPMDefinition.label : runMetadata.label);

            resolve([{
              target : [
                0,
                [3, 24]
              ],
              data : [[runLabel]]
            }]);
          });
        }

        function airPollutionExternalitySheet(schema) {
          function sheetFactory(schema, type) {
            return new Promise ((resolve, reject) => {
              HTDataStock[(type === 'passenger' ?
                'getAirPassengerPollutionCosts' :
                'getAirFreightPollutionCosts'
              )](schema).then(result => {
                if (!result.data) {
                  return Promise.reject(new Error('Missing data from air pollution.'));
                }
                if (!Array.isArray(result.data)) {
                  return Promise.reject(new Error('The structure of the received air pollution data was wrong.'));
                }

                // Add Empty rows to fill template without overwriting formulas
                const modeOrder = (type === 'passenger' ? {
                  'road' : 1,
                  'rail' : 2,
                  'air' : 3
                } : {
                  'road' : 1,
                  'rail' : 2,
                  'air' : 3,
                  'iww' : 4,
                  'sss' : 5
                });

                const pollutantKeys = [
                  'pm25Pollution',
                  'noxPollution',
                  'nmvocPollution',
                  'so2Pollution'
                ];

                const dataArr = [];

                for (let i = 0, ln = (pollutantKeys.length + 1) * Object.keys(modeOrder).length - 1; i < ln; i += 1) {
                  dataArr[i] = new Array(9).fill(0);
                }

                for (let i = pollutantKeys.length, ln = dataArr.length; i < ln; i += pollutantKeys.length + 1) {
                  dataArr[i] = [];
                }

                result.data.sort((a, b) => {
                  return modeOrder[a.mode] - modeOrder[b.mode];
                });

                result.data.forEach((mode, indexMode) => {
                  mode.values.sort((a, b) => {
                    return parseInt(a.year, 10) - parseInt(b.year, 10);
                  });

                  mode.values.forEach((year, indexYear) => {
                    pollutantKeys.forEach((key, indexKey) => {
                      dataArr[5 * indexMode + indexKey][indexYear] = Math.round(year[key] * 100) / 100;
                    });
                  });

                });

                resolve({
                  target : [
                    'Externalities',
                    (type === 'passenger' ? [3, 22] : [3, 42])
                  ],
                  data : dataArr
                });
              })
              .catch(err => reject(new Error(err)));
            });
          }

          return Promise.all([
            sheetFactory(schema, 'passenger'),
            sheetFactory(schema, 'freight')
          ]);
        }

        function climateChangeExternalitySheet(schema) {
          function sheetFactory(schema, type) {
            return new Promise ((resolve, reject) => {
              HTDataStock[(type === 'passenger' ?
                'getClimateChangePassengerCosts' :
                'getClimateChangeFreightCosts'
              )](schema).then(result => {
                if (!result.data) {
                  return Promise.reject(new Error('Missing data from climate change.'));
                }
                if (!Array.isArray(result.data)) {
                  return Promise.reject(new Error('The structure of the received climate change data was wrong.'));
                }

                // Add Empty rows to fill template without overwriting formulas
                const modeOrder = (type === 'passenger' ? {
                  'road' : 1,
                  'rail' : 2,
                  'air' : 3
                } : {
                  'road' : 1,
                  'rail' : 2,
                  'air' : 3,
                  'iww' : 4,
                  'sss' : 5
                });

                const dataArr = [];
                const timeTicks = 9;

                for (let i = 0, ln = Object.keys(modeOrder).length; i < ln; i += 1) {
                  dataArr[i] = new Array(timeTicks).fill(0);
                }

                result.data.sort((a, b) => {
                  return modeOrder[a.mode] - modeOrder[b.mode];
                });

                result.data.forEach((mode, indexMode) => {
                  mode.values.sort((a, b) => {
                    return parseInt(a.year, 10) - parseInt(b.year, 10);
                  });

                  mode.values.forEach((year, indexYear) => {
                    dataArr[indexMode][indexYear] = Math.round(year.climateChange * 100) / 100;
                  });

                });

                resolve({
                  target : [
                    'Externalities',
                    (type === 'passenger' ? [3, 72] : [3, 76])
                  ],
                  data : dataArr
                });
              })
              .catch(err => reject(new Error(err)));
            });
          }

          return Promise.all([
            sheetFactory(schema, 'passenger'),
            sheetFactory(schema, 'freight')
          ]);
        }

        function singleCostsSheets(schema) {
          return new Promise((resolve, reject) => {
            Promise.all([
              HTDataStock.getUpDownStreamCosts(schema),
              HTDataStock.getMarginalInfrastructureCosts(schema),
              HTDataStock.getAccidentsCosts(schema),
              HTDataStock.getGeneralizedCosts(schema, 'passenger'),
              HTDataStock.getGeneralizedCosts(schema, 'freight')
            ])
            .then(results => {
              try {
                const sheets = results.map((result, index) => {
                  let sheetName, costKey, targetCoordinates, modeOrder, targetSheet;

                  switch (index) {
                    case 0:
                      sheetName = 'Up-downstream processes';
                      costKey = 'updownstream';
                      targetCoordinates = [3, 87];
                      targetSheet = 'Externalities';
                      modeOrder = {
                        'road' : 1,
                        'coach' : 2,
                        'truck' : 3,
                        'railPassenger' : 4,
                        'railFreight' : 5,
                        'air' : 6,
                        'iww' : 7,
                        'sss' : 8
                      };
                      break;
                    case 1:
                      sheetName = 'Marginal infrastructure';
                      costKey = 'marginalinfrastructurecosts';
                      targetCoordinates = [3, 100];
                      targetSheet = 'Externalities';
                      modeOrder = {
                        'cars' : 1,
                        'coaches' : 2,
                        'hdv' : 3,
                        'iww' : 4
                      };
                      break;
                    case 2:
                      sheetName = 'Accidents';
                      costKey = 'fatalities';
                      targetCoordinates = [3, 109];
                      targetSheet = 'Externalities';
                      modeOrder = {
                        'car' : 1,
                        'bike' : 2,
                        'p2w' : 3,
                        'pedestrian' : 4,
                        'publicTransport' : 5,
                        'rail' : 6,
                        'air' : 7,
                        'truck' : 8,
                        'iww' : 9,
                        'sss' : 10
                      };
                      break;
                    case 3:
                      sheetName = 'Passenger generalised transport cost';
                      costKey = 'generalizedcosts';
                      targetCoordinates = [3, 11];
                      targetSheet = 'Costs';
                      modeOrder = {
                        'road' : 1,
                        'rail' : 2,
                        'air' : 3,
                        'coach' : 4
                      };
                      break;
                    case 4:
                      sheetName = 'Freight generalised transport cost';
                      costKey = 'generalizedcosts';
                      targetCoordinates = [3, 21];
                      targetSheet = 'Costs';
                      modeOrder = {
                        'road' : 1,
                        'rail' : 2,
                        'iww' : 3,
                        'sss' : 4,
                        'maritime' : 5
                      };
                      break;
                  }

                  if (!result.data) {
                    return Promise.reject(new Error(`Missing data from ${sheetName}.`));
                  }
                  if (!Array.isArray(result.data)) {
                    return Promise.reject(new Error(`The structure of the received ${sheetName} data was wrong.`));
                  }

                  const dataArr = [];
                  const timeTicks = 9;

                  // Accidents need an extra row to separate car accidents and injuries
                  for (let i = 0, ln = Object.keys(modeOrder).length + (index === 2 ? 1 : 0); i < ln; i += 1) {
                    dataArr[i] = new Array(timeTicks).fill(0);
                  }

                  result.data.sort((a, b) => {
                    return modeOrder[a.mode] - modeOrder[b.mode];
                  });

                  result.data.forEach((mode, indexMode) => {
                    mode.values.sort((a, b) => {
                      return parseInt(a.year, 10) - parseInt(b.year, 10);
                    });

                    // This lambda join serious and slight car injuries
                    mode.values.forEach((index === 2 ?
                      (year, indexYear) => {
                        if (indexMode === 0) {
                          dataArr[indexMode][indexYear] = Math.round((year.severeinjuries + year.slightinjuries) * 100) / 100;
                        }
                        dataArr[indexMode + 1][indexYear] = Math.round(year[costKey] * 100) / 100;
                      } :
                      (year, indexYear) => {
                        dataArr[indexMode][indexYear] = Math.round(year[costKey] * 100) / 100;
                      })
                    );

                  });

                  return {
                    target : [
                      targetSheet,
                      targetCoordinates
                    ],
                    data : dataArr
                  };
                });
                resolve(sheets);
              } catch (e) {
                reject(new Error(e));
              }
            })
            .catch(err => reject(new Error(err)));
          });
        }

        function inputsSheet(runMetadata) {
          const imageHeight = 26;
          const pageIndex = 2;

          function EvaluateFactory(values, initialValue) {
            if (values.representationaData.timeSeries && values.representationaData.timeSeries.length >= 2) {
              const spline = new LinearAlgebra.HermiteSpline(values.representationaData.timeSeries);
              return function (year) {
                year = parseFloat(year, 10);
                if (!Number.isNaN(year) && year >= values.startYear) {
                  return spline.evaluate(year);
                }
                return initialValue;
              }
            }
            return function (year) {
              year = parseFloat(year, 10);
              if (!Number.isNaN(year) && year >= values.startYear) {
                return values.terminalValue;
              }
              return initialValue;
            }
          }

          function leverNames(runMetadata) {
            if (runMetadata.parameters.length) {
              return runMetadata.parameters.map((lever, index) => {
                const leverData = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
                const unit = (leverData.unit ? leverData.unit : 'percentage (100 = baseline)');

                return {
                  target : [
                    pageIndex,
                    [1, 6 + index * imageHeight]
                  ],
                  data : [[`${lever.label} in ${unit}`]],
                  style : 'headerSection'
                };
              });
            }
            return [{
              target : [
                pageIndex,
                [1, 6]
              ],
              data : [[`Baseline assumptions`]],
              style : 'headerSection'
            }];
          }

          function mapTitle(runMetadata) {
            return runMetadata.parameters.map((lever, index) => {
              const leverData = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
              const unit = (leverData.unit ? leverData.unit : 'percentage (100 = baseline)');

              return {
                target : [
                  pageIndex,
                  [5, 6 + index * imageHeight]
                ],
                data : [[`EU28 + NO + CH at NUTS2 Level`]],
                style : 'headerSection'
              };
            });
          }

          function regionalTrajectoriesTitle(runMetadata) {
            return runMetadata.parameters.map((lever, index) => {
              const leverData = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
              const unit = (leverData.unit ? leverData.unit : 'percentage (100 = baseline)');

              return {
                target : [
                  pageIndex,
                  [10, 6 + index * imageHeight]
                ],
                data : [[`Custom region values`]],
                style : 'headerSection'
              };
            });
          }

          function yearValuesTitle(runMetadata) {
            return runMetadata.parameters.map((lever, index) => {
              return {
                target : [
                  pageIndex,
                  [1, 8 + index * imageHeight]
                ],
                data : [['Year', `EU28 Value`]],
                style : 'headerTable'
              };
            });
          }

          function yearValues(runMetadata) {
            return runMetadata.parameters.map((lever, index) => {
              const leverData = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
              const initialValue = (leverData && leverData.initialValue ?
                leverData.initialValue :
                (lever.target && lever.target.__is_relative ? 1 : 0)
               );
              const computeFun = new EvaluateFactory(lever.values, initialValue);
              const computedValues = [];
              const scale = (lever.target && lever.target.__is_relative ? 100 : 1);

              for (let i = 0; i < 9; i += 1) {
                computedValues.push([
                  sails.config.HTConfig.firstYear + i * 5,
                  scale * computeFun(sails.config.HTConfig.firstYear + i * 5)
                ]);
              }

              return {
                target : [
                  pageIndex,
                  [1, 9 + index * imageHeight]
                ],
                style : 'tableContents',
                data : computedValues
              };
            });
          }

          function regionsTrajectories(runMetadata, nutsDictionary) {
            const insertBlocks = [];
            const header = ['Year'];
            const values = [];
            for (let i = 0; i < 9; i += 1) {
              values.push([sails.config.HTConfig.firstYear + i * 5]);
            }
            runMetadata.parameters.forEach((lever, index) => {
              if (lever.values && lever.values.representationaData &&
                lever.values.representationaData.spread && lever.values.representationaData.spread.modifiedNodes
              ) {
                const modifiedNodes = lever.values.representationaData.spread.modifiedNodes;
                const leverData = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
                const initialValue = (leverData && leverData.initialValue ?
                  leverData.initialValue :
                  (lever.target && lever.target.__is_relative ? 1 : 0)
                 );
                const scale = (lever.target && lever.target.__is_relative ? 100 : 1);
                modifiedNodes.forEach(node => {
                  header.push(nutsDictionary[node.key]);
                    const shimEvaluateFactoryValues = {
                      representationaData : {
                        timeSeries : node.timeSeries || []
                      },
                      terminalValue : node.value,
                      startYear : function () {
                        let found = false;
                        for (let i = 1, ln = node.timeSeries.length; i < ln && !found; i += 1) {
                          if (Math.abs(node.timeSeries[i][1] - node.timeSeries[i - 1][1]) > 1e-4) {
                            found =  node.timeSeries[i][0];
                          }
                        }
                        return (found ? found : lever.values.startYear);
                      }()
                   };

                   const evalFun = new EvaluateFactory(shimEvaluateFactoryValues, initialValue);
                   values.map(row => {
                     row.push(Math.round(1000 * scale * evalFun(row[0])) / 1000);
                     return row;
                   });
                 });

                 insertBlocks.push({
                   target : [
                     pageIndex,
                     [10, 8 + index * imageHeight]
                   ],
                   style : 'headerTable',
                   data : [header]
                 });

                 insertBlocks.push({
                   target : [
                     pageIndex,
                     [10, 9 + index * imageHeight]
                   ],
                   style : 'tableContents',
                   data : values.slice(0, values.length - 1)
                 });

                 insertBlocks.push({
                   target : [
                     pageIndex,
                     [10, 9 + (values.length - 1) + index * imageHeight]
                   ],
                   style : 'doubleLowerLine',
                   data : [values[values.length - 1]]
                 });

              }
            });
            return insertBlocks;
          }

          function freeStyles(runMetadata) {
            const maxCustomsRegions = runMetadata.parameters.reduce((max, lever) => {
              if (lever.values && lever.values.representationaData &&
                lever.values.representationaData.spread && lever.values.representationaData.spread.modifiedNodes
              ) {
                max = Math.max(max, lever.values.representationaData.spread.modifiedNodes.length);
              }
              return max;
            }, 0);

            return runMetadata.parameters.map((lever, i) => {
              const chunk = {
                target : [
                  pageIndex
                ],
                type : 'styles',
                data : [
                  {
                    style : 'doubleLowerLine',
                    targets : [
                      [
                        [1, 17 + i * imageHeight],
                        [2, 17 + i * imageHeight]
                      ],[
                        [1, 30 + i * imageHeight],
                        [10 + maxCustomsRegions, 30 + i * imageHeight]
                      ]
                    ]
                  }
                ]
              };
              return chunk;
            });

          }


          function nuts2Maps(runMetadata) {
            const coordinates = {
              upperLeft : [4, 8],
              lowerRight : [8, 36]
            };

            return new Promise((resolve, reject) => {
              const maps = runMetadata.parameters
              .reduce((promiseChain, lever) => {
                const {is_geo_active_n0, is_geo_active_n2} = sails.schema.nodeAttributes[sails.uuidTrie.get(lever.uuid)];
                if (is_geo_active_n0 == 1 || is_geo_active_n2 == 1) {
                  promiseChain.push(new Promise((inResolve, inReject) => {
                    GISService.generateMAP(lever, 'svg', 'nuts2', (err, map) => {
                      if (err) {
                        inReject(new Error(err));
                      } else {
                        inResolve(map);
                      }
                    });
                  }));
                } else {
                  promiseChain.push(Promise.resolve(null));
                }
                return promiseChain;
              }, []);

              Promise.all(maps)
                .then((svgMaps) => {
                  const mapList = svgMaps.reduce((acum, svg, index) => {
                    if (svg !== null) {
                      acum.push({
                        target : [
                          pageIndex,
                          [coordinates.upperLeft[0], coordinates.upperLeft[1] + imageHeight * index],
                          [coordinates.lowerRight[0], coordinates.lowerRight[1] + imageHeight * index]
                        ],
                        type : 'imageBase64',
                        data : ['svg', new Buffer(svg).toString('base64')]
                      });
                    }
                    return acum;
                  }, []);
                  resolve(mapList);
                })
                .catch(reject);
            });
          }

          return new Promise((resolve, reject) => {
            Promise.all([
              nuts2Maps(runMetadata)
            ])
            .then(contents => {
              GISService.nutsCodes.then(nutsDictionary => {
                let retArr = contents.reduce((arr, content) => {
                  content.forEach(insertBlock => {
                    arr.push(insertBlock);
                  });
                  return arr;
                }, []);

                try {
                  retArr = retArr.concat(leverNames(runMetadata));
                  retArr = retArr.concat(yearValuesTitle(runMetadata));
                  retArr = retArr.concat(yearValues(runMetadata));
                  retArr = retArr.concat(mapTitle(runMetadata));
                  retArr = retArr.concat(regionalTrajectoriesTitle(runMetadata));
                  retArr = retArr.concat(regionsTrajectories(runMetadata, nutsDictionary));
                  retArr = retArr.concat(freeStyles(runMetadata));
                  resolve(retArr);
                } catch (err) {
                  return Promise.reject(err);
                }
              });
            })
            .catch(reject => {
              reject(err);
              console.log(err.stack);
            });
          });
        }

        sails.promise.all([
          metadataSheet(runMetadata),
          inputsSheet(runMetadata),
          airPollutionExternalitySheet(runMetadata.name),
          climateChangeExternalitySheet(runMetadata.name),
          singleCostsSheets(runMetadata.name),
          createDEMSheet(runMetadata.name),
          createECRSheet(runMetadata.name),
          createVESSheet(runMetadata.name),
          createPADSheet(runMetadata.name),
          createFRDSheet(runMetadata.name),
          createENVSheet(runMetadata.name),
          createSAFSheet(runMetadata.name)
        ])
          .then(function (results) {
            results = JSON.stringify(results
              .reduce((prev, curr) => {
                return prev.concat(curr);
              }, [])
              .filter(item => {
                return (item.data.length > 0);
              }));
            const xlsxstyles = {
              headerSection : {
                fontBold : true,
                fontSize : 13,
              },
              headerTable : {
                fontSize : 11,
                fontBold : true,
                align : 'CENTER',
                border : {
                  color : [[255, 0, 0]],
                  style : ['NONE', 'NONE', 'DOUBLE', 'NONE']
                }
              },
              tableContents : {
                align : 'CENTER'
              },
              doubleLowerLine : {
                align : 'CENTER',
                border : {
                  color : [[255, 0, 0]],
                  style : ['NONE', 'NONE', 'DOUBLE', 'NONE']
                }
              }
            }


            TemplateCompiler.compileXlsxTemplate('HTPolicyReportTemplate.xlsx',
              `${results}`, xlsxstyles, res);
            res.setHeader('Content-disposition', 'attachment; filename=output.xlsx');
            res.setHeader('X-Filename', 'output.xls');
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          })
          .catch(function (err) {
            return res.status(500).send(err.stack);
          });
        });
    },
    downloadTable : function (req, res) {
      var searchParams = {
        id : req.params.schemaId
      };
      if (!req.user.isAdmin) {
        searchParams.owner = req.user.id;
      }

      HTModelRuns.findOne(searchParams)
        .then(function (result) {
          if (result || req.params.schemaId === sails.config.HTConfig.baseSchema) {
            pg.connect(sails.config.connections.HTDataStock, function (err, client, done) {
              if (err) {
                return res.status(500).send(err);
              }

              const searchSchema = (req.params.schemaId === sails.config.HTConfig.baseSchema ? sails.config.HTConfig.baseSchema : sails.config.HTConfig.schemaPrefix + req.params.schemaId),
                sqlQuery = `SELECT * FROM ${searchSchema}.${req.params.tableId};`,
                stream = client.query(new QueryStream(sqlQuery));

              let header = null;

              res.setHeader('Content-disposition', 'attachment; filename=' + req.params.tableId + '.csv');
              res.setHeader('x-filename', req.params.tableId + '.csv');
              res.setHeader('Content-type', 'text/csv');

              function closeCursorAndResponse() {
                res.end();
                done();
              }

              stream
                .on('end',closeCursorAndResponse)
                .on('close', closeCursorAndResponse)
                .on('data', (chunk) => {
                  if (!header) {
                    header = Object.keys(chunk);
                    res.write(`${header.join(';')}\n`);
                  }
                  const row = header.reduce(function (prev, curr) {
                    prev.push(chunk[curr]);
                    return prev;
                  }, []).join(';');

                  res.write(row + '\n');
                })
                .on('error', function (err) {
                  sails.log(err);
                  closeCursorAndResponse();
                });
            });
          } else {
            return res.status(404).send('Schema not found in your schemata list.');
          }
        })
        .catch(function (err) {
          return res.status(500).send(err);
        });
    },
    uploadTable : function (req, res) {
      const validMimeTypes = ['text/csv', 'text/comma-separated-values', 'text/csv', 'application/csv', 
        'application/vnd.ms-excel', ''];
      const searchParams = {
        id : req.params.schemaId
      };

      if (!req.user.isAdmin) {
        searchParams.owner = req.user.id;
      }

      // Check correctness of body
      if (!req.body) {
        return res.status(400).send('Missing file data.');
      }

      if (!req.body.filename) {
        return res.status(400).send('Missing file name.');
      }

      const tableName = req.body.filename.split('.')[0];
      if (!/^(i|o|p|_)_[a-zA-Z_]+$/.test(tableName)) {
        return res.status(400).send(`The name of the file points to the invalid table ${tableName}`);
      }

      if (!req.body.mime && req.body.mime !== '') {
        return res.status(400).send('Missing file mime.');
      }

      if (validMimeTypes.indexOf(req.body.mime) === -1) {
        return res.status(400).send('Unsupported file mime.');
      }

      switch (req.body.encoding) {
        case 'base64':
          req.body.data = new Buffer(req.body.data, 'base64');
          break;
        case 'utf8':
          req.body.data = new Buffer(req.body.data, 'utf8');
          break;
        default :
        return res.status(400).send('Unsupported enconding.');
      }

      HTModelRuns.findOne(searchParams)
        .then(runInfo => {
          if (runInfo) {
            pg.connect(sails.config.connections.HTDataStock, (err, client, done) => {
              if (err) {
                return res.status(500).send(err);
              }

              const schemaName = sails.config.HTConfig.schemaPrefix + runInfo.id;

              client.query(`SELECT table_name
                            FROM information_schema.tables
                            WHERE table_schema = $1 AND table_name = $2;`, [
                              schemaName,
                              tableName
              ], (err, result) => {
                if (err) {
                  done();
                  return res.status(500).send(err);
                }

                if (result.rows.length === 0) {
                  return res.status(401).send(`The table ${tableName} does not exist in your schema.`);
                }
                const stream = require('stream');
                const bufferStream = new stream.PassThrough();
                bufferStream.end(req.body.data);

                client.query(`BEGIN`, err => {
                  if (err) {
                    done();
                  } else {
                    client.query(`DELETE FROM ${schemaName}.${tableName};`, err => {
                      if (err) {
                        client.query('ROLLBACK;', () => {
                          done();
                          return res.status(500).send(err);
                        });
                      } else {
                        const copyFrom = require('pg-copy-streams').from;
                        const pgStream = client.query(copyFrom(`COPY ${schemaName}.${tableName} FROM STDIN WITH DELIMITER ';' CSV HEADER;`));

                        const uploaded = new Promise((resolve, reject) => {
                          function endOrFinishCb() {
                            client.query('COMMIT;', (err) => {
                              done();
                              if (err) {
                                reject(err);
                              } else {
                                resolve()
                              }
                            });
                          }

                          bufferStream.pipe(pgStream)
                            .on('error', (err) => {
                              client.query('ROLLBACK', () => {
                                done();
                                reject(err);
                              });
                            })
                            .on('finish', endOrFinishCb)
                            .on('end', endOrFinishCb);
                        });

                        uploaded
                          .then(() => res.status(202).send(''))
                          .catch(err => res.status(500).send(err));
                      }
                    });
                  }
                });
              });
            });
          } else {
            return res.status(404).send('Schema not found in your schemata list.');
          }
        });
    },
    updateHypernetworkEdges : function (req, res) {
      if (!Array.isArray(req.body.edges)) {
        return res.status(400).send('You must provide a list of edges using application/vnd.jgf+json structure');
      }
      if (req.body.edges.length === 0) {
        return res.status(200);
      }

      checkOwnerShip(req, res, (req, res, runMetadata) => {
        GISService.updateHypernetworkEdges(`${sails.config.HTConfig.schemaPrefix}${runMetadata.id}`, req.params.modeId, req.body.edges, (err, result) => {
          if (err) {
            return res.status(500).send(err);
          }
          return res.status(200).send(result);
        });
      });
    },
    updateHypernetworkNodes : function (req, res) {
      if (!Array.isArray(req.body.nodes)) {
        return res.status(400).send('You must provide a list of nodes using application/vnd.jgf+json structure');
      }

      if (req.body.nodes.length === 0) {
        return res.status(200);
      }

      checkOwnerShip(req, res, (req, res, runMetadata) => {
        GISService.updateHypernetworkNodes(`${sails.config.HTConfig.schemaPrefix}${runMetadata.id}`, req.params.modeId, req.body.nodes, (err, result) => {
          if (err) {
            return res.status(500).send(err);
          }
          return res.status(200).send(result);
        });
      });
    },
    getHyperNetwork : function (req, res) {
      checkOwnerShip(req, res, (req, res, runMetadata) => {
        const where = Object.keys(req.query).reduce((acum, queryKey) => {
          acum.push({
            type : 'AND',
            comparator : '=',
            column_name : queryKey,
            value : req.query[queryKey]
          });
          return acum;
        }, []);

        GISService.getHyperNetwork(`${sails.config.HTConfig.schemaPrefix}${runMetadata.id}`, where, (err, HyperNetwork) => {
          if (err) {
            return res.status(500).send(err);
          }
          return res.set({
            'Cache-Control' : 'no-cache, no-store, must-revalidate',
            'Pragma' : 'no-cache',
            'Expires' : 0
          }).json(HyperNetwork);
        });
    });
  }
  };
}());
