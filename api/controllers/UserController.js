/*jslint node: true, white : true, es6 : true, esnext : true*/
/*globals sails, User, Promise*/
(function () {
  'use strict';
  /**
   * UserController
   *
   * @description :: Server-side logic for managing users
   * @help        :: See http://links.sailsjs.org/docs/controllers
   */

  module.exports = {
    create: function (req, res) {
      if (!req.user || !req.user.isAdmin) {
        return res.status(403).send('Only administrators can create users');
      }

      User.create(req.body)
        .then(newUser => {
          return res.status(202).json({
            id: newUser.id,
            name : newUser.name,
            email: newUser.email,
            isAdmin: newUser.isAdmin
          });
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    },
    destroy : function (req, res) {
      if (!req.user.isAdmin) {
        return res.status(403).send('Only administrators can delete user accounts.');
      }

      if (parseInt(req.user.id, 10) === parseInt(req.params.id, 10)) {
        return res.status(401).send('You cannot delete your own user.');
      }

      User.destroy({id: req.params.id})
        .then(() => {
          return res.status(204).send('Ok');
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    },
    editById : function (req, res) {
      if (!req.user.isAdmin && req.user.id !== req.params.id) {
        return res.status(403).send('Only administrators can alter user data.');
      }

      const newData = ['name', 'password', 'email'].reduce(function (prev, key) {
        if (req.body[key]) {
          prev[key] = req.body[key];
        }
        return prev;
      }, {});

      User.update({id : req.params.id}, newData)
        .then(updatedRecords => {
          if (updatedRecords.length) {
            return res.status(200).json({id : updatedRecords[0].id});
          }
          return res.status(404).send('User not found');
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    },
    getQuota : function (req, res) {
      if (!req.user.isAdmin && req.user.id !== req.params.id) {
        return res.unauthorized();
      }

      User.findOne({id : req.params.id})
        .then(result => {
          if (result) {
            return res.json({schemaQuota: result.schemaQuota});
          }
          return res.status(404).send('User not found.');
        })
        .catch(function (err) {
          return res.status(500).send(err);
        });
    },
    list : function (req, res) {
      let queryParams = ['name', 'id', 'email'].reduce(function (prev, key) {
        if (req.query[key]) {
          prev[key] = req.query[key];
        }
	      return prev;
      }, {});

      if (!req.user.isAdmin) {
        queryParams.id = req.user.id;
      }

      User.find(queryParams)
        .then(users => {
          return res.json(users);
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    },
    userDetails : function (req, res) {
      if (req.user.isAdmin || req.params.id === req.user.id) {
        User.findOne({id : req.params.id})
          .then(user => {
            if (user) {
              return res.json(user);
            }
            return res.notFound();
          })
          .catch(err => {
            return res.status(500).send(err);
          });
      } else {
        return res.forbidden();
      }
    },
    setAdmin : function (req, res) {
      if (!req.user.isAdmin) {
        return res.status(403).send('Only administrators can change admin status');
      }

      if (req.user.id === req.params.id) {
        return res.status(401).send('Change own admin privileges can result in unexpected behaviour');
      }

      User.update({id : req.params.id}, {isAdmin : req.body.isAdmin})
        .then(updatedRecords => {
          if (updatedRecords.length) {
            return res.status(200).json({id : updatedRecords[0].id});
          }
          return res.status(404).send('User not found');
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    }
  };
}());
