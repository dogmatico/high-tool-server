/*jslint node: true, es6 : true, esnext : true*/
(function () {
  /**
   * Controller to handle request related to HIGH-TOOL Java Modules
   */
  'use strict';

  module.exports = {
    listModules : function (req, res) {
      if (!req.user || !req.user.isAdmin) {
        res.status(403).send('Only administrators can list master servers.');
      } else {
        HTServers.find({})
          .then(result => res.json(result))
          .catch(err => {
            res.status(500).send(err);
          });
      }
    },
    sendTemplateCompiler : function (req, res) {
      const fs = require('fs'),
        filePath = `${process.cwd()}/java_modules/HTTemplateCompiler.jar`;

        fs.stat(filePath, err => {
          if (err) {
            return res.status(404).send('HT Template compiler not found.');
          }

          const fileStream = fs.createReadStream(filePath);
          res
            .set({
              'Content-disposition' : `attachment; filename=HTTemplateCompiler.jar`,
              'X-Filename' :  `HTTemplateCompiler.jar`,
              'Content-Type' : 'application/java-archive'
            });

          fileStream.pipe(res);
        });
    },
    getInstalledModules : function (req, res) {
      var result = {};

      function getInstalledInMaster(masterServer) {
        return new sails.promise(function (resolve, reject) {
          const https = require('https'),
            options = {
              host : masterServer.host || null,
              port : masterServer.port || 1337,
              path: `/api/modules`
            },
            request = https.request(options, function(response) {
              var retObj = {
                serverId : masterServer.id,
                data : ''
              };

              response
                .setEncoding('utf-8')
                .on('response', msg => {
                  if (msj.status !== 200) {
                    resolve({result: 'failure', status : msg.statusCode, msg : msg.statusMessage, server : `${options.host}:${options.port}`});
                  }
                })
                .on('data', chunk => {
                  retObj.data += chunk;
                })
                .on('error', err => {
                  resolve({result: 'failure', status : 500, msg : err, server : `${options.host}:${options.port}`});
                })
                .on('end', () => {
                  retObj.data = JSON.parse(retObj.data);
                  retObj.result = 'ok';
                  resolve(retObj);
                });
            });

            request
              .on('error', err => {
                resolve({result: 'failure', status : 500, msg : err, server : `${options.host}:${options.port}`});
              });
            request.end();
        });
      }

      HTModulesService.getModuleVersions(process.cwd())
        .then(moduleVersions => {
          result.installed = moduleVersions;
          return HTServers.find({});
        })
        .then(masterServers => {
          var masterRequests = [];
          masterServers.forEach(server => {
            masterRequests.push(getInstalledInMaster(server));
          });
          return sails.promise.all(masterRequests);
        })
        .then(masterResponses => {
          result.serverFailures = masterResponses
            .filter(item => {
              return (item === null || item.result !== 'ok');
            });

          masterResponses
          .filter(item => {
            return (item !== null && item.result === 'ok');
          })
          .forEach(response => {
            Object.keys(response.data).forEach(HTModule => {
              if (!result[HTModule]) {
                result[HTModule] = {
                  name : HTModule,
                  version : 'NA'
                };
              }

              if (result[HTModule].update &&
                (result[HTModule].version == 'NA' || result[HTModule].version < response.data[HTModule].version)) {
                  if (!result[HTModule].update || result[HTModule].update.version < response.data[HTModule].version) {
                    result[HTModule].update = {
                      serverId : response.serverId,
                      version : response.data[HTModule].version
                    };
                  }
                }
              });
            });
            return res.json(result);
          })
          .catch(err => {
            return res.status(500).send(err);
          });
    },
    registerMasterServer : function (req, res) {
      if (!req.user || !req.user.isAdmin) {
        return res.status(403).send('Only administrators can register new master servers');
      }

      HTServers.create({
        host : req.body.host,
        port : req.body.port || null,
        description : req.body.description || 'New master server'
      })
        .then(newServer => {
          return res.status(202).json(newServer);
        })
        .catch(err => {
          return res.status(500).send(err);
        });
    },
    deleteMasterServer : function (req, res) {
      if (!req.user || !req.user.isAdmin) {
        res.status(403).send('Only administrators can delete master servers');
      } else {
        HTServers.destroy({id : req.params.serverId})
          .then(() => {
            return res.status(202).send('ok');
          })
          .catch(err => {
            return res.status(500).send(err);
          });
      }
    },
    serveModule : function (req, res) {
      const moduleManifest = {
        DEM : [{
            filename : 'java_modules/Demography.jar'
          },
          {
            filename : 'java_modules/config/demography.cfg',
            conceal : [
              'dbURL',
              'user',
              'password'
            ]
        }],
        ECR : [{
            filename : 'java_modules/Economy.jar'
          },
          {
            filename : 'java_modules/config/economy.cfg',
            conceal : [
              'dbURL',
              'user',
              'password'
            ]
        }],
        FRD : [{
            filename : 'java_modules/FreightDemand.jar'
          },
          {
            filename : 'java_modules/config/freight_demand.cfg',
            conceal : [
              'dbURL',
              'user',
              'password'
            ]
        }],
        PAD : [
          {
            filename : 'java_modules/PassModule.jar'
          },
          {
            filename : 'java_modules/config/passenger.cfg',
            conceal : [
              'dbURL',
              'user',
              'password'
            ]
          },
          {
            filename : 'java_modules/config/passenger.cfg'
          },
          {
            filename : 'java_modules/config/_pd_airic_purposes.csv'
          },
          {
            filename : 'java_modules/config/_pd_airic_regions_eu.csv'
          },
          {
            filename : 'java_modules/config/_pd_airic_regions_row.csv'
          },
          {
            filename : 'java_modules/config/_pd_core_modes.csv'
          },
          {
            filename : 'java_modules/config/_pd_core_purposes.csv'
          },
          {
            filename : 'java_modules/config/_pd_core_regions.csv'
          },
          {
            filename : 'java_modules/config/_pd_urban_modes.csv'
          },
          {
            filename : 'java_modules/config/_pd_urban_regions.csv'
          },
          {
            filename : 'java_modules/config/README.txt'
          },
          {
            filename : 'java_modules/data/i_pd_airic_exogenous_table_v52.csv'
          },
          {
            filename : 'java_modules/data/i_pd_core_exogenous_table_v54_04_rev608.csv'
          },
          {
            filename : 'java_modules/data/i_pd_core_exogenous_table_v53_rev561.csv'
          },
          {
            filename : 'java_modules/data/i_pd_core_eureftrend_pkm_car.csv'
          },
          {
            filename : 'java_modules/data/i_pd_urban_delta_utility__no_TPMs.csv'
          },
          {
            filename : 'java_modules/data/i_pd_urban_exogenous_table_v9.csv'
          },
          {
            filename : 'java_modules/data/o_vs_cstavggen__v54_04_baseline_rev608.csv'
          },
          {
            filename : 'java_modules/data/p_pd_core_tollvkm_v8.csv'
          },
          {
            filename : 'java_modules/data/p_pd_urban_calib6modes_2010_v18.csv'
          },
          {
            filename : 'java_modules/data/p_pd_urban_trend6modes_euref_v18.csv'
          },
          {
            filename : 'java_modules/data/i_pd_core_travellevel_v53.csv'
          }
        ],
        VES : [{
          filename : 'java_modules/Vehstock.jar'
        },
        {
          filename : 'java_modules/config/vehicle_stock.cfg',
          conceal : [
            'dbURL',
            'user',
            'password'
          ]
        }],
        SAF : [{
          filename : 'java_modules/Safety.jar'
        },
        {
          filename : 'java_modules/config/safety.cfg',
          conceal : [
            'dbURL',
            'user',
            'password',
            'PathExcel',
            'PathFolder'
          ]
        }],
        ENV : [{
          filename : 'java_modules/Environment.jar'
        },
        {
          filename : 'java_modules/config/environment.cfg',
          conceal : [
            'dbURL',
            'user',
            'password'
          ]
        }]
      };

      var AdmZip = require('adm-zip'),
        zip = new require('jszip')(),
        fs = require('fs');

      if (!req.params || !req.params.moduleId || !moduleManifest[req.params.moduleId]) {
        return res.status(401).send('Wrong module');
      }

      function addArchive(zipFile, fileData) {
        return new Promise(function (resolve, reject) {
          fs.stat(fileData.filename, err => {
            if (err) {
              reject(err);
            } else {
              if (fileData.conceal) {
                fs.readFile(fileData.filename, 'utf8', (err, data) => {
                  if (err) {
                    reject(err);
                  } else {
                    const concealRegExp = new RegExp(`(${fileData.conceal.join('|')})\\s*=\\s*(.*)`, 'g');
                    zip.file(fileData.filename, new Buffer(data.replace(concealRegExp, '$1 = xxx')), {
                      binary : true
                    });
                    resolve();
                  }
                });
              } else {
                fs.readFile(fileData.filename, (err, data) => {
                  zip.file(fileData.filename, data,{
                    binary : true
                  });
                  resolve();
                });
              }
            }
          });
        });
      }

      var readQueue = [];
      moduleManifest[req.params.moduleId].forEach(fileData => {
        readQueue.push(addArchive(zip, fileData));
      });

      sails.promise.all(readQueue)
        .then(() => {
          res
            .set({
              'Content-disposition' : `attachment; filename=module_${req.params.moduleId}.zip`,
              'X-Filename' :  `module_${req.params.moduleId}.zip`,
              'Content-Type' : 'application/zip'
            });

          zip
            .generateNodeStream()
            .pipe(res);
        });

    },
    fetchAndUpdate : function (req, res) {
      if (!req.user.isAdmin) {
        return res.status(403).send('Only administrators can request a module update.');
      }

      function fetchModule(masterServer) {
        if (masterServer === null) {
          return Promise.reject({status : 404, msg : 'Master server not found'});
        }

        return new Promise((resolve, reject) => {
          const https = require('https');

          https.get(`https://${masterServer.host}/api/modules/${req.params.moduleId}`, (response) => {
            var data = [];

            response
              .on('response', msg => {
                if (msj.status !== 200) {
                  reject({status : msg.statusCode, msg : msg.statusMessage});
                  return response.end();
                }
              })
              .on('data', chunk => {
                data.push(chunk);
              })
              .on('err', err => {
                return reject({status : err.status, msg : err.msg});
              })
              .on('end', () => {
                return resolve(Buffer.concat(data));
              });
          });
        });
      }

      function unzipModule(data) {
        const path = require('path');
        try {
          const Zip = new require('adm-zip');
          const zip = new Zip(new Buffer(data, 'binary'));
          const folderToUnzip = path.resolve(__dirname, '..', '..');
          const {updateModulesConfig} = require(path.resolve(__dirname, '..', '..', 'configUtils', 'updateModelsConfig.js'));

          zip.extractAllTo(folderToUnzip, true);
          const URLConnection = path.resolve(__dirname, '..', '..', 'config', 'connections.js');
          if (require.cache[URLConnection]) {
            delete require.cache[URLConnection];
          }
          const {HTDataStock} = require(URLConnection).connections;
          return updateModulesConfig(path.resolve(__dirname, '..', '..'), HTDataStock);
        } catch (e) {
          console.error(e.stack);
          return Promise.reject(e.stack);
        }
      }

      HTServers.findOne({id : req.params.serverId})
        .then(fetchModule)
        .then(unzipModule)
        .then(() => {
          sails.controllers.notifications.sendMessageToAdmins('module-update', {
            message : `Updated module ${req.params.moduleId}`
          });
          res.status(202).send('Ok');
        })
        .catch(function (err) {
          sails.controllers.notifications.sendMessageToAdmins('module-update', {
            message : `Failed to updated module ${req.params.moduleId}`
          });
          if (err.status) {
            return res.status(err.status).send(err.msg);
          }
          return res.status(500).send(err);
        });
    }
  };
}());
