/*jslint node: true, es6 : true, white : true*/
/*globals sails, Package*/
(function () {
  'use strict';
  /**
   * PackageController
   *
   * @description :: Server-side logic for managing packages
   * @help        :: See http://links.sailsjs.org/docs/controllers
   */
  function getPackageRoot(packageType) {
    var rootNode = [];
    switch (packageType) {
    case 'framework':
      rootNode.push({'rootNode' :'FCROOT'});
      break;
    case 'combined':
    case 'custom':
    case 'policy':
      rootNode.push({'rootNode' : 'TPROOT'});
      rootNode.push({'rootNode' : {'like' : '%TPM%'}});
      break;
    case 'scenario':
      rootNode.push({'rootNode' :'SCROOT'});
      break;
    case 'output':
      rootNode.push({'rootNode' :'OUROOT'});
      break;
    }
    return rootNode;
  }

  function findAndDeleteMetadata(searchParams, userData, listPublic) {
    return Package.find(searchParams)
      .populate('owner')
      .then(function (result) {
        return result
          filter(item => {
            if (listPublic) {
              return (userData.isAdmin || userData.id === item.owner.id || item.public);
            }
            return (userData.isAdmin || userData.id === item.owner.id);
          })
          .map(function (item) {
            if (item.owner) {
              delete item.owner.isAdmin;
              delete item.owner.createdAt;
              delete item.owner.updatedAt;
            }
            return {
              id: item.id,
              label: item.label,
              headline: item.headline,
              abstract: item.abstract,
              owner: item.owner,
              rootNode: item.rootNode,
              updatedAt : item.updatedAt
            };
          });
      });
  }

  module.exports = {
    compare: function (req, res) {
      function compareSpread(valueOne, valueTwo) {
        var bothHaveAndIsEqual = (valueOne.representationaData.spread && valueTwo.representationaData.spread && (valueOne.representationaData.spread.id === valueTwo.representationaData.spread.id)),
          bothDontHave = (!valueOne.representationaData.spread && !valueTwo.representationaData.spread);
        return (bothDontHave || bothHaveAndIsEqual);
      }

      function compareTerminalValues(valueOne, valueTwo) {
        return Math.abs(valueOne.terminalValue - valueTwo.terminalValue) < 1e-4;
      }

      function compareNUTS(valueOne, valueTwo) {
        var result;
        function sortByKey(a, b) {
          return a.key > b.key;
        }
        if (valueOne.representationaData.spread && valueTwo.representationaData.spread) {
          if (valueOne.representationaData.modifiedNodes.length === valueTwo.representationaData.modifiedNodes.length) {
            valueOne.representationaData.modifiedNodes.sort(sortByKey);
            valueTwo.representationaData.modifiedNodes.sort(sortByKey);

            result = valueOne.representationaData.modifiedNodes.every(function(item, i) {
              return (item.key === valueTwo.representationaData.modifiedNodes[i].key && Math.abs(item.value - valueTwo.representationaData.modifiedNodes[i].value) < 1e-4);
            });
          }
        }
        return result !== false;
      }

      function compareIntermediatePoints(valueOne, valueTwo) {
        var result;
        if (valueOne.representationaData.timeSeries.length !== valueTwo.representationaData.timeSeries.length) {
          result = false;
        } else {
          result = valueOne.representationaData.timeSeries.every(function (point, index) {
            return point.every(function (coordinate, compNum) {
              return Math.abs(coordinate - valueTwo.representationaData.timeSeries[index][compNum]) < 1e-4;
            });
          });
        }
        return result !== false;
      }

      sails.promise.all([
        Package.findOne({id : req.params.idOne}),
        Package.findOne({id : req.params.idTwo})
      ])
        .then(function (results) {
          var packageOneData = new sails.Trie();

          if (req.user.isAdmin === true || ((req.user.id === results[0].owner || results[0].public === true) && (req.user.id === results[1].owner || results[1].public === true))) {
            if (results[0].rootNode !== results[1].rootNode) {
              return sails.promise.reject({code: 400, message : "The selected packages are not of the same kind."});
            }

            results[0].nodeList.forEach(function (node) {
              packageOneData.add(node.uuid, node.values);
            });

            const differences = results[1].nodeList.reduce(function (prev, curr) {
              const packageOneItem = packageOneData.get(curr.uuid),
                packageTwoItem = curr.values;

              if (packageOneItem === false) {
                prev.push({
                  uuid : curr.uuid,
                  cause : 'Node not found in package one.'
                });
              } else if (!compareTerminalValues(packageOneItem, packageTwoItem)) {
                prev.push({
                  uuid : curr.uuid,
                  cause : 'Different terminal value.'
                });
              } else {
                if (!compareIntermediatePoints(packageOneItem, packageTwoItem)) {
                  prev.push({
                    uuid : curr.uuid,
                    cause : 'Used different intermediate points to interpolate trajectory.'
                  });
                }
                if (!compareSpread(packageOneItem, packageTwoItem)) {
                  prev.push({
                    uuid : curr.uuid,
                    cause : 'Used different spatial distribution.'
                  });
                } else if (!compareNUTS(packageOneItem, packageTwoItem)) {
                  prev.push({
                    uuid : curr.uuid,
                    cause : 'Found NUTS with different custom values.'
                  });
                }
              }
              return prev;
            }, []);

            return res.json(differences);
          }
          return sails.promise.reject({code: 403, message : "You are not the owner of the packages."});
        })
        .catch(function (err) {
          if (err.code) {
            return res.status(err.code).send(err.message);
          }
          return res.status(500).send(err);
        });
    },
    list: function (req, res) {
      findAndDeleteMetadata({'or' : getPackageRoot(req.params.packageType)}, req.user, true)
        .then(function (results) {
          return res.json(results);
        })
        .catch(function (err) {
          sails.log(err);
          return res.status(500).send(err);
        });
    },
    listOwned : function (req, res) {
      findAndDeleteMetadata({'or' : getPackageRoot(req.params.packageType)}, req.user, false)
        .then(function (results) {
          return res.json(results);
        })
        .catch(function (err) {
          sails.log(err);
          return res.status(500).send(err);
        });
    },
    print: function (req, res) {
      Package.findOne({id : req.params.id})
        .populate('owner')
        .then(function (result) {
          if (result === undefined) {
            return sails.promise.reject({code: 404, message : "Package not found"});
          }
          if (req.user.isAdmin === true || req.user.id === result.owner || result.public === true) {
            if (result.owner) {
              delete result.owner.isAdmin;
              delete result.owner.createdAt;
              delete result.owner.updatedAt;
            }
            return res.json(result);
          }
          return sails.promise.reject({code: 403, message : "You are not the owner of the package."});
        })
        .catch(function (err) {
          if (err.code) {
            return res.status(err.code).send(err.message);
          }
          return res.status(500).send(err);
        });
    },
    printMetadata: function (req, res) {
      Package.findOne({id : req.params.id})
        .populate('owner')
        .then(function (result) {
          if (result === undefined) {
            return res.status(404).send('Package not found.');
          }
          if (req.user.isAdmin === true || req.user.id === result.owner || result.public === true) {
            if (result.owner) {
              delete result.owner.isAdmin;
              delete result.owner.createdAt;
            }
            delete result.nodeList;
            return res.json(result);
          }
          return res.status(403).send('You are not the owner of the package.');
        })
        .catch(function (err) {
          return res.status(500).send(err);
        });
    },
    create: function (req, res) {
      const newPackageData = {
        label : req.body.label,
        headline: req.body.headline || '',
        abstract: req.body.abstract || '',
        owner : req.user.id,
        rootNode : req.body.rootNode,
        nodeList : req.body.nodeList,
        data : req.body.data || {},
        subpackages : req.body.subpackages || []
      };

      Package.create(newPackageData)
        .then(function (result) {
          return res.status(202).json(result);
        })
        .catch(function (err) {
          return res.status(500).send(err);
        });
    },
    update: function (req, res) {
      Package.findOne({id : req.params.id})
        .then(function (packageData) {
          if (packageData === undefined) {
            return sails.promise.reject({code: 404, message : "Package not found"});
          }
          if (!req.user.isAdmin && req.user.id !== packageData.owner) {
            return sails.promise.reject({code: 403, message : "You are not the owner of the package"});
          }
          return Package.update(
            {id : req.params.id},
            { label : req.body.label,
              headline : req.body.headline,
              abstract : req.body.abstract,
              nodeList : req.body.nodeList,
              data : req.body.data || {},
              subpackages: req.body.subpackages
              }
          );
        })
        .then(function (newValue) {
          if (newValue.length) {
            return res.json(newValue[0]);
          }
          return res.status(404).send('Package not found.');
        })
        .catch(function (err) {
          if (err.code) {
            return res.status(err.code).send(err.message);
          }
          return res.status(500).send(err);
        });
    },
    destroy: function (req, res) {
      Package.findOne({id : req.params.id})
        .then(function (result) {
          if (req.user.isAdmin === true || result.owner === req.user.id) {
            return Package.destroy({id: result.id});
          }
          return sails.promise.reject({code : 403, message: "You do not own the selected package."});
        })
        .then(function () {
          return res.status(204).send('Deleted');
        })
        .catch(function (err) {
          if (err.code) {
            return res.status(err.code).send(err.message);
          }
          return res.status(500).send(err);
        });
    }
  };
}());
