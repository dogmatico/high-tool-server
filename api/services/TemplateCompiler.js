/*jslint node: true, indent: 2, es6 : true, esnext : true */
(function () {
  'use strict';

  function compileTemplate(template, data, styles, targetStream) {
    var jvm = require('child_process').fork('workers/template_compiler.js', [], {silent: true} );

    jvm.stdout.pipe(targetStream);

    jvm.stderr.pipe(process.stderr);

    jvm.send(`{
      "templateURL" : "${process.cwd()}/ooo_templates/${template}",
      "styles" : ${JSON.stringify(styles)},
      "data": ${data}
    }`);



    jvm.on('message', function (msg) {
      if (msg === 'ok') {
        console.log('Compiled Template');
        targetStream.end();
        jvm.kill();
      }
    });
  }

module.exports = {
  compileXlsxTemplate : compileTemplate
};

}());
