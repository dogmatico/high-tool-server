(function () {
  'use strict';

  const pg = require('pg'),
    topojson = require('topojson'),
    fs = require('fs');


  function AffineTransformation(min, max, lowerBound, upperBound) {
    if (Math.abs(min - max) < 1e-4) {
      this.compute = function (value) {
        return value;
      }
    } else {
      this.compute = function (value) {
        return lowerBound + (value - min)*(upperBound - lowerBound)/(max - min)
      }
    }
  }

  function getFirstValueUpward(id, trie) {
    var originalId = id,
      value;

    while (id.length && !trie.get(id)) {
      id = id.slice(0, -1);
    }

    if (id.length === 0) {
      value = trie.get('EU28');
    } else {
      value = trie.get(id);
    }

    if (id !== originalId) {
      trie.add(originalId, value);
    }
    return value;
  }

  function GISService(pgCredentials, schema) {
    this._pgCredentials = pgCredentials;
    this._schema = schema;

    this.nutsCodes = new Promise((resolve, reject) => {
      const sql = `SELECT name AS code, remark AS value FROM ${schema}._country WHERE name IS NOT NULL
        UNION DISTINCT
        SELECT nuts2_id AS code, name AS value FROM ${schema}._ht2006_level_2 WHERE nuts2_id IS NOT NULL;`;
      pg.connect(this._pgCredentials, (err, client, done) => {
        if (err) {
          reject(err)
          throw 'GISService: Cannot connect to the Data Stock';
        }
        client.query(sql, (err, result) => {
          done();
          if (err) {
            reject(err)
            throw 'GISService: Cannot get NUTS codes from the Data Stock';
          }
          resolve(result.rows.reduce((dict, row) => {
            dict[row.code] = row.value;
            return dict;
          }, {}));
        });
      });
    });
  }

  /**
   * Validate if an array of columns exists within a table.
   * @param  {string}   table
   * @param  {string[]} columns
   * @param  {Function} cb      Callback(error, validated columns : string[])
   * @return {void}
   */
  GISService.prototype._getTableColumns = function (table, cb) {
    pg.connect(this._pgCredentials, (err, client, done) => {
      if (err) {
        cb(err, null);
      } else {
        const queryConfig = {
          text: ` SELECT column_name from information_schema.columns where
                  table_name = $1 AND table_schema = $2;`,
          values: [
            table,
            this._schema
          ]
        };

        client.query(queryConfig, (err, res) => {
          done();
          if (err) {
            done();
            return cb(err, null);
          }

          if (res.rows.length === 0) {
            return cb(new Error('The schema/table pair to validate returned no columns.'), null);
          }
          const validColumns = res.rows.reduce((acum, column) => {
            acum[column.column_name] = true;
            return acum;
          }, {});

          cb(null, validColumns);
        });
      }
    });
  };

  /**
   * Validated a set of where conditions against set of columns, comparators and
   * types
   * @param  {Object} validColumns keys are the valid columns
   * @param  {Object[]} whereArray   Array to check
   * @return {Object[]}              Filtered whereArray
   */
  GISService.prototype._validateWhere = function (validColumns, whereArray) {
    const validComparators = [
      '<>',
      '<',
      '<=',
      '>=',
      '>',
      '='
    ];

    if (!Array.isArray(whereArray)) {
      return [];
    }

    return whereArray.filter(condition => {
      return (validComparators.indexOf(condition.comparator) !== -1 &&
        (condition.type === 'OR' || condition.type === 'AND') &&
        validColumns[condition.column_name]);
    });
  };

  GISService.prototype._formatTransformation = function (format, idProperty, collectionName, geojson) {
    switch (format) {
      case 'geojson':
        return geojson;
        break;
      case 'topojson':
        const topoJsonOptions = {
          'property-transform' : function (feature) {
            delete feature.properties[idProperty];
            return feature.properties;
          },
          id : function (d) {
            return d.properties[idProperty];
          }
        };
        const transObj = {};
        transObj[collectionName] = geojson;
        return topojson.topology(transObj, topoJsonOptions);
        break;
      default:
        return null;
    }
  };

  GISService.prototype.getNUTS2 = function (features, where, format, cb) {
    const table = '_ht2006_level_2';

    format = format || 'geojson';

    if (!Array.isArray(features)) {
      features = [];
    }
    if (features.indexOf('nuts2_id') === -1) {
      fetures.push('nuts2_id');
    }
    if (!Array.isArray(where)) {
      where = [];
    }

    this._getTableColumns(table, (err, columns) => {
      const validFeatures = features.filter(feature => {
        return columns[feature];
      });

      if (validFeatures.length === 0) {
        return cb(new Error('Unable to build the GeoJSON query: No valid features where provided.'), null);
      }

      const validatedWhere = this._validateWhere(columns, where);

      const whereSegment = (validatedWhere.length === 0 ? '' :
        validatedWhere.reduce((acum, condition, index) => {
          if (index !== 0) {
            acum += condition.type + ' ';
          }
          acum += `${condition.column_name} ${condition.comparator} $${index + 1} `;
          return acum;
        }, 'WHERE '));

        const sqlConfig = {
          text : `SELECT row_to_json(fc)
           FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
           FROM (SELECT 'Feature' As type,
           CASE
            WHEN (ST_IsSimple(lg.simplifedGeometry)) THEN
              ST_AsGeoJSON(ST_ForceRHR(lg.simplifedGeometry))::json
            ELSE
              ST_AsGeoJSON(ST_ForceRHR(lg.shape::geometry))::json
           END
           AS geometry,
           row_to_json((SELECT l FROM (SELECT ${validFeatures.join(', ')}) AS l
          )) As properties
          FROM (
            SELECT ST_Simplify(${this._schema}.${table}.shape::geometry, 0.05)::geometry AS simplifedGeometry, *
            FROM ${this._schema}.${table}
          ) As lg ${whereSegment}) As f )  As fc;`,
          values : where.reduce((acum, condition) => {
            acum.push(condition.value);
            return acum;
          }, [])
        };


        pg.connect(this._pgCredentials, (err, client, done) => {
          if (err) {
            return cb(err, null);
          }
          client.query(sqlConfig, (err, res) => {
            done();
            if (err) {
              return cb(err, null);
            }
            const transformedMap = this._formatTransformation(format, 'nuts2_id', 'nuts2', res.rows[0].row_to_json);
            if (!transformedMap) {
              return cb(new Error('Unkown format'), null);
            }
            return cb(null, transformedMap);
          });
        });
    });
  };

  GISService.prototype.getNUTS0 = function (features, where, format, cb) {
    const table = '_country';

    format = format || 'geojson';

    if (!Array.isArray(features)) {
      features = [];
    }

    if (!Array.isArray(where)) {
      where = [];
    }

    this._getTableColumns(table, (err, columns) => {
      if (err) {
        return cb(err, null);
      }

      const validFeatures = features.filter(feature => {
        return columns[feature];
      });

      if (validFeatures.length === 0) {
        return cb(new Error('Unable to build the GeoJSON query: No valid features where provided.'), null);
      }

      const validatedWhere = this._validateWhere(columns, where);

      const whereSegment = (validatedWhere.length === 0 ? '' :
        validatedWhere.reduce((acum, condition, index) => {
          if (index !== 0) {
            acum += condition.type + ' ';
          }
          acum += `${table}.${condition.column_name}${condition.cast ? `::${condition.cast}` : ''} ${condition.comparator} $${index + 1} `;
          return acum;
        }, 'WHERE '));

        const sqlConfig = {
          text : `SELECT row_to_json(fc)
           FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
           FROM (SELECT 'Feature' As type,
           CASE
            WHEN (ST_IsSimple(lg.simplifedGeometry)) THEN
              ST_AsGeoJSON(ST_ForceRHR(lg.simplifedGeometry))::json
            ELSE
              ST_AsGeoJSON(ST_ForceRHR(lg.geometry))::json
           END
           AS geometry,
           row_to_json((SELECT l FROM (SELECT id, name, description) AS l
          )) As properties
          FROM (
              SELECT upper(${this._schema}._country.name) AS id,
              ${this._schema}._country.remark AS name,
              ${this._schema}._country.description AS description,
              ${this._schema}._country.shape::geometry as geometry,
              ST_Simplify(${this._schema}._country.shape::geometry, 0.04)::geometry as simplifedGeometry
              FROM ${this._schema}._country
              INNER JOIN (
                SELECT DISTINCT ${this._schema}._ht2006_level_2.country_id AS id
                FROM ${this._schema}._ht2006_level_2
                WHERE ${this._schema}._ht2006_level_2.ht2006_cont_id = '1'
              ) AS EU28NOCH ON (EU28NOCH.id = ${this._schema}._country.id)
              ${whereSegment}
          ) As lg) As f) As fc;`,
          values : where.reduce((acum, condition) => {
            acum.push(condition.value);
            return acum;
          }, [])
        };

        pg.connect(this._pgCredentials, (err, client, done) => {
          if (err) {
            return cb(err, null);
          }
          client.query(sqlConfig, (err, res) => {
            done();
            if (err) {
              return cb(err, null);
            }
            const transformedMap = this._formatTransformation(format, 'id', 'nuts0', res.rows[0].row_to_json);
            if (!transformedMap) {
              return cb(new Error('Unkown format'), null);
            }
            return cb(null, transformedMap);
          });
        });
    });
  };

  GISService.prototype.getHyperNetwork = function (schema, where, cb) {
    if (!Array.isArray(where)) {
      where = [];
    }

    const retObj = {
      graph : {
        type : "HyperNetwork",
        label : "HyperNetwork",
        directed : true
      }
    };

    this._getTableColumns('i_pd_core_hyper_net_link', (err, columns) => {
      const validatedWhere = this._validateWhere(columns, where);
      const whereSegment = (validatedWhere.length === 0 ? '' :
        validatedWhere.reduce((acum, condition, index) => {
          if (index !== 0) {
            acum += condition.type + ' ';
          }
          acum += `${condition.column_name} ${condition.comparator} $${index + 1} `;
          return acum;
        }, 'WHERE '));

      pg.connect(this._pgCredentials, (err, client, done) => {
        const sqlGetNodes = {
          text : `SELECT i_pd_core_hyper_net_node.year_id AS year,
                  i_pd_core_hyper_net_node.region_id AS id,
                  i_pd_core_hyper_centroid.i_pd_capital AS label,
                  i_pd_core_hyper_centroid.i_pd_capital_lat AS lat,
                  i_pd_core_hyper_centroid.i_pd_capital_long AS long,
                  i_pd_core_hyper_net_node.mode_id AS mode_id,
                  i_pd_ae_time,
                  i_pd_ae_dist,
                  i_pd_ae_time_weight,
                  i_pd_ae_dist_weight
                  FROM ${schema}.i_pd_core_hyper_net_node
                  INNER JOIN ${schema}.i_pd_core_hyper_centroid
                    ON (${schema}.i_pd_core_hyper_centroid.region_id = ${schema}.i_pd_core_hyper_net_node.region_id)
                  ${(validatedWhere.length === 0 ?
                    '' :
                    validatedWhere.reduce((acum, condition, index) => {
                      if (index !== 0) {
                        acum += condition.type + ' ';
                      }
                      acum += `i_pd_core_hyper_net_node.${condition.column_name} ${condition.comparator} $${index + 1} `;
                      return acum;
                    }, 'WHERE ')
                  )}
                  ORDER BY id, year;`,
          values : validatedWhere.reduce((acum, condition) => {
            acum.push(condition.value);
            return acum;
          }, [])
        };
        client.query(sqlGetNodes, (err, res) => {
            if (err) {
              done();
              cb(err, null);
            } else {
              retObj.graph.nodes = res.rows.reduce((acum, row, index) => {
                if (index === 0 || acum[acum.length - 1].id !== row.id) {
                  acum.push({
                    id : row.id,
                    label : row.label,
                    metadata : {
                      geo : {
                        lat : row.lat,
                        long : row.long
                      },
                      years : {}
                    }
                  });
                }

                acum[acum.length - 1].metadata.years[row.year] = {};
                Object.keys(row)
                  .filter(key => {
                    return (key !== 'id' && key !== 'label' && key !== 'year' && key !== 'lat' && key !== 'long');
                  })
                  .forEach(key => {
                    acum[acum.length - 1].metadata.years[row.year][key] = row[key];
                  });


                return acum;
              }, []);

              const sqlConfig = {
                text : `SELECT *
                  FROM ${schema}.i_pd_core_hyper_net_link
                  ${whereSegment}
                  ORDER BY region_o_id, region_d_id, year_id;`,
                values : validatedWhere.reduce((acum, condition) => {
                  acum.push(condition.value);
                  return acum;
                }, [])
              };

              client.query(sqlConfig, (err, resEdges) => {
                done();
                if (err) {
                  cb(err, null);
                } else {
                  retObj.graph.edges = resEdges.rows.reduce((acum, edge, index) => {
                    if (index === 0 || acum[acum.length - 1].source !== edge.region_o_id ||
                      acum[acum.length - 1].target !== edge.region_d_id) {
                        acum.push({
                          source : edge.region_o_id,
                          target : edge.region_d_id,
                          directed : true,
                          metadata : {}
                        });
                    }

                    acum[acum.length - 1].metadata[edge.year_id] = {};

                    Object.keys(edge).filter(key => {
                      return (key !== 'region_o_id' && key != 'region_d_id' && key != 'year_id');
                    })
                    .forEach(key => {
                      acum[acum.length - 1].metadata[edge.year_id][key] = edge[key];
                    });

                    return acum;
                  }, []);
                  cb(null, retObj);
                }
              });
            }
          });
      });
    });
  };

  GISService.prototype.updateHypernetworkEdges = function (schema, modeId, edges, cb) {
    pg.connect(this._pgCredentials, (err, client, done) => {
      if (err) {
        cb(err, null);
        return;
      }

      // Editable columns
      const metadataKeys = ['i_pd_link_time_weight', 'i_pd_link_dist_weight'];

      const edgesTable = edges.reduce((acum, edge) => {
        Object.keys(edge.metadata).forEach(year => {
          const rowData = [
            year,
            modeId,
            edge.source,
            edge.target
          ];

          metadataKeys.forEach(column => {
            rowData.push(edge.metadata[year][column]);
          });
          acum.push(rowData);
        });
        return acum;
      }, []);

      client.query('BEGIN TRANSACTION', err => {
        if (err) {
          done();
          return cb(err, null);
        }
        const sqlConfig = {
          text : `UPDATE ${schema}.i_pd_core_hyper_net_link
            SET ${metadataKeys.reduce((acum, key, index) => {
              acum.push(`${key} = $${5 + index}`);
              return acum;
            }, []).join(', ')}
            WHERE year_id = $1 AND mode_id = $2 AND region_o_id = $3 AND region_d_id = $4;`,
          values : edgesTable[0],
          name : 'update_hypernetwork_edges'
        };

        client.query(sqlConfig, (err) => {
          if (err) {
            client.query('ROLLBACK', () => {
              done();
              cb(err, null);
            });
          } else {
            function* SQLStatements(edgesTable)  {

                for (let i = 1, ln = edgesTable.length; i < ln; i+= 1) {
                  try {
                    yield new Promise((resolve, reject) => {
                      client.query({
                        name : 'update_hypernetwork_edges',
                        values : edgesTable[i]
                        }, err => {
                          if (err) {
                            reject(err);
                          } else {
                            resolve();
                          }
                        });
                      });
                  } catch (e) {
                    console.log(e);
                    client.query('ROLLBACK', () => {
                      done();
                    });
                  }
                }

              return;
            }
            const gen = SQLStatements(edgesTable);
            const recursive = function () {
              let ret = gen.next();
              if (ret.done) {
                client.query('COMMIT', (err) => {
                  done();
                  if (err) {
                    cb(err, null);
                  } else {
                    cb(null, true);
                  }
                });
              } else {
                ret.value.then(() => {
                  recursive();
                })
                .catch(err => {
                  gen.throw(err);
                  cb(err, null);
                });
              };
            }
            recursive();
          }
        });
      });
    });
  };

  GISService.prototype.updateHypernetworkNodes = function (schema, modeId, nodes, cb) {
    pg.connect(this._pgCredentials, (err, client, done) => {
      if (err) {
        cb(err, null);
        return;
      }

      // Editable columns
      const metadataKeys = ['i_pd_ae_time_weight', 'i_pd_ae_dist_weight'];

      const nodesTable = nodes.reduce((acum, node) => {
        Object.keys(node.metadata.years).forEach(year => {
          const rowData = [
            year,
            modeId,
            node.id
          ];

          metadataKeys.forEach(column => {
            rowData.push(node.metadata.years[year][column]);
          });
          acum.push(rowData);
        });
        return acum;
      }, []);

      client.query('BEGIN TRANSACTION', err => {
        if (err) {
          done();
          return cb(err, null);
        }
        const sqlConfig = {
          text : `UPDATE ${schema}.i_pd_core_hyper_net_node
            SET ${metadataKeys.reduce((acum, key, index) => {
              acum.push(`${key} = $${4 + index}`);
              return acum;
            }, []).join(', ')}
            WHERE year_id = $1 AND mode_id = $2 AND region_id = $3;`,
          values : nodesTable[0],
          name : 'update_hypernetwork_nodes'
        };

        client.query(sqlConfig, (err) => {
          if (err) {
            client.query('ROLLBACK', () => {
              done();
              cb(err, null);
            });
          } else {
            function* SQLStatements(nodesTable)  {

                for (let i = 1, ln = nodesTable.length; i < ln; i+= 1) {
                  try {
                    yield new Promise((resolve, reject) => {
                      client.query({
                        name : 'update_hypernetwork_nodes',
                        values : nodesTable[i]
                        }, err => {
                          if (err) {
                            reject(err);
                          } else {
                            resolve();
                          }
                        });
                      });
                  } catch (e) {
                    console.log(e);
                    client.query('ROLLBACK', () => {
                      done();
                    });
                  }
                }

              return;
            }
            const gen = SQLStatements(nodesTable);
            const recursive = function () {
              let ret = gen.next();
              if (ret.done) {
                client.query('COMMIT', (err) => {
                  done();
                  if (err) {
                    cb(err, null);
                  } else {
                    cb(null, true);
                  }
                });
              } else {
                ret.value.then(() => {
                  recursive();
                })
                .catch(err => {
                  gen.throw(err);
                  cb(err, null);
                });
              };
            }
            recursive();
          }
        });
      });
    });
  };

  /**
   * Return a map with data and a legend in the requested format
   * @param  {Object}   data   Data of lever to in the package or run. Must include {
   *  min : Number, max: Number, startYear: Number ,representationaData :{
   *  timeSeries: {}}, terminalValue: Number, spread... See PackageService
   * @param  {String}   format svg only
   * @param  {String}   zoom   nuts0 or nuts2
   * @param  {Function} cb     Callback(err, mapFile)
   * @return {undefined}
   */
  GISService.prototype.generateMAP = function (data, format, zoom, cb) {
    format = format || 'svg';
    zoom = zoom || 'nuts0';

    switch (zoom) {
      case 'nuts0':
      case 'nuts2':
        break;
      default:
        return cb(new Error('Invalid zoom for requested map. Valid options: \'nuts0\' and \'nuts2\'.'), null);
    }

    const d3 = require('d3');
    require(process.cwd() + '/modifiedVendorLibraries/d3-composite-projections/conicConformalEurope-proj.js');
    const jsdom = require('jsdom');

    // General Defines
    const width = 520;
    const height = 500;
    const factor = (data.target && data.target.__is_relative === false ? 1 : 100);

    const color = d3.scale.linear()
      .domain([data.min, data.max])
      .range(['#e6e6ff', '#000080']);

    // Legend
    const numSquaresLegend = 8,
      widthLegend = 480,
      sideSquare = (widthLegend / numSquaresLegend) * 0.5,
      truncatedVal = (data.max - data.min) / numSquaresLegend;

    // Create document;
    jsdom.env({
	    html:'',
	    features: {
        QuerySelector : true,
        createElementNS : true,
      }, //you need query selector for D3 to work
	    done: (err, window) => {
        if (err) {
          return cb(err, null);
        }

        window.d3 = d3.select(window.document);

        const document = window.document;

        const svg = window.d3.select('body')
	    		.append('div').attr('class','container') //make a container div to ease the saving process
	    		.append('svg')
	    			.attr({
			      		xmlns : 'http://www.w3.org/2000/svg',
			      		width : Math.max(width, widthLegend) / 1.2,
			      		height : (height + 150) / 1.2,
                viewBox : `0 0 ${Math.max(width, widthLegend)} ${height + 150}`
			      	});


        const legend = svg.append('g')
          .attr('class', 'legend')
          .attr('transform', `translate(${Math.max(0, width / 2 - widthLegend / 4)}, ${height})`);

        for (let i = 0; i <= numSquaresLegend;i +=1 ) {
          let squareLegendG = legend.append('g')
            .attr('transform', 'translate(' + (i * sideSquare) + ', 0)')
            .attr('overflow', 'visible');

          squareLegendG
            .append('rect')
              .attr('width', sideSquare)
              .attr('height', sideSquare / 2)
              .attr('fill', color(data.min + i * truncatedVal));

          if (i % 2 === 0) {
            squareLegendG
              .append('text')
                .text(Math.round(1000 * factor * (data.min + i * truncatedVal)) / 1000)
                .attr('x', sideSquare / 2)
                .attr('y', sideSquare / 2 + 15);
          }
        }

        fs.readFile('ooo_templates/maps.css', (err, mapCSS) => {
          if (err) {
            return cb(err, null);
          }

          svg.append('defs')
            .append('style')
              .attr('type', 'text/css')
              .text(`<![CDATA[
                ${mapCSS}
              ]]>`);

        const requestedMaps = (zoom === 'nuts2' ? ['nuts0', 'nuts2'] : ['nuts0'])
          .map(item => {
            return new Promise((resolve, reject) => {
              if (item === 'nuts2') {
                this.getNUTS2(['nuts2_id', 'name'], [
                  {type : 'AND', comparator: '=', column_name : 'ht2006_cont_id', value : 1}
                ], 'topojson', (err, map) => {
                    if (err) {
                      reject(err);
                    } else {
                      resolve(map);
                    }
                  });
              } else {
                this.getNUTS0(['id'],[
                  {type : 'AND', comparator: '<', column_name : 'id', value : 1000, cast : 'int'}
                ], 'topojson', (err, map) => {
                  if (err) {
                   reject(err);
                 } else {
                   resolve(map);
                 }
               });
             };
          });
        });

        Promise.all(requestedMaps)
          .then(maps => {

            const transformation = (data.values.transformation ? new AffineTransformation(
              data.values.transformation[0],
              data.values.transformation[1],
              data.values.transformation[2],
              data.values.transformation[3]
            ) : {
              compute : (value) => value
            });

            const statData = (data.values.representationaData.spread && data.values.representationaData.spread.id && data.values.representationaData.spread.id !== 'baseline' ?
              StatisticalData.findOne({id : data.values.representationaData.spread.id}) :
              Promise.resolve({values : [{
                'code' : 'EU28',
                'value': data.values.terminalValue
              }]})
            );

            const modifiedNodesTrie = (data.values.representationaData.spread && data.values.representationaData.spread.modifiedNodes ?
            data.values.representationaData.spread.modifiedNodes : [])
              .reduce((trie, region) => {
                trie.set(region.key, region.value);
                return trie;
              }, new sails.Trie())

            statData.then(stats => {

              var baseDataTrie = stats.values.reduce((trie, region) => {
                trie.set(region.code, region.value);
                return trie;
              }, new sails.Trie());

              const projection = d3.geo.conicConformalEurope();
              const path = d3.geo.path().projection(projection);

              const g = svg.append('g')
              g.selectAll('.region')
                  .data(topojson.feature((zoom === 'nuts2' ? maps[1] : maps[0]),
                  maps[(zoom === 'nuts2' ? 1 : 0)].objects[zoom]).features)
                .enter()
                  .append('path')
                  .attr('class', 'region')
                  .attr('d', path)
                  .attr('id', d => d.id)
                  .style('stroke', '#fff')
                  .style('stroke-width', 0)
                  .style('opacity', 1)
                  .style('fill', d => {
                    const specificValue = getFirstValueUpward(d.id, modifiedNodesTrie);
                    const regionValue = parseFloat((specificValue ? specificValue : transformation.compute(getFirstValueUpward(d.id, baseDataTrie))), 10);
                    return (color(regionValue));
                  });

              // Draw borders
              // National
              g.append('path')
                .datum(topojson.mesh(maps[0], maps[0].objects.nuts0))
                  .attr('class', 'svg-nuts0-borders')
                  .attr('d', path);

              // Regional
              if (zoom === 'nuts2') {
                g.append('path')
                  .datum(topojson.mesh(maps[1], maps[1].objects.nuts2), (a, b) => {
                    return a !== b;
                  })
                    .attr('class', 'svg-nuts2-borders')
                    .attr('d', path);
              }

              // Projection borders
              g.append("path")
                .classed('projection-composition-borders', true)
                .attr("d", projection.getCompositionBorders());

              cb(null, window.d3.select('.container').html());
            })
            .catch(err => Promise.reject(err));
          })
          .catch(err => cb(err, null));
        });
      }
    });
  }

  module.exports = new GISService(sails.config.connections.HTDataStock, sails.config.HTConfig.baseSchema);

}());
