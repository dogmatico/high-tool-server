(function () {
  'use strict';

  class EuroStatService {
    constructor(httpProvider, jsonstat, baseURL) {
      this._httpProvider = httpProvider;
      this._baseURL = baseURL;
      this._jsonstat = jsonstat;
    }

    downloadTables(grupMetadata, tablesArray, options) {
      if (!Array.isArray(tablesArray)) {
        tablesArray = [tablesArray];
      }

      tablesArray = tablesArray.map(item => {
        if (typeof item === 'string') {
          item = {
            table : item
          };
        }
        item.time = item.time || 2010;
        return item;
      })

      options = options || {};
      options.precision = options.precision || 1e4;
      options.centralizerKey = options.centralizerKey || 'EU28';
      options.centralize = !!options.centralize;
      options.Dataset = options.Dataset || 0;

      const httpRequests = tablesArray.reduce((acum, table) => {
        acum.push(new Promise((resolve, reject) => {
          const tableName = table.table;
          delete table.table;

          const querySegment = Object.keys(table)
            .map(key => `${key}=${table[key]}`)
            .join('&');

          this._httpProvider.get({
            host : 'ec.europa.eu',
            path : `/eurostat/wdds/rest/data/v2.1/json/en/${tableName}?${querySegment}`,
            url :`${this._baseURL}`,
            headers : {
              'User-Agent' : '11; Ubuntu; Linux x86_64; rv:46.0) Gecko/20100101 Firefox/46.0'
            }}, res => {
            let body = '';

            res
              .on('data', chunk => {
                body += chunk
              })
              .on('end', () => {
                try {
                  const jsonObj = JSON.parse(body);
                  resolve(jsonObj);
                } catch (e) {
                  console.log(e.stack);
                  reject(e);
                }
              })
              .on('error', err => reject(err));
          });
        }));
        return acum;
      }, []);

      return new Promise((resolve, reject) => {
        Promise.all(httpRequests)
          .then(dataSets => {
            const data = dataSets
              .map(data => this._jsonstat(data).toTable({
                type : 'arrobj',
                content : 'id'
              }))
              .reduce((acum, dataSet) => {
                return acum.concat(dataSet);
              }, [])
              .reduce((acum, region) => {
                acum.push({
                  code : region.geo,
                  value : region.value
                })
                return acum;
              }, [])
              .filter(item => Math.abs(item.value > 1e-4));

            let centralizerValue = null;
            if (options.centralize) {
              for (let i = 0, ln = data.length, found = false; i < ln && centralizerValue === null; i += 1) {
                if (data[i].code === options.centralizerKey) {
                  centralizerValue = data[i].value;
                }
              }

              if (centralizerValue === null && options.centralize) {
                const error = new Error('Missing data for centralizer region ' + options.centralizerKey);
                reject(error);
                throw error;
              }
            } else {
              centralizerValue = 1;
            }

            resolve([{
              id : grupMetadata.id,
              description : grupMetadata.description,
              data : data
                .map(item => {
                  return {
                    code : item.code,
                    value : (options.centralize ?
                      Math.round(100 * options.precision *  item.value / centralizerValue) / options.precision :
                      item.value
                    )
                  };
                })
            }, {
              id : grupMetadata.id + '_inv',
              description : 'Inverse of ' + grupMetadata.description,
              data : data
                .map(item => {
                  return {
                    code : item.code,
                    value : (options.centralize ?
                      Math.round((options.centralize ? 100 : 1) * options.precision * (options.centralize ? centralizerValue : 100) / item.value) / options.precision :
                      Math.round(1000000 / item.value) / 100
                    )
                  };
                })
            }]);
          })
          .catch(reject);
      });
    }
  }

  module.exports = new EuroStatService(
    require('https'),
    require('jsonstat'),
    'https://ec.europa.eu/eurostat/wdds/rest/data/v2.1/json/en'
  );
}());
