/*jslint node: true, indent: 2, esnext : true */
(function () {
  /**
   * Collection of functions to help with the setup of the backend.
   */
  'use strict';
  var fs = require('fs'),
    pg = require('pg');

  function getModule(variableName) {
    const regExModule = new RegExp(/(i|o|p){1}_(\w{2})/),
     module2Id = variableName.match(regExModule);

    var module3Id;
    if (module2Id && module2Id[2]) {
      switch (module2Id[2]) {
       case 'de':
         module3Id = 'DEM';
         break;
       case 'er':
         module3Id = 'ECR';
         break;
       case 'ev':
         module3Id = 'ENV';
         break;
       case 'fd':
         module3Id = 'FRD';
         break;
       case 'pd':
         module3Id = 'PAD';
         break;
       case 'sa':
         module3Id = 'SAF';
         break;
       case 'vs':
         module3Id = 'VES';
         break;
       default:
         module3Id = 'OTHER';
      }
    } else {
      module3Id = 'OTHER';
    }
    return module3Id;
  }

  function spatialAndTemporalDimensions(schemaName, cb) {
    pg.connect(sails.config.connections.HTDataStock, (err, client, done) => {
      if (err) {
        done();
        cb(err, null)
      } else {
        client.query(`SELECT * FROM ${schemaName}.__parameters`, (err, result) => {
          if (err) {
            done();
            cb(err, null)
          } else {
            const dictionary = result.rows.reduce((acum, row) => {
              acum[row.id] = {
                spatial : null,
                temporal : null
              };

              for (let i = 1, ln = row.nr_of_dimensions; i <= ln; i += 1 ) {
                const dimName = row[`dimension_${i}_id`];
                if (
                  dimName === 'ht2006_level_2' ||
                  dimName === 'region_eu' ||
                  dimName === 'region' ||
                  dimName === 'country'
                ) {
                  acum[row.id].spatial = dimName;
                }

                if (
                  dimName === 'time' ||
                  dimName === 'year'
                ) {
                  acum[row.id].temporal = dimName;
                }
              }
              return acum;
            }, {});

            cb(null, dictionary);
          }
        });
      }
    });
  }

  function generateSchemaFromDS(schemaName, cb) {
    pg.connect(sails.config.connections.HTDataStock, function (err, client, done) {
      if (err) {
        done();
        return cb(err, null);
      }

      const schema = {
        adjList : [[]],
        nodeAttributes : [{
          label : 'TPMROOT',
          shortLabel : 'TPMROOT',
          description : 'TPMROOT',
          uuid : 'TPMROOT'
        }],
        rootNodes : [0]
      };
      const uuidTrie = new sails.Trie();

      uuidTrie.set('TPMROOT', 0);

      client.query(`SELECT id, name FROM ${schemaName}.__tpm_categories
                    ORDER BY id;`, (err, categories) => {
        if (err) {
          done();
          return cb(err, null);
        }

        categories.rows.forEach(category => {
          schema.nodeAttributes.push({
            label : category.name,
            shortLabel : category.name,
            description : category.name,
            uuid : `TPM_CAT_${category.id}`,
            parent : 'TPMROOT'
          });
          schema.adjList[0].push(schema.nodeAttributes.length - 1);
          schema.adjList.push([]);
          uuidTrie.set(`TPM_CAT_${category.id}`, schema.nodeAttributes.length - 1);
        });

        client.query(`SELECT id, name, short_name, description, target, white_paper, category_id
                      FROM  ${schemaName}.__tpm
                      ORDER BY id;`, (err, TPMs) => {
          if (err) {
            done();
            return cb(err, null);
          }

          const offSet = schema.nodeAttributes.length;
          TPMs.rows.forEach((TPM, index) => {
            schema.nodeAttributes.push({
              label : TPM.name,
              shortLabel : TPM.short_name,
              description : TPM.description,
              uuid : `TPM_TPM_${TPM.id}`,
              white_paper : TPM.white_paper,
              target : TPM.target,
              parent : `TPM_CAT_${TPM.category_id}`
            });
            schema.adjList.push([]);
            schema.adjList[uuidTrie.get(`TPM_CAT_${TPM.category_id}`)].push(offSet + index);
            uuidTrie.set(`TPM_TPM_${TPM.id}`, offSet + index);
          });

          const SQLQuery = `SELECT ${schemaName}.__tpm_lever_value.tpm_id,
                            ${schemaName}.__tpm_lever_value.parameter_full_id,
                            ${schemaName}.__tpm_lever_value.default_value,
                            ${schemaName}.__tpm_lever_value.lower_bound,
                            ${schemaName}.__tpm_lever_value.upper_bound,
                            ${schemaName}.__tpm_lever_value.initial_value,
                            ${schemaName}.__policy_lever.name,
                            ${schemaName}.__policy_lever.id AS leverId,
                            ${schemaName}.__policy_lever.description,
                            ${schemaName}.__policy_lever.remark,
                            ${schemaName}.__policy_lever.parameter_id,
                            ${schemaName}.__policy_lever.dimension_1_id,
                            ${schemaName}.__policy_lever.dimension_1_element_id,
                            ${schemaName}.__policy_lever.dimension_2_id,
                            ${schemaName}.__policy_lever.dimension_2_element_id,
                            ${schemaName}.__policy_lever.dimension_3_id,
                            ${schemaName}.__policy_lever.dimension_3_element_id,
                            ${schemaName}.__policy_lever.dimension_4_id,
                            ${schemaName}.__policy_lever.dimension_4_element_id,
                            ${schemaName}.__policy_lever.dimension_5_id,
                            ${schemaName}.__policy_lever.dimension_5_element_id,
                            ${schemaName}.__policy_lever.dimension_6_id,
                            ${schemaName}.__policy_lever.dimension_6_element_id,
                            ${schemaName}.__policy_lever.dimension_7_id,
                            ${schemaName}.__policy_lever.dimension_7_element_id,
                            ${schemaName}.__policy_lever.dimension_8_id,
                            ${schemaName}.__policy_lever.dimension_8_element_id,
                            ${schemaName}.__policy_lever.is_relative,
                            ${schemaName}.__policy_lever.is_geo_active_n0,
                            ${schemaName}.__policy_lever.is_geo_active_n2,
                            ${schemaName}.__policy_lever.unit_id
                            FROM ${schemaName}.__tpm_lever_value
                            INNER JOIN ${schemaName}.__policy_lever ON (${schemaName}.__tpm_lever_value.parameter_full_id = ${schemaName}.__policy_lever.id)`;

          client.query(SQLQuery, (err, levers) => {
            done();
            if (err) {
              return cb(err, null);
            }
            const offSet = schema.nodeAttributes.length;
            let orphanOffset = 0;

            levers.rows.forEach((lever, index) => {
              const parentIndex = uuidTrie.get(`TPM_TPM_${lever.tpm_id}`);

              if (parentIndex === false) {
                orphanOffset += 1;
              } else {
                if (lever.min > lever.max) {
                  let tempVal = lever.min;
                  lever.min = lever.max;
                  lever.max = tempVal;
                }

                const leverIsRelative = (lever.is_relative == 1);
                schema.nodeAttributes.push({
                  label : lever.name,
                  shortLabel : lever.name,
                  description : lever.description,
                  leverId : lever.parameter_full_id,
                  remark : lever.remark,
                  uuid : `TPM_TPM_${lever.tpm_id}_${lever.parameter_full_id}`,
                  parent : `TPM_TPM_${lever.tpm_id}`,
                  min :  (leverIsRelative ? 1 + lever.lower_bound : lever.lower_bound),
                  max : (leverIsRelative ? 1 + lever.upper_bound : lever.upper_bound),
                  baseline : (leverIsRelative ? 1 + lever.default_value : lever.default_value),
                  unit : `${lever.unit_id}` + (leverIsRelative ? ' (100 = baseline)' : ''),
                  initialValue : (leverIsRelative ? 1 + lever.initial_value : lever.initial_value),
                  is_geo_active_n0 : lever.is_geo_active_n0,
                  is_geo_active_n2 : lever.is_geo_active_n2,
                  target : {
                    __is_relative : leverIsRelative,
                    __parameters : [
                      lever.parameter_id
                    ],
                    __dimensions : {}
                  }
                });

                for (let i = 1; i <= 8; i+= 1) {
                  if (lever[`dimension_${i}_id`]) {
                    schema.nodeAttributes[offSet + index - orphanOffset].target.__dimensions[lever[`dimension_${i}_id`]] = [lever[`dimension_${i}_element_id`]];
                  }
                }
                schema.adjList.push([]);
                schema.adjList[parentIndex].push(offSet + index - orphanOffset);
                uuidTrie.set(`TPM_TPM_${lever.tpm_id}_${lever.parameter_full_id}`, offSet + index - orphanOffset);
              }
            });

            cb(null, {
              schema : schema,
              uuidTrie : uuidTrie
            });
          });
        });
      });
    });
  }


  /**
  * Adds a fake root to the schema to create custom TPM using the existing
  * codebase
  * @param {Object} The schema of this sails instace
  * @param {Trie} uuidTrie The instace tree with all the pairs uuid -> position
  * @param {cb} Callback(err, {schema, uuid})
  */
  function addCustomTPMSchema(schema, uuidTrie, cb) {
    const pg = require('pg');

    schema = schema || {};
    schema.nodeAttributes = schema.nodeAttributes || [];
    schema.adjList = schema.adjList || [];
    uuidTrie = uuidTrie || new sails.Trie();

    schema.nodeAttributes.push({
      "label": "Root custom TPMs",
      "shortLabel": "Root custom TPMs",
      "description": "description",
      "unit": "unit",
      "source": "source",
      "min": 0,
      "max": 0,
      "baseline": 0,
      "hasChildren": false,
      "target": {},
      "parent": null,
      "uuid": "CUSTOMTPMROOT"
    });
    schema.adjList.push([]);

    const CUSTOMTPMROOTIndex = schema.nodeAttributes.length - 1;


    uuidTrie.set('CUSTOMTPMROOT', CUSTOMTPMROOTIndex);

    ['DEM', 'ECR', 'VES', 'PAD', 'FRD', 'ENV', 'SAF', 'NOLEVER'].forEach((family, index) => {
      const objToPush = {
        "label": `${family} Module`,
        "shortLabel": `${family} Module`,
        "description": `TPMs related to ${family} Module`,
        "unit": "unit",
        "source": "source",
        "min": 0,
        "max": 0,
        "baseline": 0,
        "hasChildren": true,
        "target": {},
        "parent": "CUSTOMTPMROOT",
        "uuid": `CUSTOMTPM_${family}`
      };

      if (family === 'NOLEVER') {
        objToPush.label = `Policies without levers`;
        objToPush.shortLabel = `Policies without levers`;
        objToPush.description = `TPMs without associated Levers`;
      } else {
        objToPush.label = `${family} Module`;
        objToPush.shortLabel = `${family} Module`;
        objToPush.description = `TPMs related to ${family} Module`;
      }

      schema.nodeAttributes.push(objToPush);

      schema.adjList[CUSTOMTPMROOTIndex].push(index + CUSTOMTPMROOTIndex + 1);
      schema.adjList.push([]);
      uuidTrie.set(`CUSTOMTPM_${family}`, index + CUSTOMTPMROOTIndex + 1);
    });

    const offSet = schema.nodeAttributes.length;

    pg.connect(sails.config.connections.HTDataStock, function(err, client, done) {
      if (err) {
        cb(err, null);
      } else {
        const getLeversSQL = `SELECT * FROM ${sails.config.HTConfig.baseSchema}.__policy_lever
                              WHERE is_active_for_custom_tpm = '1';`;
        client.query(getLeversSQL, (err, result) => {
          if (err) {
            cb(err, null);
          } else {
            result.rows.forEach((row, index) => {
              const rowIsRelative = (row.is_relative == 1);
              const family = getModule(row.parameter_id);
              schema.nodeAttributes.push({
                'label': `${row.name}`,
                'shortLabel': `${row.name}`,
                'description': `${row.description}`,
                'leverId' : `${row.id}`,
                'unit': `${row.unit_id} ${(rowIsRelative ? '(100 = baseline)' : '')}`,
                'source': "source",
                'min': Math.round(((rowIsRelative ? 1 : 0 ) + row.lower_bound) * 10000) / 10000,
                'max': Math.round(((rowIsRelative ? 1 : 0 ) + row.upper_bound) * 10000) / 10000,
                'initialValue' : Math.round((rowIsRelative ? 1 + row.initial_value : row.initial_value) * 10000) / 10000,
                'baseline': Math.round(((rowIsRelative ? 1 : 0 ) + row.default_value) * 10000) / 10000,
                'hasChildren': false,
                'is_geo_active_n0' : row.is_geo_active_n0,
                'is_geo_active_n2' : row.is_geo_active_n2,
                'target': (function () {
                  let target = {
                    "__is_relative" : rowIsRelative,
                    "__parameters" : [row.parameter_id],
                    "__dimensions" : {}
                  };

                  for (let i = 1; i <= 8; i += 1) {
                    if(row[`dimension_${i}_id`]) {
                      target.__dimensions[row[`dimension_${i}_id`]] = [
                        row[`dimension_${i}_element_id`]
                      ];
                    }
                  }
                  return target;
                }()),
                "parent": `CUSTOMTPM_${family}`,
                "uuid": `CUSTOMTPM_${family}_${row.id}`
              });

              schema.adjList[uuidTrie.get(`CUSTOMTPM_${family}`)].push(index + offSet);
              schema.adjList.push([]);
              uuidTrie.set(`CUSTOMTPM_${family}_${row.id}`, index + offSet);
            });

            // Add TPM 86 as a Leverless TPM
            uuidTrie.set(`CUSTOMTPM_NOLEVER_TPM_86`, schema.adjList.length);
            schema.adjList[uuidTrie.get(`CUSTOMTPM_NOLEVER`)].push(schema.adjList.length);
            schema.adjList.push([]);
            const copyObj = Object.assign({}, schema.nodeAttributes[uuidTrie.get('TPM_TPM_86')]);
            copyObj.is_geo_active_n0 = 0;
            copyObj.is_geo_active_n2 = 0;
            copyObj.uuid = 'CUSTOMTPM_NOLEVER_TPM_86';
            copyObj.parent = 'CUSTOMTPM_NOLEVER';
            schema.nodeAttributes.push(copyObj);

            cb(null, {
              schema: schema,
              uuidTrie : uuidTrie
            });
          }
        });
      }
    });
  }

  function addLeverList(objToAttach, cb) {
    const SQLQuery = `SELECT * FROM ${sails.config.HTConfig.baseSchema}.__policy_lever`;

    pg.connect(sails.config.connections.HTDataStock, (err, client, done) => {
      if (err) {
        cb(err, null);
      } else {
        client.query(SQLQuery, (err, result) => {
          done();
          if (err) {
            cb(err, null);
          } else {
            objToAttach.levers = result.rows.reduce((acum, lever, index) => {
              const leverIsRelative = (lever.is_relative == 1);
              acum.leverList.push({
                'label': `${lever.name}`,
                'shortLabel': `${lever.name}`,
                'description': `${lever.description}`,
                'leverId' : `${lever.id}`,
                'unit': `${lever.unit_id} ${(leverIsRelative ? '(100 = baseline)' : '')}`,
                'source': "source",
                'min': Math.round(((leverIsRelative ? 1 : 0 ) + lever.lower_bound) * 1000) / 1000,
                'max': Math.round(((leverIsRelative ? 1 : 0 ) + lever.upper_bound) * 1000) / 1000,
                'initialValue' : Math.round((leverIsRelative ? 1 + lever.initial_value : lever.initial_value) * 1000) / 1000,
                'baseline': Math.round(((leverIsRelative ? 1 : 0 ) + lever.default_value) * 1000) / 1000,
                'hasChildren': false,
                'is_geo_active_n0' : lever.is_geo_active_n0,
                'is_geo_active_n2' : lever.is_geo_active_n2,
                'target': (function () {
                  let target = {
                    '__is_relative' : leverIsRelative,
                    '__parameters' : [lever.parameter_id],
                    '__dimensions' : {}
                  };

                  for (let i = 1; i <= 8; i += 1) {
                    if(lever[`dimension_${i}_id`]) {
                      target.__dimensions[lever[`dimension_${i}_id`]] = [
                        lever[`dimension_${i}_element_id`]
                      ];
                    }
                  }
                  return target;
                }()),
                'uuid' : lever.id
              });
              acum.leverDictionary[lever.id] = index;

              return acum;
            }, {
              leverList : [],
              leverDictionary : {}
            });
            cb(null, objToAttach);
          }
        });
      }
    });
  }


  module.exports = {
    addCustomTPMSchema : addCustomTPMSchema,
    generateSchemaFromDS : generateSchemaFromDS,
    addLeverList : addLeverList,
    spatialAndTemporalDimensions : spatialAndTemporalDimensions
  };
}());
