/*jslint node: true, indent: 2 */
(function () {
  /**
  * emailService.js
  *
  * @description :: Service to send email
  * @docs        :: http://sailsjs.org/#!documentation/services
  */
  "use strict";
  var nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport();

  module.exports = {
    sendEmail : function (destination, subject, text) {
      var mailOptions = {
        from: 'HT Server <high-tool@high-tool.eu>',
        to: destination,
        subject: subject,
        text: text
      };

      return new sails.promise(function (resolve, reject) {
        transporter.sendMail(mailOptions, function (err, info) {
          if (err) {
            reject(err);
          } else {
            resolve(info);
          }
        });
      });
    }
  };
  return;
}());
