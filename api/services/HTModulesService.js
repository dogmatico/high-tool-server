/*jslint node: true, indent: 2 */
/*globals sails, HTModelRuns, HTDataStock, HTModulesService, HTModules, emailService, StatisticalData, linearAlgebra, Symbol, User*/
(function () {
  /**
  * HTModulesService
  *
  * @module :: Service
  * @description :: Functions to call Handle Model run workflow
  *
  * @docs :: http://sailsjs.org/#!documentation/services
  */
  'use strict';

  module.exports = {
    getModuleVersions : function (sailsRoot) {
      var fs = require('fs'),
        parseActions = [];

      function parseConfigFile(filename) {
        return new Promise(function (resolve, reject) {
          var regExpParams = new RegExp(/(application|version)\s*=\s*(.*)/g),
            retObj = {},
            param;

          fs.readFile(`${sailsRoot}/java_modules/config/${filename}`, 'utf8', (err, data) => {
            if (err) {
              reject(err);
            } else {
              while ((param = regExpParams.exec(data)) !== null) {
                retObj[param[1]] = param[2]
              }
              resolve(retObj);
            }
          });
        });
      }

      return new Promise(function (resolve, reject) {
        fs.readdir(`${sailsRoot}/java_modules/config`, (err, files) => {
          if (err) {
            reject(err);
          } else {
            files
              .filter(filename => {
                return filename.search('.cfg') !== -1
              })
              .forEach(filename => {
                parseActions.push(parseConfigFile(filename));
              });

            Promise.all(parseActions)
              .then(res => resolve(res))
              .catch(err => reject(err));
          }
        });
      });
    },
    LifeCycleRun : function (runList, deleteList) {
      var run = Symbol("run"),
        del = Symbol("del"),
        activeRun = Symbol("activeRun");

      function LifeCycleRun(runList, deleteList) {
        this[run] = new Set(runList);
        this[del] = new Set(deleteList);
        this[activeRun] = false;
      }

      LifeCycleRun.prototype = Object.create({}, {
        "active" : {
          enumerable : true,
          get : function () {
            return this[activeRun];
          }
        },
        "activeRuns" : {
          enumerable : true,
          get : function () {
            return this[run];
          }
        },
        "markedDeletion" : {
          enumerable : true,
          get : function () {
            return this[del];
          }
        },
        "deleteNext" : {
          value : function () {
            const deleteIds = [...this[del].values()];

            if (deleteIds.length) {
              HTModelRuns.findOne({
                status : [1, 3, 5, 10],
                id : deleteIds
              })
              .then(runInfo => {
                return (runInfo ?
                  HTModulesService.deleteRun(runInfo) :
                  Promise.resolve(null)
                );
              })
              .then(runInfo => {
                if (runInfo) {
                  this[del].delete(runInfo.id);
                  const notificationMsg = `Data stock ${runInfo.label} (${runInfo.id}) has been deleted`;
                  // Status 6 = deleted;

                  runInfo.status = 6;
                  sails.controllers.notifications.sendMessageToOwner(runInfo.owner, 'datastock-update', {
                    message : notificationMsg,
                    runInfo : runInfo
                  });
                  sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
                    message : notificationMsg,
                    runInfo : runInfo
                  });

                  return HTModelRuns.destroy({id : runInfo.id});
                }
              })
              .catch(err => {
                console.error(err);
                throw new Error(err)
              });
            }
          }
        },
        "runNext" : {
          value : function () {
            const runIds = [...this[run].values()];
            if (runIds.length) {
              HTModelRuns.findOne({
                id : [...this[run].values()],
                paused : false
              })
              .then(runInfo => {
                if (!runInfo) {
                  this[activeRun] = false;
                  return Promise.resolve(null);
                }
                this[activeRun] = runInfo.id;
                this[run].delete(runInfo.id);
                return (runInfo.step === 'EXPERTMODE' ?
                  Promise.resolve([runInfo]) :
                  HTModelRuns.update({id : runInfo.id}, {
                    status : 2
                  })
                );
              })
              .then(runInfo => {
                return (runInfo && runInfo.length ?
                  HTModulesService.runModule(runInfo[0]) :
                  Promise.resolve(null)
                );
              })
              .then(runInfo => {
                if (runInfo) {
                  const currentStep = sails.config.HTConfig.steps[sails.config.HTConfig.getStepNumber(runInfo.step)];
                  const nextStep = sails.config.HTConfig.steps[sails.config.HTConfig.getStepNumber(runInfo.step) + 1];

                  if (currentStep.id === 'EXPERTMODE' && runInfo.expertMode && runInfo.status === 11) {
                    sails.controllers.notifications.sendMessageToOwner(runInfo.owner, 'datastock-update', {
                      message : 'Data Stock copied and ready to Expert Mode',
                      runInfo : runInfo
                    });

                    sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
                      message : 'Data Stock copied and ready to Expert Mode',
                      runInfo : runInfo
                    });
                    return Promise.resolve([runInfo]);
                  }

                  const updatedData = {
                    step : nextStep.id,
                    description : nextStep.description
                  };

                  if (nextStep.id === 'DONE') {
                    updatedData.status = 1;
                  }
                  return HTModelRuns.update({id : runInfo.id}, updatedData);
                }
                return Promise.resolve(null);
              })
              .then(runInfo => {
                if (runInfo && runInfo.length) {
                  if (runInfo[0].step === 'DONE') {
                    User.findOne({id : runInfo[0].owner})
                      .then((ownerData) => {
                        emailService.sendEmail(ownerData.email, 'Execution Done', `The execution of the model ${runInfo[0].label} (${runInfo[0].id}) has been completed`);
                      });

                    sails.controllers.notifications.sendMessageToOwner(runInfo[0].owner, 'datastock-update', {
                      message : `The execution of the model ${runInfo[0].label} (${runInfo[0].id}) has been completed`,
                      runInfo : runInfo[0]
                    });

                    sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
                      message : `The execution of the model ${runInfo[0].label} (${runInfo[0].id}) has been completed`,
                      runInfo : runInfo[0]
                    });
                    this.next();
                  } else {
                    this.run(runInfo[0].id);
                  }
                }
              })
              .catch(err => {
                if (err.type && err.type === 'java_modules') {
                  HTModelRuns.update({id : err.runInfo.id}, {
                    status : 5,
                    paused : true
                  })
                  .then(() => {
                    this[activeRun] = false;
                    this.run(err.runInfo.id);
                  });
                } else {
                  console.error(err);
                  throw new Error(err);
                }
              });
            }
          }
        },
        "run" : {
          enumerable : true,
          value : function (id) {
            this[run].add(id);
            this.runNext();
          }
        },
        "cancelDelete" : {
          enumerable : true,
          value : function (id) {
            if (this[del].has(id)) {
              HTModelRuns.update({id : id}, {
                status : 10,
                markedDelete : false,
                paused : (this[activeRun] === id ? false : true)
              })
              .then(runMetadata => {
                this[del].delete(id);
                if (runMetadata.length) {
                  this.run(runMetadata[0].id);
                }
              });
            }
          }
        },
        "delete" : {
          enumerable : true,
          value : function (id) {
            this[del].add(id);
            this.deleteNext();
          }
        },
        "next" : {
          enumerable : true,
          value : function () {
            this.deleteNext();
            this.runNext();
          }
        }
      });
      return new LifeCycleRun(runList, deleteList);
    },
    runModule: function (runInfo) {
      var stepNumber = sails.config.HTConfig.getStepNumber(runInfo.step);
      sails.log(`runInfo.Id: ${runInfo.Id}, stepNumber: ${stepNumber}`)
      if (runInfo.step === 'NEW')  {
        return HTModulesService.initSchema(runInfo);
      }

      if (runInfo.step === 'EXPERTMODE') {
        return HTModulesService.expertMode(runInfo);
      }

      if (stepNumber !== null) {
        return new Promise((resolve, reject) => {
          sails.controllers.notifications.sendMessageToOwner(runInfo.owner, 'datastock-update', {
            message : `Run ${runInfo.label} (${runInfo.id}) started ${runInfo.step}`,
            runInfo : runInfo
          });

          sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
            message : `Run ${runInfo.label} (${runInfo.id}) started ${runInfo.step}`,
            runInfo : runInfo
          });

          const jvm = require('child_process').fork('workers/java_modules.js');

          const stepData = {};
          Object.keys(sails.config.HTConfig.steps[stepNumber]).forEach(key => {
            if (Array.isArray(sails.config.HTConfig.steps[stepNumber][key])) {
              stepData[key] = sails.config.HTConfig.steps[stepNumber][key].slice(0);
            } else {
              stepData[key] = sails.config.HTConfig.steps[stepNumber][key];
            }
          });
          stepData.id = runInfo.id;

          function notifyError(err, runInfo, cb) {
            User.findOne({id : runInfo.owner})
              .then(function (ownerData) {
                emailService.sendEmail(ownerData.email, 'Execution Failed', 'The execution of the model ' + runInfo.label + ' failed. The backtrace of the error is:\n' + err);
              });


            cb(err);
          }

          jvm.on('message', function (msg) {
            const parsedMsg = JSON.parse(msg);
            jvm.kill();
            if (parsedMsg.result === 'error') {
              sails.controllers.notifications.sendMessageToOwner(runInfo.owner, 'datastock-update', {
                message : `Run ${runInfo.label} (${runInfo.id}) failed at ${runInfo.step}`,
                runInfo : runInfo
              });

              sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
                message : `Run ${runInfo.label} (${runInfo.id}) failed at ${runInfo.step}`,
                runInfo : runInfo
              });

              reject({
                type : 'java_modules',
                runInfo : runInfo,
                err : parsedMsg.err
              });
            } else {
              sails.controllers.notifications.sendMessageToOwner(runInfo.owner, 'datastock-update', {
                message : `Run ${runInfo.label} (${runInfo.id}) completed step ${runInfo.step}`,
                runInfo : runInfo
              });

              sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
                message : `Run ${runInfo.label} (${runInfo.id}) completed step ${runInfo.step}`,
                runInfo : runInfo
              });

              resolve(runInfo);
            }
          });

          jvm.on('error', function (err) {
            reject({
              type : 'java_error',
              err : err,
              runInfo : runInfo
            });
            jvm.kill();
          });

          jvm.send(JSON.stringify(stepData));
        });
      }
      return Promise.reject(new Error('Step not found'));
    },
    deleteRun : function (runInfo) {
      const schemaName = sails.config.HTConfig.schemaPrefix + runInfo.id
      return new Promise((resolve, reject) => {
        HTDataStock.query(`DROP SCHEMA IF EXISTS ${schemaName} CASCADE;`, err => {
          if (err) {
            return reject(err);
          }
          return resolve(runInfo);
        });
      });
    },
    expertMode : function (runInfo) {
      return new Promise((resolve, reject) => {
        if (runInfo.expertMode === true) {
          const updatedData = (runInfo.status === 11 ? {
            paused : false,
            status : 1
          } : {
            paused : true,
            status : 11
          });
          HTModelRuns.update({id : runInfo.id}, updatedData)
            .then(updatedRuns => {
              if (updatedRuns && updatedRuns.length) {
                resolve(updatedRuns[0]);
              } else {
                resolve(null);
              }
            })
            .catch(err => reject(err));
        } else {
          resolve(runInfo);
        }
      });
    },
    initSchema : function (newRun) {
      return new Promise((resolve, reject) => {
        sails.controllers.notifications.sendMessageToOwner(newRun.owner, 'datastock-update', {
          message : `Creating a new copy of the Data Stock using ${newRun.label} (${newRun.id})`,
          runInfo : newRun
        });

        sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
          message : `Creating a new copy of the Data Stock using ${newRun.label} (${newRun.id}).`,
          runInfo : newRun
        });

        HTModulesService.cloneSchema(sails.config.HTConfig.baseSchema, newRun)
          .then()
          .then(newRun => {
            sails.controllers.notifications.sendMessageToOwner(newRun.owner, 'datastock-update', {
              message : `Created a new copy of the Data Stock using ${newRun.label} (${newRun.id})`,
              runInfo : newRun
            });
            sails.controllers.notifications.sendMessageToAdmins('datastock-update', {
              message : `Created a new copy of the Data Stock using ${newRun.label} (${newRun.id}).`,
              runInfo : newRun
            });

            resolve(newRun);
          })
          .catch(err => reject(new Error(err)));
      });
    },
    cloneSchema: function(inputSchema, newRun) {
      const outputSchema = sails.config.HTConfig.schemaPrefix + newRun.id;
      const dsQuery = sails.promise.promisify(HTDataStock.query);

      function resetSchema() {
        return dsQuery(`DROP SCHEMA IF EXISTS ${outputSchema} CASCADE;`)
          .then(() => {
            return dsQuery(`CREATE SCHEMA ${outputSchema};`);
          });
      }

      function copyTables(newRun) {
        const pg = require('pg');
        const QueryStream = require('pg-query-stream');
        const {Transform} = require('stream');
        const {HTDataStock} = sails.config.connections;
        const {schemaPrefix} = sails.config.HTConfig;
        const {RowModification} = require('high-tool-ds-updater');

        const outputSchema = schemaPrefix + newRun.id;

        class ToCSVStream extends Transform {
          constructor(options) {
            super(options);

            Object.defineProperties(this, {
              '__firstRow' : {
                enumerable : false,
                writable : true,
                value : null
              },
              '_transform' : {
                enumerable : false,
                value : function (data, encoding, callback) {
                  try {
                    if (!this.__firstRow) {
                      this.__firstRow = Object.keys(data);
                      this.push(this.__firstRow.join(',') + '\n');
                    }
                    const newChunk = this.__firstRow.map(key => data[key]).join(',') + '\n';
                    this.push(newChunk);
                    callback();
                  } catch (e) {
                    callback(e);
                  }
                }
              }
            })
          }
        }

        class ModifyRowStream extends Transform {
          constructor(options, modificationFun) {
            super(options);

            Object.defineProperties(this, {
              '__rowModification' : {
                enumerable : false,
                writable : false,
                value : modificationFun
              },
              '_transform' : {
                enumerable : false,
                value : function (data, encoding, callback) {
                  data = this.__rowModification.exec(data);
                  this.push(data);
                  callback();
                }
              }
            });
          }
        }

        return new Promise((resolve, reject) => {
          let tablesSet
          dsQuery('SELECT table_name::text AS name FROM information_schema.TABLES WHERE table_schema = $1;', [inputSchema])
            .then(res => {
              tablesSet = new Set(res.rows.map(item => item.name));

              return Promise.all(
                [...tablesSet].map(tableName => dsQuery(`CREATE TABLE ${outputSchema}.${tableName} (LIKE ${inputSchema}.${tableName} INCLUDING CONSTRAINTS INCLUDING INDEXES INCLUDING DEFAULTS);`))
              );
            })
            .then(() => {
              Promise.all([
                HTModelRuns.getRowModificationParameters(newRun.id),
                sails.models.htdatastock.getParameters(),
                sails.models.htdatastock.getGeoIdToNUTSMap()
              ])
              .then(results => {
                const [funArgs, parametersDict, idToNUTSMap] = results;
                const modifiedCopySet = new Set();
                let tablesToModify = funArgs.reduce((acum, curr) => {
                  if (!acum[parametersDict[curr.target].table]) {
                    acum[parametersDict[curr.target].table] = [];
                  }
                  modifiedCopySet.add(parametersDict[curr.target].table);
                  acum[parametersDict[curr.target].table].push(curr);
                  return acum;
                }, {});

                let streams = [...tablesSet]
                  .filter(table => !modifiedCopySet.has(table))
                  .map(tableName => dsQuery(`INSERT INTO ${outputSchema}.${tableName} (SELECT * FROM ${inputSchema}.${tableName});`));

                const sequentialCopy = new Promise((seqResolve, seqReject) => {
                  function* seqGen() {
                    for (let table in tablesToModify) {
                      tablesToModify[table] = tablesToModify[table].map(item => {
                        const {value, target, options} = item;
                        options.geoIdToCodes = idToNUTSMap;
                        const rowModification = new RowModification(value, target, options);
                        return new ModifyRowStream({objectMode : true}, rowModification);
                      });

                      yield new Promise((resolve, reject) => {
                        pg.connect(HTDataStock, (err, clientRead, doneRead) => {
                          if (err) {
                            reject(new Error(err));
                          } else {
                            const pgStream = clientRead.query(new QueryStream(`Select * FROM ${inputSchema}.${table};`));
                            pg.connect(sails.config.connections.HTDataStock, (err, clientWrite, doneWrite) => {
                              if (err) {
                                reject(new Error(err));
                              } else {
                                var copyFrom = require('pg-copy-streams').from,
                                  copyStream = clientWrite.query(copyFrom(`COPY ${schemaPrefix + newRun.id}.${table} FROM STDIN WITH DELIMITER ',' CSV HEADER;`));

                                const fullStream = tablesToModify[table].reduce((acum, curr) => {
                                  acum.pipe(curr);
                                  return curr;
                                }, pgStream);

                                const toCSVStream = new ToCSVStream({objectMode : true});

                                fullStream
                                  .pipe(toCSVStream)
                                  .pipe(copyStream)
                                  .on('end', () => {
                                    doneRead();
                                    doneWrite();
                                    resolve();
                                  })
                                  .on('finish', () => {
                                    doneRead();
                                    doneWrite();
                                    resolve();
                                  })
                                  .on('error', err => {
                                    doneRead();
                                    doneWrite();
                                    console.log(err);
                                    reject(new Error(err));
                                  });
                              }
                            });
                          }
                        });
                      });
                    }
                    return;
                  }

                  function scheduler(generator, onError, onDone) {
                    const currentStep = generator.next();
                    if (!currentStep.done) {
                      currentStep.value.then(() => {
                        scheduler(generator, onError, onDone);
                      })
                      .catch(onError);
                    } else {
                      onDone();
                    }
                  }

                  scheduler(seqGen(), seqReject, seqResolve);
                });

                streams.push(sequentialCopy);

              Promise.all(streams)
                .then(() => {})
                .then(resolve)
                .catch(reject);
              })
              .catch(reject);

            });
          });
      }

      function applyPolicy86() {
        return new Promise((resolve, reject) => {
          if (newRun.policy86 === true) {
            HTDataStock.shiftHypernetworkImpedances(
              `${sails.config.HTConfig.schemaPrefix}${newRun.id}`,
              sails.config.HTConfig.firsYearOfshiftHypernetworkImpedances,
              sails.config.HTConfig.lastYear,
              sails.config.HTConfig.yearsOfshiftHypernetworkImpedances
            )
              .then(resolve)
              .catch(reject);
          } else {
            resolve();
          }
        });
      }

      return new Promise((resolve, reject) => {
        resetSchema()
          .then(() => copyTables(newRun))
          .then(() => applyPolicy86())
          .then(() => {
            sails.log(`Schema ${outputSchema} is ready to host a model run.`);
            resolve(newRun);
          })
          .catch(err => reject(new Error(err)));
      });
    }
  };
  return;
}());
