(function () {
  function countryOrderPreference(country) {
    var priority;

    switch (country) {
    case 'EU28':
      priority = 0;
      break;
    case 'Austria':
    case 'Belgium':
    case 'Bulgaria':
    case 'Cyprus':
    case 'Czech Republic':
    case 'Germany':
    case 'Denmark':
    case 'Estonia':
    case 'Spain':
    case 'Finland':
    case 'France':
    case 'Greece':
    case 'Croatia':
    case 'Hungary':
    case 'Ireland':
    case 'Italy':
    case 'Lithuania':
    case 'Luxembourg':
    case 'Latvia':
    case 'Malta':
    case 'Netherlands':
    case 'Poland':
    case 'Portugal':
    case 'Romania':
    case 'Sweden':
    case 'Slovenia':
    case 'Slovakia':
    case 'United Kingdom':
      priority = 1;
      break;
    case 'Switzerland':
    case 'Norway':
      priority = 2;
      break;
    case 'Iceland':
    case 'Turkey':
    case 'Azerbaijan':
    case 'Bosnia Herzegovina':
    case 'Andorra':
    case 'Monaco':
    case 'Albania':
    case 'Armenia':
    case 'San Marino':
    case 'Serbia':
    case 'Liechtenstein':
    case 'Montenegro':
    case 'TFYR of Macedonia':
    case 'Belarus':
    case 'Ukraine':
    case 'Moldova':
    case 'Morocco':
    case 'Georgia':
    case 'Iran':
    case 'Uzbekistan':
    case 'Turkmenistan':
    case 'Russian Federation':
    case 'Kazakhstan':
      priority = 3;
      break;
    case 'CIS':
    case 'Antarctica':
    case 'Europe, Rest':
    case 'America Canada':
    case 'America USA':
    case 'America Mexico':
    case 'America Central':
    case 'America Caribbean':
    case 'America South':
    case 'Asia/Pacific Far East':
    case 'Asia/Pacific Southern Asia':
    case 'Asia/Pacific Indian Subcontinent':
    case 'Asia/Pacific Australia/Oceania':
    case 'Africa Nord':
    case 'Africa Central and South':
    case 'Africa East':
    case 'Middle East Mediterranean':
    case 'Middle East East':
    case 'Russia, east of Urals':
    case 'Russia, west of Urals':
      priority = 4;
      break;
    default:
      priority = 5;
    }
    return priority;
  }

  module.exports = {
    countryOrderPreference : countryOrderPreference
  };
}());
