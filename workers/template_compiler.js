/*jslint node: true, indent: 2, es6 : true */
(function () {
  /**
   * Module used to create a template compiler in a different process. Using a
   * different process allows to safely pipe std streams to the parent process.
   */
  'use strict';

  const java = require('java');

  java.classpath.push(`${process.cwd()}/java_modules/HTTemplateCompiler.jar`);



  process.on('message', msg => {
    msg = JSON.parse(msg);
    java.callStaticMethod('com.mcrit.ht.templateCompiler.XlsxTemplate',
      'compileAndStreamTemplate',
        msg.templateURL,
        JSON.stringify(msg.data),
        JSON.stringify(msg.styles)
      , function (err) {
          if (err) {
            throw new Error(err);
          }
          process.send('ok');
    });

  });

}());
