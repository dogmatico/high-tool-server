/*jslint node: true, indent: 2, es6 : true */
(function () {
  /**
   * Module used to handle JVM creation using processes.
   */
  'use strict';

  var config = require(process.cwd() + '/config/HTConfig.js').HTConfig,
    java = require('java');

  process.on('message', function (stepData) {
    stepData = JSON.parse(stepData);
    console.log(`Received the order to run the module ${stepData}.`);
    stepData.jars.forEach((jar) => java.classpath.push(`${process.cwd()}/java_modules/${jar}`));

    if (!stepData.jvmFlags) {
      stepData.jvmFlags = [];
    }
    stepData.jvmFlags.forEach((flag) => java.options.push(flag));

    if (!stepData.otherArguments) {
      stepData.otherArguments = [];
    }

    java.callStaticMethod(stepData.className, stepData.javaMethod, [
      config.schemaPrefix + stepData.id,
      config.schemaPrefix + stepData.id]
      .concat(stepData.otherArguments), err => {
        const msg = (err ? {
          result : 'error',
          err : err.toString()
        } : {
          result : 'ok'
        });
        process.send(JSON.stringify(msg));
    });
  });

}());
