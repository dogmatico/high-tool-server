# hightool-server

This is the server side of the [HIGH-TOOL](http://www.high-tool.eu/) interface. The objective of 
this software is to serve a REST API consumed by the HIGH-TOOL client.

## Dependencies
The app needs the following pieces of software to work:

* NodeJs 6.x and NPM 3.x.
* A Java v8 JDK (OpenJDK and Oracle JDK have been tested).
* A Postgresql instance with a copy of the Data Stock
* The usual dependencies of node-gyp (Python 2.7, a C++ compiler, etc.).
* The HIGH-TOOL models packaged in JAR files and their dependencies (mainly config files and Excel or LibreOffice).

## Installation
### Recomended tools 
The following tools are recommended to be installed as node globals
```bash
npm install sails -g
npm install grunt-cli
```
The server can be installed and set up without those tools, but having them will make 
the process easier.

### HIGH-TOOL-SERVER installation
1. Clone this repository using git.
2. Go to the source folder and install the dependencies

        npm install

3. Edit the file *config/jwt-variables.js* to set your JSON Web Token (JWT):

        (function () {
        /** 
        * Secret key to generate tokens.
        * 
        */
          return module.exports.JWT = {
            secretKey : 'REPLACE WITH YOUR LONG AND SECRET KEY'
          };
        }());

4. Set the credentials to connect to the HIGH-TOOL Data Stock editing this section of the 
   file *config/connections.js*:

        HTDataStock: {
          migrate : 'safe',
          adapter: 'sails-postgresql',
          host: 'REPLACE WITH THE DATA STOCK IP',
          port: REPLACE WITH THE DATA STOCK PORT,
          user: 'REPLACE WITH THE DATA STOCK USER',
          password: 'REPLACE WITH THE DATA STOCK PASSWORD',
          database: 'REPLACE WITH THE DATA STOCK DATABASE NAME'
        }

5. Start the server, 
    
        sails console
    
    and select option 2 to generate the instance files.

6. Exit from the sails instance pressing *CTRL+D*.

7. HIGH-TOOL server may use statistical data to modifiy policy levers. To install them, replace 
   *.tmp/localDiskDb.db* with *seedData/localDiskDb.db*.

       cp seedData/localDiskDb.db .tmp/localDiskDb.db
  
8. Create a new admin user with the console.

         sails console

    Select option *1. Safe* to keep your local data.

    Once inside the console, write this order to create the user

         User.create({email: 'EMAIL', name: 'NAME', isAdmin: true, password: 'PASSWORD'}).then(() => console.log('Ok'))


    The console should reply with an *Ok*.

This ends the installation of the HIGH-TOOL server.

### HIGH-TOOL Modules
The HIGH-TOOL modules are independent of the HIGH-TOOL Server. You need to install and configure them inside 
the *java_modules* folder. Use the *integration* branch for all the modules except *SAF*. For *SAF* use 
*SAFNoExcel* branch.

There is an script that can be used to download them from the HIGH-TOOL main server:
```bash
node downloadModules.js
``` 

### XLSX Template compiler
The template compiler is an independent Java module that must be installed inside *java_modules* folder. 

The *downloadModules.js* from the last point will download and install it if used.

## Server configuration

The steps of the installation guide provide a working instance. This may not be suitable for production environments.
If you're using a Linux distribution with Systemd, like Ubuntu, this configuration may be suitable for your needs.
Assuming that the app is installed under */opt/high-tool-server* and mean to be run as *www-data* user and port 5000, create a Systemd 
unit using this definition:

```
[Unit]
Description=High Tool Server

[Service]
Type=simple
Environment=NODE_ENV=production
WorkingDirectory=/opt/high-tool-server
ExecStart=/usr/bin/node app.js
Restart=on-failure
User=www-data
Group=www-data

[Install]
WantedBy=multi-user.target
```

and copy *config/local.js.tmpl* to *config/local.js*.