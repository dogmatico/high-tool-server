const {
  downloadAllModules,
  downloadTemplateCompiler,
  safSpecifics
} = require('./configUtils/updateModelsConfig.js');

Promise.all([
  downloadAllModules(),
  downloadTemplateCompiler()
])
.then(() => process.exit())
.catch(err => {
  console.log(err);
  process.exit(1);
});

