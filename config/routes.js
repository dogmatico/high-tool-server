/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/


  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  *  If a request to a URL doesn't match any of the custom routes above, it  *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/
  'post /login': 'AuthUserController.login',
  'get /logout': 'AuthUserController.logout',

  'get /api/user' : 'UserController.list',
  'get /api/user/:id' : 'UserController.userDetails',
  'post /api/user' : 'UserController.create',
  'put /api/user/:id/data' : 'UserController.editById',
  'get /api/user/:id/schemaQuota' : 'UserController.getQuota',
  'put /api/user/:id/setAdmin' : 'UserController.setAdmin',
  'delete /api/user/:id' : 'UserController.destroy',

  'get /api/run/' : 'RunController.listOwned',
  'get /api/run/done' : 'RunController.printReadyToOutput',
  'get /api/run/steps' : 'RunController.getSteps',
  'get /api/run/definition/:runId' : 'RunController.getDefinition',
  'get /api/run/results/:runId/DEM' : 'HTDataStockController.getDEMResults',
  'get /api/run/results/:runId/ECR' : 'HTDataStockController.getECRResults',
  'get /api/run/results/:runId/VES/:outputType' : 'HTDataStockController.getVESResults',
  'get /api/run/results/:runId/PAD' : 'HTDataStockController.getPADResults',
  'get /api/run/results/:runId/FRD' : 'HTDataStockController.getFRETransit',
  'get /api/run/results/:runId/SAF' : 'HTDataStockController.getSAFInjuries',
  'get /api/run/results/:runId/ENV/emissions' : 'HTDataStockController.getEnvironmentEmissions',
  'get /api/run/results/:runId/externalities/airPollution/:type' : 'HTDataStockController.getAirPollution',
  'get /api/run/results/:runId/externalities/climateChange/:type' : 'HTDataStockController.getClimateChange',
  'get /api/run/results/:runId/externalities/marginalInfrastructureCosts' : 'HTDataStockController.getMarginalInfrastructureCosts',
  'get /api/run/results/:runId/externalities/upDownstream' : 'HTDataStockController.getUpDownstreamCosts',
  'get /api/run/results/:runId/externalities/accidents' : 'HTDataStockController.getAccidentsCosts',
  'get /api/run/results/:runId/generalizedCosts/:type' : 'HTDataStockController.getGeneralizedCosts',
  'get /api/run/results/:runId/exportToXLS' : 'HTDataStockController.exportToXLS',
  'post /api/run/TPM' : 'RunController.runTPM',
  'post /api/run/policy/:packageId' : 'RunController.runPackage',
  'post /api/run/scenario/:scenarioId' : 'RunController.runScenario',
  'post /api/run/pause/:runId' : 'RunController.pauseRun',
  'post /api/run/resume/:runId' : 'RunController.resumeRun',
  'put /api/run/:runId' : 'RunController.changeStep',
  'delete /api/run/:runId' : 'RunController.deleteRun',

  'get /api/schema' : 'HTSchemaController.printAll',
  'get /api/schema/tpm/families' : 'HTSchemaController.getAllFamilies',
  'get /api/schema/tpm/combinations' : 'HTSchemaController.getTPMCombinations',
  'get /api/schema/tpm/spatialAndTemporalDimensions' : 'HTSchemaController.getSpatialAndTemporalDimensions',
  'get /api/schema/lever' : 'HTSchemaController.getLeverList',
  'get /api/schema/:rootNode' : 'HTSchemaController.printSubGraph',

  'get /api/package' : 'PackageController.list',
  'get /api/package/type/:packageType' : 'PackageController.list',
  'get /api/package/own' : 'PackageController.listOwned',
  'get /api/package/own/type/:packageType' : 'PackageController.listOwned',
  'get /api/package/:id' : 'PackageController.print',
  'get /api/package/:id/metadata' : 'PackageController.printMetadata',
  'get /api/package/:idOne/compare/:idTwo' : 'PackageController.compare',
  'post /api/package' : 'PackageController.create',
  'put /api/package/:id' : 'PackageController.update',
  'delete /api/package/:id' : 'PackageController.destroy',

  'get /api/transvisions/:id' : 'TransvisionsController.run',

  'get /api/assumptions/:schemaId/:moduleId' : 'HTDataStockController.getAssumptions',

  'get /api/tables/variables' : 'HTDataStockController.getVariableList',
  'get /api/tables/schema/:schemaId/table/:tableId' : 'HTDataStockController.downloadTable',
  'post /api/tables/schema/:schemaId/table/:tableId' : 'HTDataStockController.uploadTable',

  'get /api/statistics' : 'StatisticalDataController.listAll',
  'get /api/statistics/:id' : 'StatisticalDataController.list',
  'post /api/statistics' : 'StatisticalDataController.create',

  'get /api/modules' : 'HTModulesController.getInstalledModules',
  'get /api/modules/templateCompiler' : 'HTModulesController.sendTemplateCompiler',
  'get /api/modules/:moduleId' : 'HTModulesController.serveModule',
  'get /api/repositories' : 'HTModulesController.listModules',
  'post /api/repositories' : 'HTModulesController.registerMasterServer',
  //'put /api/modules/server/:serverId' : 'HTModulesController.registerMasterServer',
  'put /api/modules/:serverId/:moduleId' : 'HTModulesController.fetchAndUpdate',
  'delete /api/modules/:serverId' : 'HTModulesController.deleteMasterServer',

  'get /api/map/:regionId/:admLevel' : 'GISController.downloadMap',
  'get /api/HyperNetwork' : 'GISController.getHyperNetwork',
  'get /api/HyperNetwork/:runId' : 'HTDataStockController.getHyperNetwork',
  'put /api/HyperNetwork/:runId/mode/:modeId/edges' : 'HTDataStockController.updateHypernetworkEdges',
  'put /api/HyperNetwork/:runId/mode/:modeId/nodes' : 'HTDataStockController.updateHypernetworkNodes',

  'get /api/notification' : 'NotificationsController.subscribeToNotification',
  'delete /api/notification' : 'NotificationsController.unSubscribeToNotification'


};
