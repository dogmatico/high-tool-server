/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */
'use strict';
module.exports.bootstrap = function(cb) {
  sails.promise = require('bluebird');

  /* Trie data structure */
  sails.Trie = function() {
    this.value;
  }

  sails.Trie.prototype.add = function(uuid, value) {
    var node = this;

    for(var i = 0, ln = uuid.length; i < ln; i++) {
      var letter = uuid[i];
      node = node[letter] || (node[letter] = new sails.Trie());
    }
    node.value = value;
    node.iW = true;
    return;
  }

  sails.Trie.prototype.set = function(uuid, value) {
    this.add.call(this, uuid, value);
  }

  sails.Trie.prototype.get = function(uuid) {
    var node = this;

    for(var i = 0, ln = uuid.length; i < ln && node !== false; i++) {
      var letter = uuid[i];
      node = node[letter] || false ;
    }

    if(i === ln && node && node.iW) {
      var nodeVal = node.value;
    } else {
      var nodeVal = false;
    }

    return nodeVal;
  }

  /* Construct the graph from the database */
  HTSetupUtils.generateSchemaFromDS(sails.config.HTConfig.baseSchema, (err, res) => {
    if (err) {
      throw new Error(err);
    }
    sails.schema = res.schema;
    sails.uuidTrie = res.uuidTrie;
    HTSetupUtils.addCustomTPMSchema(sails.schema, sails.uuidTrie, err => {
      if (err) {
        throw new Error(err);
      }

      HTSetupUtils.addLeverList(sails.schema, err => {
        if (err) {
          throw new Error(err);
        }

        HTSetupUtils.spatialAndTemporalDimensions(sails.config.HTConfig.baseSchema, (err, dict) => {
          if (err) {
            throw new Error(err);
          }

          sails.schema.spatialAndTemporalDimensions = dict;
          HTModelRuns.find()
            .then(function (runs) {
              const markedDelete = runs.filter(function (item) {
                return item.markedDelete;
              })
              .map((item) => {
                return item.id;
              });

              const runnable = runs.filter(function (item) {
                return !item.markedDelete && item.step !== 'DONE';
              })
              .map((item) => {
                return item.id;
              });

              sails.runAndDeleteQueue = new HTModulesService.LifeCycleRun(runnable, markedDelete);
              //sails.runAndDeleteQueue.next();

              // It's very important to trigger this callback method when you are finished
              // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
              //cb();
              cb();
            });
        });
      });
    });
  });
};
