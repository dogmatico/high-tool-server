(function () {
  /**
   * Passport set to JWT
   */ 
  var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bcrypt = require('bcrypt-nodejs');
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });
    passport.use(new LocalStrategy ({
        usernameField: 'email',
        passwordField: 'password'
      },
      function(username, password, done) {
        User.findOne({email : username})
          .exec(function (err, user) {
            if(err) {
              return done(err);
            } else if (!user) {
              return done(null, false, {
                message : 'User not found'
              });
            } else {
              bcrypt.compare(password, user.password, function(err, res) {
                if(err || !res) {
                  return done(null, false, {
                    message : 'Invalid Password'
                  });
                } else {
                  return done(null, user);
                }
              });
            }  
          });
      })
    );
}());
