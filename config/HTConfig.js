/*jslint node: true, indent: 2 */
(function () {
  /**
   * Use this file to set the values and parameters of the HT Modules
   */
  'use strict';

  /**
   * placeholder type : Controller will do specific steps,
   * java type : controller will create a JVM with the specified information
   */

  var stepsArray = [{
    id : 'NEW',
    type : 'placeholder',
    description : 'In queue to create schema'
  },
  {
    id : 'EXPERTMODE',
    type : 'placeholder',
    description : 'Waiting for user\'s updates'
  }, {
    id : 'DEM',
    type : 'java',
    jars : ['Demography.jar'],
    className : 'eu.hightool.module.demography.Demography',
    javaMethod : 'main',
    jvmFlags : null,
    otherArguments : null,
    description : 'DEM module'
  }],
    years = ['2015', '2020', '2025', '2030', '2035', '2040', '2045', '2050'];

  years.forEach(function (year) {
    stepsArray.push({
      id : 'ECR_' + year,
      type : 'java',
      jars : ['Economy.jar'],
      className : 'eu.hightool.module.economy.EconomyModule',
      javaMethod : 'main',
      otherArguments : [year],
      description : 'ECR Module, year ' + year
    }, {
      id : 'VES_' + year,
      type : 'java',
      jars : ['Vehstock.jar'],
      jvmFlags : ['-Xms8G', '-Xmx14G'],
      className : 'eu.hightool.module.vehstock.Vehstock',
      javaMethod : 'main',
      otherArguments : [year],
      description : 'VES Module, year ' + year
    }, {
      id : 'PAD_' + year,
      type : 'java',
      jars : ['PassModule.jar'],
      className : 'eu.hightool.module.passenger.PassModule',
      javaMethod : 'main',
      otherArguments : [year],
      description : 'PAS Module, year ' + year
    }, {
      id : 'FRE_' + year,
      className : 'eu.hightool.module.freightdemand.FreightDemand',
      javaMethod : 'main',
      type : 'java',
      jars : ['FreightDemand.jar'],
      jvmFlags : ['-Xms10G', '-Xmx14G'],
      otherArguments : ['2010', '2050', '5', year],
      description : 'FRE Module, year ' + year
    });
  });

  years.forEach(year => {
    stepsArray.push({
      id : 'ENV_' + year,
      type : 'java',
      jars : ['Environment.jar'],
      className : 'eu.hightool.module.environment.Environment',
      javaMethod : 'main',
      jvmFlags : ['-Xms10G', '-Xmx14G'],
      otherArguments : [year],
      description : 'ENV Module, year' + year
    });
  })

  stepsArray = stepsArray.concat([
    {
      id : 'SAF',
      type : 'java',
      jars : ['Safety.jar'],
      className : 'eu.hightool.module.safety.Safety',
      javaMethod : 'main',
      jvmFlags : ['-Xms10G', '-Xmx14G'],
      otherArguments : null,
      description : 'SAF module'
    }, {
      id : 'DONE',
      type : 'placeholder',
      description : 'Finished. Ready to produce reports'
    }
  ]);

  function getStepNumber(id) {
    id = id.split('_');

    var moduleName = id[0],
      year = id[1] || null,
      step,
      substep = 0;

    switch (moduleName) {
    case 'NEW':
      step = 0;
      break;
    case 'EXPERTMODE':
      step = 1;
      break;
    case 'DEM':
      step = 2;
      break;
    case 'FRE':
      substep += 1;
    case 'PAD':
      substep += 1;
    case 'VES':
      substep += 1;
    case 'ECR':
      substep += 1;
      if (year !== null) {
        step = 2 + substep + ((parseInt(year, 10) - parseInt(years[0], 10)) / 5) * 4;
      } else {
        step = null;
      }
      break;
    case 'ENV':
      step = 3 + years.length * 4 + ((parseInt(year, 10) - parseInt(years[0], 10)) / 5);
      break;
    case 'DONE':
      substep += 1;
    case 'SAF':
      step = 3 + years.length * 5 + substep;
      break;
    default:
      step = null;
    }
    return step;
  }

  module.exports.HTConfig = {
    baseSchema : 'high_tool',
    LibreOfficeURL : 'uno:socket,host=localhost,port=2002;urp;StarOffice.ServiceManager',
    steps : stepsArray,
    reportTemplateFirstTab : 4,
    getStepNumber : getStepNumber,
    // Set the number of simultaneous runs that a user can save/launch. -1 = unlimited
    userQuota : -1,
    firstYear : 2010, // Used in the reports. 2010 is the first year but isn't computed. It comes from the Data Stock
    lastYear : 2050,
    stepSize : 5,
    standardFirstComputeYear : 2015,
    rootNode : 'EU28',
    schemaPrefix : 'userschema',
    yearsOfshiftHypernetworkImpedances : 5,
    firsYearOfshiftHypernetworkImpedances : 2025
  };
}());
