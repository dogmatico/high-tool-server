/**
 * Copy files and folders.
 *
 * ---------------------------------------------------------------
 *
 * # dev task config
 * Copies all directories and files, exept coffescript and less fiels, from the sails
 * assets folder into the .tmp/public directory.
 *
 * # build task config
 * Copies all directories nd files from the .tmp/public directory into a www directory.
 *
 * For usage docs see:
 * 		https://github.com/gruntjs/grunt-contrib-copy
 */
module.exports = function(grunt) {

	grunt.config.set('copy', {
		dev: {
			files: [{
				expand: true,
				cwd: './assets',
				src: ['**/*.!(coffee|less)'],
				dest: '.tmp/public'
			},
      { 
       expand: true,
       cwd: './bower_components/tinymce',
       src: [
       '*.*', 
       '**/*.*'
       ],
       dest: '.tmp/public/tinymce'
      },
      { 
       expand: true,
       cwd: './bower_components',
       src: [
       'angular/angular.min.js', 
       'angular-ui-router/release/angular-ui-router.min.js',
       'angular-bootstrap/ui-bootstrap-tpls.min.js',
       'angular-ui-tinymce/src/tinymce.js',
       'angular-sanitize/angular-sanitize.min.js',
       'd3/d3.min.js',
       'topojson/topojson.js'
       ],
       flatten: true,
       dest: '.tmp/public/js/dependencies'
      },
      { 
       expand: true,
       cwd: './bower_components',
       src: [
       'bootstrap-css/css/bootstrap.min.css',
       'bootstrap-css/css/bootstrap-theme.min.css'
       ],
       flatten: true,
       dest: '.tmp/public/styles'
      },
      { 
       expand: true,
       cwd: './bower_components/',
       src: [
       'bootstrap-css/fonts/glyphicons-halflings-regular.eot',
       'bootstrap-css/fonts/glyphicons-halflings-regular.svg',
       'bootstrap-css/fonts/glyphicons-halflings-regular.ttf',
       'bootstrap-css/fonts/glyphicons-halflings-regular.woff'
       ],
       flatten: true,
       dest: '.tmp/public/fonts'
      }]
		},
		build: {
			files: [{
				expand: true,
				cwd: '.tmp/public',
				src: ['**/*'],
				dest: 'www'
			}]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
};
